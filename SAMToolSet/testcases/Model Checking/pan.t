#ifdef PEG
struct T_SRC {
	char *fl; int ln;
} T_SRC[NTRANS];

void
tr_2_src(int m, char *file, int ln)
{	T_SRC[m].fl = file;
	T_SRC[m].ln = ln;
}

void
putpeg(int n, int m)
{	printf("%5d	trans %4d ", m, n);
	printf("%s:%d\n",
		T_SRC[n].fl, T_SRC[n].ln);
}
#endif

void
settable(void)
{	Trans *T;
	Trans *settr(int, int, int, int, int, char *, int, int, int);

	trans = (Trans ***) emalloc(5*sizeof(Trans **));

	/* proctype 3: f */

	trans[3] = (Trans **) emalloc(11*sizeof(Trans *));

	trans[3][7]	= settr(553,0,6,1,0,".(goto)", 0, 2, 0);
	T = trans[3][6] = settr(552,0,0,0,0,"DO", 0, 2, 0);
	T = T->nxt	= settr(552,0,3,0,0,"DO", 0, 2, 0);
	    T->nxt	= settr(552,0,4,0,0,"DO", 0, 2, 0);
	T = trans[ 3][3] = settr(549,2,0,0,0,"ATOMIC", 1, 2, 0);
	T->nxt	= settr(549,2,1,0,0,"ATOMIC", 1, 2, 0);
	trans[3][1]	= settr(547,0,6,3,3,"(!((sum<=300)))", 1, 2, 0); /* m: 2 -> 6,0 */
	reached3[2] = 1;
	trans[3][2]	= settr(0,0,0,0,0,"assert(!(!((sum<=300))))",0,0,0);
	trans[3][4]	= settr(550,0,6,1,0,"(1)", 0, 2, 0);
	trans[3][5]	= settr(551,0,6,1,0,"goto T0_init", 0, 2, 0);
	trans[3][8]	= settr(554,0,9,1,0,"break", 0, 2, 0);
	trans[3][9]	= settr(555,0,10,1,0,"(1)", 0, 2, 0);
	trans[3][10]	= settr(556,0,0,4,4,"-end-", 0, 3500, 0);

	/* proctype 2: :init: */

	trans[2] = (Trans **) emalloc(60*sizeof(Trans *));

	trans[2][1]	= settr(488,0,16,5,5,"ConPurse.ConPurse_field1 = 0", 0, 2, 0); /* m: 2 -> 0,16 */
	reached2[2] = 1;
	trans[2][2]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field2 = 100",0,0,0);
	trans[2][3]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field3 = 10",0,0,0);
	trans[2][4]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field4 = 1",0,0,0);
	trans[2][5]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field5 = 1",0,0,0);
	trans[2][6]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field6 = 0",0,0,0);
	trans[2][7]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field7 = 50",0,0,0);
	trans[2][8]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field8 = 1",0,0,0);
	trans[2][9]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field9 = 1",0,0,0);
	trans[2][10]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field10 = 1",0,0,0);
	trans[2][11]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field11 = 10",0,0,0);
	trans[2][12]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field12 = 2",0,0,0);
	trans[2][13]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field13 = 0",0,0,0);
	trans[2][14]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field14 = 1",0,0,0);
	trans[2][15]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field15 = 1",0,0,0);
	trans[2][16]	= settr(503,0,17,6,6,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[2][17]	= settr(504,0,32,7,7,"ConPurse.ConPurse_field1 = 11", 0, 2, 0); /* m: 18 -> 0,32 */
	reached2[18] = 1;
	trans[2][18]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field2 = 200",0,0,0);
	trans[2][19]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field3 = 10",0,0,0);
	trans[2][20]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field4 = 1",0,0,0);
	trans[2][21]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field5 = 3",0,0,0);
	trans[2][22]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field6 = 0",0,0,0);
	trans[2][23]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field7 = 50",0,0,0);
	trans[2][24]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field8 = 1",0,0,0);
	trans[2][25]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field9 = 1",0,0,0);
	trans[2][26]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field10 = 1",0,0,0);
	trans[2][27]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field11 = 2",0,0,0);
	trans[2][28]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field12 = 2",0,0,0);
	trans[2][29]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field13 = 0",0,0,0);
	trans[2][30]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field14 = 1",0,0,0);
	trans[2][31]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field15 = 1",0,0,0);
	trans[2][32]	= settr(519,0,33,8,8,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[2][33]	= settr(520,0,46,9,9,"", 0, 2, 0); /* m: 34 -> 0,46 */
	reached2[34] = 1;
	trans[2][34]	= settr(0,0,0,0,0,"",0,0,0);
	trans[2][35]	= settr(0,0,0,0,0,"",0,0,0);
	trans[2][36]	= settr(0,0,0,0,0,"msg_in.msg_in_field1 = 0",0,0,0);
	trans[2][37]	= settr(0,0,0,0,0,"msg_in.msg_in_field2 = 3",0,0,0);
	trans[2][38]	= settr(0,0,0,0,0,"msg_in.msg_in_field3 = 50",0,0,0);
	trans[2][39]	= settr(0,0,0,0,0,"msg_in.msg_in_field4 = 1",0,0,0);
	trans[2][40]	= settr(0,0,0,0,0,"msg_in.msg_in_field5 = 0",0,0,0);
	trans[2][41]	= settr(0,0,0,0,0,"msg_in.msg_in_field6 = 3",0,0,0);
	trans[2][42]	= settr(0,0,0,0,0,"msg_in.msg_in_field7 = 50",0,0,0);
	trans[2][43]	= settr(0,0,0,0,0,"msg_in.msg_in_field8 = 1",0,0,0);
	trans[2][44]	= settr(0,0,0,0,0,"msg_in.msg_in_field9 = 1",0,0,0);
	trans[2][45]	= settr(0,0,0,0,0,"msg_in.msg_in_field10 = 0",0,0,0);
	trans[2][46]	= settr(533,0,47,10,10,"place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 6, 0);
	trans[2][47]	= settr(534,0,57,11,11,"msg_in.msg_in_field1 = 7", 0, 2, 0); /* m: 48 -> 0,57 */
	reached2[48] = 1;
	trans[2][48]	= settr(0,0,0,0,0,"msg_in.msg_in_field2 = 0",0,0,0);
	trans[2][49]	= settr(0,0,0,0,0,"msg_in.msg_in_field3 = 50",0,0,0);
	trans[2][50]	= settr(0,0,0,0,0,"msg_in.msg_in_field4 = 1",0,0,0);
	trans[2][51]	= settr(0,0,0,0,0,"msg_in.msg_in_field5 = 0",0,0,0);
	trans[2][52]	= settr(0,0,0,0,0,"msg_in.msg_in_field6 = 3",0,0,0);
	trans[2][53]	= settr(0,0,0,0,0,"msg_in.msg_in_field7 = 50",0,0,0);
	trans[2][54]	= settr(0,0,0,0,0,"msg_in.msg_in_field8 = 1",0,0,0);
	trans[2][55]	= settr(0,0,0,0,0,"msg_in.msg_in_field9 = 1",0,0,0);
	trans[2][56]	= settr(0,0,0,0,0,"msg_in.msg_in_field10 = 3",0,0,0);
	trans[2][57]	= settr(544,0,58,12,12,"place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 6, 0);
	trans[2][58]	= settr(545,0,59,13,13,"(run Main())", 0, 2, 0);
	trans[2][59]	= settr(546,0,0,14,14,"-end-", 0, 3500, 0);

	/* proctype 1: check */

	trans[1] = (Trans **) emalloc(11*sizeof(Trans *));

	trans[1][8]	= settr(485,0,7,1,0,".(goto)", 0, 2, 0);
	T = trans[1][7] = settr(484,0,0,0,0,"DO", 0, 2, 0);
	T = T->nxt	= settr(484,0,1,0,0,"DO", 0, 2, 0);
	T = T->nxt	= settr(484,0,2,0,0,"DO", 0, 2, 0);
	T = T->nxt	= settr(484,0,3,0,0,"DO", 0, 2, 0);
	    T->nxt	= settr(484,0,5,0,0,"DO", 0, 2, 0);
	trans[1][1]	= settr(478,0,7,15,15,"place_ConPurse??<purse1.ConPurse_field1,purse1.ConPurse_field2,purse1.ConPurse_field3,purse1.ConPurse_field4,purse1.ConPurse_field5,purse1.ConPurse_field6,purse1.ConPurse_field7,purse1.ConPurse_field8,purse1.ConPurse_field9,purse1.ConPurse_field10,purse1.ConPurse_field11,purse1.ConPurse_field12,purse1.ConPurse_field13,purse1.ConPurse_field14,purse1.ConPurse_field15>", 1, 503, 0);
	trans[1][2]	= settr(479,0,7,16,16,"place_ConPurse??<purse2.ConPurse_field1,purse2.ConPurse_field2,purse2.ConPurse_field3,purse2.ConPurse_field4,purse2.ConPurse_field5,purse2.ConPurse_field6,purse2.ConPurse_field7,purse2.ConPurse_field8,purse2.ConPurse_field9,purse2.ConPurse_field10,purse2.ConPurse_field11,purse2.ConPurse_field12,purse2.ConPurse_field13,purse2.ConPurse_field14,purse2.ConPurse_field15>", 1, 503, 0);
	trans[1][3]	= settr(480,0,4,17,0,"((purse1.ConPurse_field1!=purse2.ConPurse_field1))", 1, 2, 0);
	trans[1][4]	= settr(481,0,7,18,18,"sum = (purse1.ConPurse_field2+purse2.ConPurse_field2)", 1, 2, 0);
	trans[1][5]	= settr(482,0,10,19,0,"((sum>300))", 1, 2, 0);
	trans[1][6]	= settr(483,0,10,1,0,"goto :b1", 0, 2, 0);
	trans[1][9]	= settr(486,0,10,1,0,"break", 0, 2, 0);
	trans[1][10]	= settr(487,0,0,20,20,"-end-", 0, 3500, 0);

	/* proctype 0: Main */

	trans[0] = (Trans **) emalloc(479*sizeof(Trans *));

	trans[0][476]	= settr(475,0,475,1,0,".(goto)", 0, 2, 0);
	T = trans[0][475] = settr(474,0,0,0,0,"DO", 0, 2, 0);
	T = T->nxt	= settr(474,0,50,0,0,"DO", 0, 2, 0);
	T = T->nxt	= settr(474,0,100,0,0,"DO", 0, 2, 0);
	T = T->nxt	= settr(474,0,150,0,0,"DO", 0, 2, 0);
	T = T->nxt	= settr(474,0,200,0,0,"DO", 0, 2, 0);
	T = T->nxt	= settr(474,0,250,0,0,"DO", 0, 2, 0);
	T = T->nxt	= settr(474,0,300,0,0,"DO", 0, 2, 0);
	T = T->nxt	= settr(474,0,350,0,0,"DO", 0, 2, 0);
	T = T->nxt	= settr(474,0,392,0,0,"DO", 0, 2, 0);
	T = T->nxt	= settr(474,0,442,0,0,"DO", 0, 2, 0);
	    T->nxt	= settr(474,0,474,0,0,"DO", 0, 2, 0);
	T = trans[ 0][50] = settr(49,2,0,0,0,"ATOMIC", 1, 2, 0);
	T->nxt	= settr(49,2,49,0,0,"ATOMIC", 1, 2, 0);
	T = trans[ 0][49] = settr(48,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(48,0,13,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][13] = settr(12,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(12,0,1,0,0,"sub-sequence", 1, 2, 0);
	trans[0][1]	= settr(0,2,2,21,0,"((place_ConPurse?[ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15]&&place_msg_in?[msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10]))", 1, 2, 0);
	trans[0][2]	= settr(1,2,3,22,22,"place_ConPurse?ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 503, 0);
	trans[0][3]	= settr(2,2,11,23,23,"place_msg_in?msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 506, 0);
	T = trans[0][11] = settr(10,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(10,2,5,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(10,2,7,0,0,"IF", 1, 2, 0);
	T = trans[ 0][5] = settr(4,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(4,0,4,0,0,"sub-sequence", 1, 2, 0);
	trans[0][4]	= settr(3,2,47,24,24,"((((((((msg_in.msg_in_field1==0)&&(ConPurse.ConPurse_field1==msg_in.msg_in_field10))&&(ConPurse.ConPurse_field1==msg_in.msg_in_field2))&&(ConPurse.ConPurse_field2>=msg_in.msg_in_field3))&&(ConPurse.ConPurse_field10==1))&&1)&&1))", 1, 2, 0); /* m: 6 -> 47,0 */
	reached0[6] = 1;
	trans[0][6]	= settr(0,0,0,0,0,"startFrom_is_enabled = 1",0,0,0);
	trans[0][12]	= settr(11,2,47,1,0,".(goto)", 1, 2, 0);
	trans[0][7]	= settr(6,2,10,2,0,"else", 1, 2, 0);
	T = trans[ 0][10] = settr(9,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(9,0,8,0,0,"sub-sequence", 1, 2, 0);
	trans[0][8]	= settr(7,2,9,25,25,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[0][9]	= settr(8,2,47,26,26,"place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 6, 0);
	T = trans[0][47] = settr(46,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(46,2,14,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(46,2,45,0,0,"IF", 1, 2, 0);
	trans[0][14]	= settr(13,2,44,27,27,"(startFrom_is_enabled)", 1, 2, 0);
	T = trans[ 0][44] = settr(43,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(43,0,43,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][43] = settr(42,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(42,0,15,0,0,"sub-sequence", 1, 2, 0);
	trans[0][15]	= settr(14,2,40,28,28,"ConPurse.ConPurse_field1 = ConPurse.ConPurse_field1", 1, 2, 0); /* m: 16 -> 0,40 */
	reached0[16] = 1;
	trans[0][16]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field2 = ConPurse.ConPurse_field2",0,0,0);
	trans[0][17]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field3 = ConPurse.ConPurse_field3",0,0,0);
	trans[0][18]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field4 = (ConPurse.ConPurse_field4+1)",0,0,0);
	trans[0][19]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field5 = ConPurse.ConPurse_field1",0,0,0);
	trans[0][20]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field6 = msg_in.msg_in_field2",0,0,0);
	trans[0][21]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field7 = msg_in.msg_in_field3",0,0,0);
	trans[0][22]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field8 = ConPurse.ConPurse_field4",0,0,0);
	trans[0][23]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field9 = msg_in.msg_in_field4",0,0,0);
	trans[0][24]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field10 = 2",0,0,0);
	trans[0][25]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field11 = ConPurse.ConPurse_field11",0,0,0);
	trans[0][26]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field12 = ConPurse.ConPurse_field12",0,0,0);
	trans[0][27]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field13 = ConPurse.ConPurse_field13",0,0,0);
	trans[0][28]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field14 = ConPurse.ConPurse_field14",0,0,0);
	trans[0][29]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field15 = ConPurse.ConPurse_field15",0,0,0);
	trans[0][30]	= settr(0,0,0,0,0,"msg_out.msg_out_field1 = 3",0,0,0);
	trans[0][31]	= settr(0,0,0,0,0,"msg_out.msg_out_field2 = msg_in.msg_in_field2",0,0,0);
	trans[0][32]	= settr(0,0,0,0,0,"msg_out.msg_out_field3 = msg_in.msg_in_field3",0,0,0);
	trans[0][33]	= settr(0,0,0,0,0,"msg_out.msg_out_field4 = msg_in.msg_in_field4",0,0,0);
	trans[0][34]	= settr(0,0,0,0,0,"msg_out.msg_out_field5 = msg_in.msg_in_field5",0,0,0);
	trans[0][35]	= settr(0,0,0,0,0,"msg_out.msg_out_field6 = msg_in.msg_in_field6",0,0,0);
	trans[0][36]	= settr(0,0,0,0,0,"msg_out.msg_out_field7 = msg_in.msg_in_field7",0,0,0);
	trans[0][37]	= settr(0,0,0,0,0,"msg_out.msg_out_field8 = msg_in.msg_in_field8",0,0,0);
	trans[0][38]	= settr(0,0,0,0,0,"msg_out.msg_out_field9 = msg_in.msg_in_field9",0,0,0);
	trans[0][39]	= settr(0,0,0,0,0,"msg_out.msg_out_field10 = msg_in.msg_in_field10",0,0,0);
	trans[0][40]	= settr(39,2,41,29,29,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[0][41]	= settr(40,0,475,30,30,"place_msg_out!msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10", 1, 5, 0); /* m: 42 -> 475,0 */
	reached0[42] = 1;
	trans[0][42]	= settr(0,0,0,0,0,"startFrom_is_enabled = 0",0,0,0);
	trans[0][48]	= settr(47,0,475,31,31,".(goto)", 1, 2, 0);
	trans[0][45]	= settr(44,2,46,2,0,"else", 1, 2, 0);
	trans[0][46]	= settr(45,0,475,32,32,"(1)", 1, 2, 0); /* m: 48 -> 475,0 */
	reached0[48] = 1;
	T = trans[ 0][100] = settr(99,2,0,0,0,"ATOMIC", 1, 2, 0);
	T->nxt	= settr(99,2,99,0,0,"ATOMIC", 1, 2, 0);
	T = trans[ 0][99] = settr(98,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(98,0,63,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][63] = settr(62,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(62,0,51,0,0,"sub-sequence", 1, 2, 0);
	trans[0][51]	= settr(50,2,52,33,0,"((place_ConPurse?[ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15]&&place_msg_in?[msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10]))", 1, 2, 0);
	trans[0][52]	= settr(51,2,53,34,34,"place_ConPurse?ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 503, 0);
	trans[0][53]	= settr(52,2,61,35,35,"place_msg_in?msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 506, 0);
	T = trans[0][61] = settr(60,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(60,2,55,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(60,2,57,0,0,"IF", 1, 2, 0);
	T = trans[ 0][55] = settr(54,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(54,0,54,0,0,"sub-sequence", 1, 2, 0);
	trans[0][54]	= settr(53,2,97,36,36,"((((((msg_in.msg_in_field1==4)&&(ConPurse.ConPurse_field1==msg_in.msg_in_field10))&&(ConPurse.ConPurse_field10==2))&&1)&&1))", 1, 2, 0); /* m: 56 -> 97,0 */
	reached0[56] = 1;
	trans[0][56]	= settr(0,0,0,0,0,"req_is_enabled = 1",0,0,0);
	trans[0][62]	= settr(61,2,97,1,0,".(goto)", 1, 2, 0);
	trans[0][57]	= settr(56,2,60,2,0,"else", 1, 2, 0);
	T = trans[ 0][60] = settr(59,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(59,0,58,0,0,"sub-sequence", 1, 2, 0);
	trans[0][58]	= settr(57,2,59,37,37,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[0][59]	= settr(58,2,97,38,38,"place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 6, 0);
	T = trans[0][97] = settr(96,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(96,2,64,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(96,2,95,0,0,"IF", 1, 2, 0);
	trans[0][64]	= settr(63,2,94,39,39,"(req_is_enabled)", 1, 2, 0);
	T = trans[ 0][94] = settr(93,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(93,0,93,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][93] = settr(92,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(92,0,65,0,0,"sub-sequence", 1, 2, 0);
	trans[0][65]	= settr(64,2,90,40,40,"ConPurse.ConPurse_field1 = ConPurse.ConPurse_field1", 1, 2, 0); /* m: 66 -> 0,90 */
	reached0[66] = 1;
	trans[0][66]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field2 = (ConPurse.ConPurse_field2-msg_in.msg_in_field7)",0,0,0);
	trans[0][67]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field3 = ConPurse.ConPurse_field3",0,0,0);
	trans[0][68]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field4 = ConPurse.ConPurse_field4",0,0,0);
	trans[0][69]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field5 = ConPurse.ConPurse_field5",0,0,0);
	trans[0][70]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field6 = ConPurse.ConPurse_field6",0,0,0);
	trans[0][71]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field7 = ConPurse.ConPurse_field7",0,0,0);
	trans[0][72]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field8 = ConPurse.ConPurse_field8",0,0,0);
	trans[0][73]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field9 = ConPurse.ConPurse_field9",0,0,0);
	trans[0][74]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field10 = 5",0,0,0);
	trans[0][75]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field11 = ConPurse.ConPurse_field11",0,0,0);
	trans[0][76]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field12 = ConPurse.ConPurse_field12",0,0,0);
	trans[0][77]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field13 = ConPurse.ConPurse_field13",0,0,0);
	trans[0][78]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field14 = ConPurse.ConPurse_field14",0,0,0);
	trans[0][79]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field15 = ConPurse.ConPurse_field15",0,0,0);
	trans[0][80]	= settr(0,0,0,0,0,"msg_out.msg_out_field1 = 6",0,0,0);
	trans[0][81]	= settr(0,0,0,0,0,"msg_out.msg_out_field2 = msg_in.msg_in_field2",0,0,0);
	trans[0][82]	= settr(0,0,0,0,0,"msg_out.msg_out_field3 = msg_in.msg_in_field3",0,0,0);
	trans[0][83]	= settr(0,0,0,0,0,"msg_out.msg_out_field4 = msg_in.msg_in_field4",0,0,0);
	trans[0][84]	= settr(0,0,0,0,0,"msg_out.msg_out_field5 = ConPurse.ConPurse_field5",0,0,0);
	trans[0][85]	= settr(0,0,0,0,0,"msg_out.msg_out_field6 = ConPurse.ConPurse_field6",0,0,0);
	trans[0][86]	= settr(0,0,0,0,0,"msg_out.msg_out_field7 = ConPurse.ConPurse_field7",0,0,0);
	trans[0][87]	= settr(0,0,0,0,0,"msg_out.msg_out_field8 = ConPurse.ConPurse_field8",0,0,0);
	trans[0][88]	= settr(0,0,0,0,0,"msg_out.msg_out_field9 = ConPurse.ConPurse_field9",0,0,0);
	trans[0][89]	= settr(0,0,0,0,0,"msg_out.msg_out_field10 = msg_in.msg_in_field6",0,0,0);
	trans[0][90]	= settr(89,2,91,41,41,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[0][91]	= settr(90,0,475,42,42,"place_msg_out!msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10", 1, 5, 0); /* m: 92 -> 475,0 */
	reached0[92] = 1;
	trans[0][92]	= settr(0,0,0,0,0,"req_is_enabled = 0",0,0,0);
	trans[0][98]	= settr(97,0,475,43,43,".(goto)", 1, 2, 0);
	trans[0][95]	= settr(94,2,96,2,0,"else", 1, 2, 0);
	trans[0][96]	= settr(95,0,475,44,44,"(1)", 1, 2, 0); /* m: 98 -> 475,0 */
	reached0[98] = 1;
	T = trans[ 0][150] = settr(149,2,0,0,0,"ATOMIC", 1, 2, 0);
	T->nxt	= settr(149,2,149,0,0,"ATOMIC", 1, 2, 0);
	T = trans[ 0][149] = settr(148,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(148,0,113,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][113] = settr(112,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(112,0,101,0,0,"sub-sequence", 1, 2, 0);
	trans[0][101]	= settr(100,2,102,45,0,"((place_ConPurse?[ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15]&&place_msg_in?[msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10]))", 1, 2, 0);
	trans[0][102]	= settr(101,2,103,46,46,"place_ConPurse?ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 503, 0);
	trans[0][103]	= settr(102,2,111,47,47,"place_msg_in?msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 506, 0);
	T = trans[0][111] = settr(110,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(110,2,105,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(110,2,107,0,0,"IF", 1, 2, 0);
	T = trans[ 0][105] = settr(104,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(104,0,104,0,0,"sub-sequence", 1, 2, 0);
	trans[0][104]	= settr(103,2,147,48,48,"((((((((msg_in.msg_in_field1==7)&&(ConPurse.ConPurse_field1==msg_in.msg_in_field10))&&(ConPurse.ConPurse_field1==msg_in.msg_in_field2))&&(ConPurse.ConPurse_field2>=msg_in.msg_in_field3))&&(ConPurse.ConPurse_field10==1))&&1)&&1))", 1, 2, 0); /* m: 106 -> 147,0 */
	reached0[106] = 1;
	trans[0][106]	= settr(0,0,0,0,0,"startTo_is_enabled = 1",0,0,0);
	trans[0][112]	= settr(111,2,147,1,0,".(goto)", 1, 2, 0);
	trans[0][107]	= settr(106,2,110,2,0,"else", 1, 2, 0);
	T = trans[ 0][110] = settr(109,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(109,0,108,0,0,"sub-sequence", 1, 2, 0);
	trans[0][108]	= settr(107,2,109,49,49,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[0][109]	= settr(108,2,147,50,50,"place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 6, 0);
	T = trans[0][147] = settr(146,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(146,2,114,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(146,2,145,0,0,"IF", 1, 2, 0);
	trans[0][114]	= settr(113,2,144,51,51,"(startTo_is_enabled)", 1, 2, 0);
	T = trans[ 0][144] = settr(143,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(143,0,143,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][143] = settr(142,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(142,0,115,0,0,"sub-sequence", 1, 2, 0);
	trans[0][115]	= settr(114,2,140,52,52,"ConPurse.ConPurse_field1 = ConPurse.ConPurse_field1", 1, 2, 0); /* m: 116 -> 0,140 */
	reached0[116] = 1;
	trans[0][116]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field2 = ConPurse.ConPurse_field2",0,0,0);
	trans[0][117]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field3 = ConPurse.ConPurse_field3",0,0,0);
	trans[0][118]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field4 = (ConPurse.ConPurse_field4+1)",0,0,0);
	trans[0][119]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field5 = ConPurse.ConPurse_field1",0,0,0);
	trans[0][120]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field6 = msg_in.msg_in_field2",0,0,0);
	trans[0][121]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field7 = msg_in.msg_in_field3",0,0,0);
	trans[0][122]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field8 = ConPurse.ConPurse_field4",0,0,0);
	trans[0][123]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field9 = msg_in.msg_in_field4",0,0,0);
	trans[0][124]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field10 = 8",0,0,0);
	trans[0][125]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field11 = ConPurse.ConPurse_field11",0,0,0);
	trans[0][126]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field12 = ConPurse.ConPurse_field12",0,0,0);
	trans[0][127]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field13 = ConPurse.ConPurse_field13",0,0,0);
	trans[0][128]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field14 = ConPurse.ConPurse_field14",0,0,0);
	trans[0][129]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field15 = ConPurse.ConPurse_field15",0,0,0);
	trans[0][130]	= settr(0,0,0,0,0,"msg_out.msg_out_field1 = 4",0,0,0);
	trans[0][131]	= settr(0,0,0,0,0,"msg_out.msg_out_field2 = msg_in.msg_in_field2",0,0,0);
	trans[0][132]	= settr(0,0,0,0,0,"msg_out.msg_out_field3 = msg_in.msg_in_field3",0,0,0);
	trans[0][133]	= settr(0,0,0,0,0,"msg_out.msg_out_field4 = msg_in.msg_in_field4",0,0,0);
	trans[0][134]	= settr(0,0,0,0,0,"msg_out.msg_out_field5 = msg_in.msg_in_field2",0,0,0);
	trans[0][135]	= settr(0,0,0,0,0,"msg_out.msg_out_field6 = ConPurse.ConPurse_field1",0,0,0);
	trans[0][136]	= settr(0,0,0,0,0,"msg_out.msg_out_field7 = msg_in.msg_in_field3",0,0,0);
	trans[0][137]	= settr(0,0,0,0,0,"msg_out.msg_out_field8 = ConPurse.ConPurse_field4",0,0,0);
	trans[0][138]	= settr(0,0,0,0,0,"msg_out.msg_out_field9 = msg_in.msg_in_field4",0,0,0);
	trans[0][139]	= settr(0,0,0,0,0,"msg_out.msg_out_field10 = msg_in.msg_in_field2",0,0,0);
	trans[0][140]	= settr(139,2,141,53,53,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[0][141]	= settr(140,0,475,54,54,"place_msg_out!msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10", 1, 5, 0); /* m: 142 -> 475,0 */
	reached0[142] = 1;
	trans[0][142]	= settr(0,0,0,0,0,"startTo_is_enabled = 0",0,0,0);
	trans[0][148]	= settr(147,0,475,55,55,".(goto)", 1, 2, 0);
	trans[0][145]	= settr(144,2,146,2,0,"else", 1, 2, 0);
	trans[0][146]	= settr(145,0,475,56,56,"(1)", 1, 2, 0); /* m: 148 -> 475,0 */
	reached0[148] = 1;
	T = trans[ 0][200] = settr(199,2,0,0,0,"ATOMIC", 1, 2, 0);
	T->nxt	= settr(199,2,199,0,0,"ATOMIC", 1, 2, 0);
	T = trans[ 0][199] = settr(198,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(198,0,163,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][163] = settr(162,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(162,0,151,0,0,"sub-sequence", 1, 2, 0);
	trans[0][151]	= settr(150,2,152,57,0,"((place_ConPurse?[ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15]&&place_msg_in?[msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10]))", 1, 2, 0);
	trans[0][152]	= settr(151,2,153,58,58,"place_ConPurse?ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 503, 0);
	trans[0][153]	= settr(152,2,161,59,59,"place_msg_in?msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 506, 0);
	T = trans[0][161] = settr(160,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(160,2,155,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(160,2,157,0,0,"IF", 1, 2, 0);
	T = trans[ 0][155] = settr(154,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(154,0,154,0,0,"sub-sequence", 1, 2, 0);
	trans[0][154]	= settr(153,2,197,60,60,"(((((((msg_in.msg_in_field1==9)&&(ConPurse.ConPurse_field1==msg_in.msg_in_field10))&&(ConPurse.ConPurse_field10==1))&&(ConPurse.ConPurse_field3==10))&&1)&&1))", 1, 2, 0); /* m: 156 -> 197,0 */
	reached0[156] = 1;
	trans[0][156]	= settr(0,0,0,0,0,"readExceptionLog_is_enabled = 1",0,0,0);
	trans[0][162]	= settr(161,2,197,1,0,".(goto)", 1, 2, 0);
	trans[0][157]	= settr(156,2,160,2,0,"else", 1, 2, 0);
	T = trans[ 0][160] = settr(159,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(159,0,158,0,0,"sub-sequence", 1, 2, 0);
	trans[0][158]	= settr(157,2,159,61,61,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[0][159]	= settr(158,2,197,62,62,"place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 6, 0);
	T = trans[0][197] = settr(196,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(196,2,164,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(196,2,195,0,0,"IF", 1, 2, 0);
	trans[0][164]	= settr(163,2,194,63,63,"(readExceptionLog_is_enabled)", 1, 2, 0);
	T = trans[ 0][194] = settr(193,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(193,0,193,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][193] = settr(192,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(192,0,165,0,0,"sub-sequence", 1, 2, 0);
	trans[0][165]	= settr(164,2,190,64,64,"ConPurse.ConPurse_field1 = ConPurse.ConPurse_field1", 1, 2, 0); /* m: 166 -> 0,190 */
	reached0[166] = 1;
	trans[0][166]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field2 = ConPurse.ConPurse_field2",0,0,0);
	trans[0][167]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field3 = ConPurse.ConPurse_field3",0,0,0);
	trans[0][168]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field4 = ConPurse.ConPurse_field4",0,0,0);
	trans[0][169]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field5 = ConPurse.ConPurse_field5",0,0,0);
	trans[0][170]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field6 = ConPurse.ConPurse_field6",0,0,0);
	trans[0][171]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field7 = ConPurse.ConPurse_field7",0,0,0);
	trans[0][172]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field8 = ConPurse.ConPurse_field8",0,0,0);
	trans[0][173]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field9 = ConPurse.ConPurse_field9",0,0,0);
	trans[0][174]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field10 = ConPurse.ConPurse_field10",0,0,0);
	trans[0][175]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field11 = ConPurse.ConPurse_field11",0,0,0);
	trans[0][176]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field12 = ConPurse.ConPurse_field12",0,0,0);
	trans[0][177]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field13 = ConPurse.ConPurse_field13",0,0,0);
	trans[0][178]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field14 = ConPurse.ConPurse_field14",0,0,0);
	trans[0][179]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field15 = ConPurse.ConPurse_field15",0,0,0);
	trans[0][180]	= settr(0,0,0,0,0,"msg_out.msg_out_field1 = 11",0,0,0);
	trans[0][181]	= settr(0,0,0,0,0,"msg_out.msg_out_field2 = msg_in.msg_in_field2",0,0,0);
	trans[0][182]	= settr(0,0,0,0,0,"msg_out.msg_out_field3 = msg_in.msg_in_field3",0,0,0);
	trans[0][183]	= settr(0,0,0,0,0,"msg_out.msg_out_field4 = msg_in.msg_in_field4",0,0,0);
	trans[0][184]	= settr(0,0,0,0,0,"msg_out.msg_out_field5 = ConPurse.ConPurse_field11",0,0,0);
	trans[0][185]	= settr(0,0,0,0,0,"msg_out.msg_out_field6 = ConPurse.ConPurse_field12",0,0,0);
	trans[0][186]	= settr(0,0,0,0,0,"msg_out.msg_out_field7 = ConPurse.ConPurse_field13",0,0,0);
	trans[0][187]	= settr(0,0,0,0,0,"msg_out.msg_out_field8 = ConPurse.ConPurse_field14",0,0,0);
	trans[0][188]	= settr(0,0,0,0,0,"msg_out.msg_out_field9 = ConPurse.ConPurse_field15",0,0,0);
	trans[0][189]	= settr(0,0,0,0,0,"msg_out.msg_out_field10 = msg_in.msg_in_field10",0,0,0);
	trans[0][190]	= settr(189,2,191,65,65,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[0][191]	= settr(190,0,475,66,66,"place_msg_out!msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10", 1, 5, 0); /* m: 192 -> 475,0 */
	reached0[192] = 1;
	trans[0][192]	= settr(0,0,0,0,0,"readExceptionLog_is_enabled = 0",0,0,0);
	trans[0][198]	= settr(197,0,475,67,67,".(goto)", 1, 2, 0);
	trans[0][195]	= settr(194,2,196,2,0,"else", 1, 2, 0);
	trans[0][196]	= settr(195,0,475,68,68,"(1)", 1, 2, 0); /* m: 198 -> 475,0 */
	reached0[198] = 1;
	T = trans[ 0][250] = settr(249,2,0,0,0,"ATOMIC", 1, 2, 0);
	T->nxt	= settr(249,2,249,0,0,"ATOMIC", 1, 2, 0);
	T = trans[ 0][249] = settr(248,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(248,0,213,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][213] = settr(212,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(212,0,201,0,0,"sub-sequence", 1, 2, 0);
	trans[0][201]	= settr(200,2,202,69,0,"((place_ConPurse?[ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15]&&place_msg_in?[msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10]))", 1, 2, 0);
	trans[0][202]	= settr(201,2,203,70,70,"place_ConPurse?ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 503, 0);
	trans[0][203]	= settr(202,2,211,71,71,"place_msg_in?msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 506, 0);
	T = trans[0][211] = settr(210,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(210,2,205,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(210,2,207,0,0,"IF", 1, 2, 0);
	T = trans[ 0][205] = settr(204,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(204,0,204,0,0,"sub-sequence", 1, 2, 0);
	trans[0][204]	= settr(203,2,247,72,72,"(((((((msg_in.msg_in_field1==12)&&(ConPurse.ConPurse_field1==msg_in.msg_in_field10))&&(ConPurse.ConPurse_field10==1))&&(ConPurse.ConPurse_field3==10))&&1)&&1))", 1, 2, 0); /* m: 206 -> 247,0 */
	reached0[206] = 1;
	trans[0][206]	= settr(0,0,0,0,0,"clearExceptionLog_is_enabled = 1",0,0,0);
	trans[0][212]	= settr(211,2,247,1,0,".(goto)", 1, 2, 0);
	trans[0][207]	= settr(206,2,210,2,0,"else", 1, 2, 0);
	T = trans[ 0][210] = settr(209,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(209,0,208,0,0,"sub-sequence", 1, 2, 0);
	trans[0][208]	= settr(207,2,209,73,73,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[0][209]	= settr(208,2,247,74,74,"place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 6, 0);
	T = trans[0][247] = settr(246,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(246,2,214,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(246,2,245,0,0,"IF", 1, 2, 0);
	trans[0][214]	= settr(213,2,244,75,75,"(clearExceptionLog_is_enabled)", 1, 2, 0);
	T = trans[ 0][244] = settr(243,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(243,0,243,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][243] = settr(242,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(242,0,215,0,0,"sub-sequence", 1, 2, 0);
	trans[0][215]	= settr(214,2,240,76,76,"ConPurse.ConPurse_field1 = ConPurse.ConPurse_field1", 1, 2, 0); /* m: 216 -> 0,240 */
	reached0[216] = 1;
	trans[0][216]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field2 = ConPurse.ConPurse_field2",0,0,0);
	trans[0][217]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field3 = 13",0,0,0);
	trans[0][218]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field4 = ConPurse.ConPurse_field4",0,0,0);
	trans[0][219]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field5 = ConPurse.ConPurse_field5",0,0,0);
	trans[0][220]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field6 = ConPurse.ConPurse_field6",0,0,0);
	trans[0][221]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field7 = ConPurse.ConPurse_field7",0,0,0);
	trans[0][222]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field8 = ConPurse.ConPurse_field8",0,0,0);
	trans[0][223]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field9 = ConPurse.ConPurse_field9",0,0,0);
	trans[0][224]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field10 = ConPurse.ConPurse_field10",0,0,0);
	trans[0][225]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field11 = ConPurse.ConPurse_field11",0,0,0);
	trans[0][226]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field12 = ConPurse.ConPurse_field12",0,0,0);
	trans[0][227]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field13 = ConPurse.ConPurse_field13",0,0,0);
	trans[0][228]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field14 = ConPurse.ConPurse_field14",0,0,0);
	trans[0][229]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field15 = ConPurse.ConPurse_field15",0,0,0);
	trans[0][230]	= settr(0,0,0,0,0,"msg_out.msg_out_field1 = 3",0,0,0);
	trans[0][231]	= settr(0,0,0,0,0,"msg_out.msg_out_field2 = msg_in.msg_in_field2",0,0,0);
	trans[0][232]	= settr(0,0,0,0,0,"msg_out.msg_out_field3 = msg_in.msg_in_field3",0,0,0);
	trans[0][233]	= settr(0,0,0,0,0,"msg_out.msg_out_field4 = msg_in.msg_in_field4",0,0,0);
	trans[0][234]	= settr(0,0,0,0,0,"msg_out.msg_out_field5 = msg_in.msg_in_field5",0,0,0);
	trans[0][235]	= settr(0,0,0,0,0,"msg_out.msg_out_field6 = msg_in.msg_in_field6",0,0,0);
	trans[0][236]	= settr(0,0,0,0,0,"msg_out.msg_out_field7 = msg_in.msg_in_field7",0,0,0);
	trans[0][237]	= settr(0,0,0,0,0,"msg_out.msg_out_field8 = msg_in.msg_in_field8",0,0,0);
	trans[0][238]	= settr(0,0,0,0,0,"msg_out.msg_out_field9 = msg_in.msg_in_field9",0,0,0);
	trans[0][239]	= settr(0,0,0,0,0,"msg_out.msg_out_field10 = msg_in.msg_in_field10",0,0,0);
	trans[0][240]	= settr(239,2,241,77,77,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[0][241]	= settr(240,0,475,78,78,"place_msg_out!msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10", 1, 5, 0); /* m: 242 -> 475,0 */
	reached0[242] = 1;
	trans[0][242]	= settr(0,0,0,0,0,"clearExceptionLog_is_enabled = 0",0,0,0);
	trans[0][248]	= settr(247,0,475,79,79,".(goto)", 1, 2, 0);
	trans[0][245]	= settr(244,2,246,2,0,"else", 1, 2, 0);
	trans[0][246]	= settr(245,0,475,80,80,"(1)", 1, 2, 0); /* m: 248 -> 475,0 */
	reached0[248] = 1;
	T = trans[ 0][300] = settr(299,2,0,0,0,"ATOMIC", 1, 2, 0);
	T->nxt	= settr(299,2,299,0,0,"ATOMIC", 1, 2, 0);
	T = trans[ 0][299] = settr(298,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(298,0,263,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][263] = settr(262,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(262,0,251,0,0,"sub-sequence", 1, 2, 0);
	trans[0][251]	= settr(250,2,252,81,0,"((place_ConPurse?[ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15]&&place_msg_in?[msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10]))", 1, 2, 0);
	trans[0][252]	= settr(251,2,253,82,82,"place_ConPurse?ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 503, 0);
	trans[0][253]	= settr(252,2,261,83,83,"place_msg_in?msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 506, 0);
	T = trans[0][261] = settr(260,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(260,2,255,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(260,2,257,0,0,"IF", 1, 2, 0);
	T = trans[ 0][255] = settr(254,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(254,0,254,0,0,"sub-sequence", 1, 2, 0);
	trans[0][254]	= settr(253,2,297,84,84,"((((((msg_in.msg_in_field1==14)&&(ConPurse.ConPurse_field1==msg_in.msg_in_field10))&&(ConPurse.ConPurse_field10==5))&&1)&&1))", 1, 2, 0); /* m: 256 -> 297,0 */
	reached0[256] = 1;
	trans[0][256]	= settr(0,0,0,0,0,"ack_is_enabled = 1",0,0,0);
	trans[0][262]	= settr(261,2,297,1,0,".(goto)", 1, 2, 0);
	trans[0][257]	= settr(256,2,260,2,0,"else", 1, 2, 0);
	T = trans[ 0][260] = settr(259,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(259,0,258,0,0,"sub-sequence", 1, 2, 0);
	trans[0][258]	= settr(257,2,259,85,85,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[0][259]	= settr(258,2,297,86,86,"place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 6, 0);
	T = trans[0][297] = settr(296,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(296,2,264,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(296,2,295,0,0,"IF", 1, 2, 0);
	trans[0][264]	= settr(263,2,294,87,87,"(ack_is_enabled)", 1, 2, 0);
	T = trans[ 0][294] = settr(293,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(293,0,293,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][293] = settr(292,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(292,0,265,0,0,"sub-sequence", 1, 2, 0);
	trans[0][265]	= settr(264,2,290,88,88,"ConPurse.ConPurse_field1 = ConPurse.ConPurse_field1", 1, 2, 0); /* m: 266 -> 0,290 */
	reached0[266] = 1;
	trans[0][266]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field2 = ConPurse.ConPurse_field2",0,0,0);
	trans[0][267]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field3 = ConPurse.ConPurse_field3",0,0,0);
	trans[0][268]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field4 = ConPurse.ConPurse_field4",0,0,0);
	trans[0][269]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field5 = ConPurse.ConPurse_field5",0,0,0);
	trans[0][270]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field6 = ConPurse.ConPurse_field6",0,0,0);
	trans[0][271]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field7 = ConPurse.ConPurse_field7",0,0,0);
	trans[0][272]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field8 = ConPurse.ConPurse_field8",0,0,0);
	trans[0][273]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field9 = ConPurse.ConPurse_field9",0,0,0);
	trans[0][274]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field10 = 1",0,0,0);
	trans[0][275]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field11 = ConPurse.ConPurse_field11",0,0,0);
	trans[0][276]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field12 = ConPurse.ConPurse_field12",0,0,0);
	trans[0][277]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field13 = ConPurse.ConPurse_field13",0,0,0);
	trans[0][278]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field14 = ConPurse.ConPurse_field14",0,0,0);
	trans[0][279]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field15 = ConPurse.ConPurse_field15",0,0,0);
	trans[0][280]	= settr(0,0,0,0,0,"msg_out.msg_out_field1 = 3",0,0,0);
	trans[0][281]	= settr(0,0,0,0,0,"msg_out.msg_out_field2 = msg_in.msg_in_field2",0,0,0);
	trans[0][282]	= settr(0,0,0,0,0,"msg_out.msg_out_field3 = msg_in.msg_in_field3",0,0,0);
	trans[0][283]	= settr(0,0,0,0,0,"msg_out.msg_out_field4 = msg_in.msg_in_field4",0,0,0);
	trans[0][284]	= settr(0,0,0,0,0,"msg_out.msg_out_field5 = msg_in.msg_in_field5",0,0,0);
	trans[0][285]	= settr(0,0,0,0,0,"msg_out.msg_out_field6 = msg_in.msg_in_field6",0,0,0);
	trans[0][286]	= settr(0,0,0,0,0,"msg_out.msg_out_field7 = msg_in.msg_in_field7",0,0,0);
	trans[0][287]	= settr(0,0,0,0,0,"msg_out.msg_out_field8 = msg_in.msg_in_field8",0,0,0);
	trans[0][288]	= settr(0,0,0,0,0,"msg_out.msg_out_field9 = msg_in.msg_in_field9",0,0,0);
	trans[0][289]	= settr(0,0,0,0,0,"msg_out.msg_out_field10 = msg_in.msg_in_field10",0,0,0);
	trans[0][290]	= settr(289,2,291,89,89,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[0][291]	= settr(290,0,475,90,90,"place_msg_out!msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10", 1, 5, 0); /* m: 292 -> 475,0 */
	reached0[292] = 1;
	trans[0][292]	= settr(0,0,0,0,0,"ack_is_enabled = 0",0,0,0);
	trans[0][298]	= settr(297,0,475,91,91,".(goto)", 1, 2, 0);
	trans[0][295]	= settr(294,2,296,2,0,"else", 1, 2, 0);
	trans[0][296]	= settr(295,0,475,92,92,"(1)", 1, 2, 0); /* m: 298 -> 475,0 */
	reached0[298] = 1;
	T = trans[ 0][350] = settr(349,2,0,0,0,"ATOMIC", 1, 2, 0);
	T->nxt	= settr(349,2,349,0,0,"ATOMIC", 1, 2, 0);
	T = trans[ 0][349] = settr(348,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(348,0,313,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][313] = settr(312,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(312,0,301,0,0,"sub-sequence", 1, 2, 0);
	trans[0][301]	= settr(300,2,302,93,0,"((place_ConPurse?[ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15]&&place_msg_in?[msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10]))", 1, 2, 0);
	trans[0][302]	= settr(301,2,303,94,94,"place_ConPurse?ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 503, 0);
	trans[0][303]	= settr(302,2,311,95,95,"place_msg_in?msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 506, 0);
	T = trans[0][311] = settr(310,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(310,2,305,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(310,2,307,0,0,"IF", 1, 2, 0);
	T = trans[ 0][305] = settr(304,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(304,0,304,0,0,"sub-sequence", 1, 2, 0);
	trans[0][304]	= settr(303,2,347,96,96,"((((((msg_in.msg_in_field1==6)&&(ConPurse.ConPurse_field1==msg_in.msg_in_field10))&&(ConPurse.ConPurse_field10==8))&&1)&&1))", 1, 2, 0); /* m: 306 -> 347,0 */
	reached0[306] = 1;
	trans[0][306]	= settr(0,0,0,0,0,"val_is_enabled = 1",0,0,0);
	trans[0][312]	= settr(311,2,347,1,0,".(goto)", 1, 2, 0);
	trans[0][307]	= settr(306,2,310,2,0,"else", 1, 2, 0);
	T = trans[ 0][310] = settr(309,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(309,0,308,0,0,"sub-sequence", 1, 2, 0);
	trans[0][308]	= settr(307,2,309,97,97,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[0][309]	= settr(308,2,347,98,98,"place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 6, 0);
	T = trans[0][347] = settr(346,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(346,2,314,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(346,2,345,0,0,"IF", 1, 2, 0);
	trans[0][314]	= settr(313,2,344,99,99,"(val_is_enabled)", 1, 2, 0);
	T = trans[ 0][344] = settr(343,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(343,0,343,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][343] = settr(342,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(342,0,315,0,0,"sub-sequence", 1, 2, 0);
	trans[0][315]	= settr(314,2,340,100,100,"ConPurse.ConPurse_field1 = ConPurse.ConPurse_field1", 1, 2, 0); /* m: 316 -> 0,340 */
	reached0[316] = 1;
	trans[0][316]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field2 = (ConPurse.ConPurse_field2+msg_in.msg_in_field7)",0,0,0);
	trans[0][317]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field3 = ConPurse.ConPurse_field3",0,0,0);
	trans[0][318]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field4 = ConPurse.ConPurse_field4",0,0,0);
	trans[0][319]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field5 = ConPurse.ConPurse_field5",0,0,0);
	trans[0][320]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field6 = ConPurse.ConPurse_field6",0,0,0);
	trans[0][321]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field7 = ConPurse.ConPurse_field7",0,0,0);
	trans[0][322]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field8 = ConPurse.ConPurse_field8",0,0,0);
	trans[0][323]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field9 = ConPurse.ConPurse_field9",0,0,0);
	trans[0][324]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field10 = 1",0,0,0);
	trans[0][325]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field11 = ConPurse.ConPurse_field11",0,0,0);
	trans[0][326]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field12 = ConPurse.ConPurse_field12",0,0,0);
	trans[0][327]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field13 = ConPurse.ConPurse_field13",0,0,0);
	trans[0][328]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field14 = ConPurse.ConPurse_field14",0,0,0);
	trans[0][329]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field15 = ConPurse.ConPurse_field15",0,0,0);
	trans[0][330]	= settr(0,0,0,0,0,"msg_out.msg_out_field1 = 14",0,0,0);
	trans[0][331]	= settr(0,0,0,0,0,"msg_out.msg_out_field2 = msg_in.msg_in_field2",0,0,0);
	trans[0][332]	= settr(0,0,0,0,0,"msg_out.msg_out_field3 = msg_in.msg_in_field3",0,0,0);
	trans[0][333]	= settr(0,0,0,0,0,"msg_out.msg_out_field4 = msg_in.msg_in_field4",0,0,0);
	trans[0][334]	= settr(0,0,0,0,0,"msg_out.msg_out_field5 = ConPurse.ConPurse_field5",0,0,0);
	trans[0][335]	= settr(0,0,0,0,0,"msg_out.msg_out_field6 = ConPurse.ConPurse_field6",0,0,0);
	trans[0][336]	= settr(0,0,0,0,0,"msg_out.msg_out_field7 = ConPurse.ConPurse_field7",0,0,0);
	trans[0][337]	= settr(0,0,0,0,0,"msg_out.msg_out_field8 = ConPurse.ConPurse_field8",0,0,0);
	trans[0][338]	= settr(0,0,0,0,0,"msg_out.msg_out_field9 = ConPurse.ConPurse_field9",0,0,0);
	trans[0][339]	= settr(0,0,0,0,0,"msg_out.msg_out_field10 = msg_in.msg_in_field5",0,0,0);
	trans[0][340]	= settr(339,2,341,101,101,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[0][341]	= settr(340,0,475,102,102,"place_msg_out!msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10", 1, 5, 0); /* m: 342 -> 475,0 */
	reached0[342] = 1;
	trans[0][342]	= settr(0,0,0,0,0,"val_is_enabled = 0",0,0,0);
	trans[0][348]	= settr(347,0,475,103,103,".(goto)", 1, 2, 0);
	trans[0][345]	= settr(344,2,346,2,0,"else", 1, 2, 0);
	trans[0][346]	= settr(345,0,475,104,104,"(1)", 1, 2, 0); /* m: 348 -> 475,0 */
	reached0[348] = 1;
	T = trans[ 0][392] = settr(391,2,0,0,0,"ATOMIC", 1, 2, 0);
	T->nxt	= settr(391,2,391,0,0,"ATOMIC", 1, 2, 0);
	T = trans[ 0][391] = settr(390,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(390,0,365,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][365] = settr(364,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(364,0,351,0,0,"sub-sequence", 1, 2, 0);
	trans[0][351]	= settr(350,2,352,105,0,"(((place_ConPurse?[ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15]&&place_LogBook?[LogBook.LogBook_field1,LogBook.LogBook_field2,LogBook.LogBook_field3,LogBook.LogBook_field4,LogBook.LogBook_field5,LogBook.LogBook_field6])&&place_msg_in?[msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10]))", 1, 2, 0);
	trans[0][352]	= settr(351,2,353,106,106,"place_ConPurse?ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 503, 0);
	trans[0][353]	= settr(352,2,354,107,107,"place_LogBook?LogBook.LogBook_field1,LogBook.LogBook_field2,LogBook.LogBook_field3,LogBook.LogBook_field4,LogBook.LogBook_field5,LogBook.LogBook_field6", 1, 504, 0);
	trans[0][354]	= settr(353,2,363,108,108,"place_msg_in?msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 506, 0);
	T = trans[0][363] = settr(362,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(362,2,356,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(362,2,358,0,0,"IF", 1, 2, 0);
	T = trans[ 0][356] = settr(355,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(355,0,355,0,0,"sub-sequence", 1, 2, 0);
	trans[0][355]	= settr(354,2,389,109,109,"((((((((msg_in.msg_in_field1==0)||(msg_in.msg_in_field1==7))||(msg_in.msg_in_field1==12))&&(ConPurse.ConPurse_field1==msg_in.msg_in_field10))&&((ConPurse.ConPurse_field10==8)||(ConPurse.ConPurse_field10==5)))&&1)&&1))", 1, 2, 0); /* m: 357 -> 389,0 */
	reached0[357] = 1;
	trans[0][357]	= settr(0,0,0,0,0,"Abort_is_enabled = 1",0,0,0);
	trans[0][364]	= settr(363,2,389,1,0,".(goto)", 1, 2, 0);
	trans[0][358]	= settr(357,2,362,2,0,"else", 1, 2, 0);
	T = trans[ 0][362] = settr(361,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(361,0,359,0,0,"sub-sequence", 1, 2, 0);
	trans[0][359]	= settr(358,2,360,110,110,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[0][360]	= settr(359,2,361,111,111,"place_LogBook!LogBook.LogBook_field1,LogBook.LogBook_field2,LogBook.LogBook_field3,LogBook.LogBook_field4,LogBook.LogBook_field5,LogBook.LogBook_field6", 1, 4, 0);
	trans[0][361]	= settr(360,2,389,112,112,"place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 6, 0);
	T = trans[0][389] = settr(388,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(388,2,366,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(388,2,387,0,0,"IF", 1, 2, 0);
	trans[0][366]	= settr(365,2,386,113,113,"(Abort_is_enabled)", 1, 2, 0);
	T = trans[ 0][386] = settr(385,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(385,0,385,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][385] = settr(384,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(384,0,367,0,0,"sub-sequence", 1, 2, 0);
	trans[0][367]	= settr(366,2,382,114,114,"ConPurse.ConPurse_field1 = ConPurse.ConPurse_field1", 1, 2, 0); /* m: 368 -> 0,382 */
	reached0[368] = 1;
	trans[0][368]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field2 = ConPurse.ConPurse_field2",0,0,0);
	trans[0][369]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field3 = 10",0,0,0);
	trans[0][370]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field4 = ConPurse.ConPurse_field4",0,0,0);
	trans[0][371]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field5 = (ConPurse.ConPurse_field5+1)",0,0,0);
	trans[0][372]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field6 = ConPurse.ConPurse_field6",0,0,0);
	trans[0][373]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field7 = ConPurse.ConPurse_field7",0,0,0);
	trans[0][374]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field8 = ConPurse.ConPurse_field8",0,0,0);
	trans[0][375]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field9 = ConPurse.ConPurse_field9",0,0,0);
	trans[0][376]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field10 = 1",0,0,0);
	trans[0][377]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field11 = ConPurse.ConPurse_field5",0,0,0);
	trans[0][378]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field12 = ConPurse.ConPurse_field6",0,0,0);
	trans[0][379]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field13 = ConPurse.ConPurse_field7",0,0,0);
	trans[0][380]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field14 = ConPurse.ConPurse_field8",0,0,0);
	trans[0][381]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field15 = ConPurse.ConPurse_field9",0,0,0);
	trans[0][382]	= settr(381,2,383,115,115,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[0][383]	= settr(382,0,475,116,116,"place_LogBook!LogBook.LogBook_field1,LogBook.LogBook_field2,LogBook.LogBook_field3,LogBook.LogBook_field4,LogBook.LogBook_field5,LogBook.LogBook_field6", 1, 4, 0); /* m: 384 -> 475,0 */
	reached0[384] = 1;
	trans[0][384]	= settr(0,0,0,0,0,"Abort_is_enabled = 0",0,0,0);
	trans[0][390]	= settr(389,0,475,117,117,".(goto)", 1, 2, 0);
	trans[0][387]	= settr(386,2,388,2,0,"else", 1, 2, 0);
	trans[0][388]	= settr(387,0,475,118,118,"(1)", 1, 2, 0); /* m: 390 -> 475,0 */
	reached0[390] = 1;
	T = trans[ 0][442] = settr(441,2,0,0,0,"ATOMIC", 1, 2, 0);
	T->nxt	= settr(441,2,441,0,0,"ATOMIC", 1, 2, 0);
	T = trans[ 0][441] = settr(440,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(440,0,405,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][405] = settr(404,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(404,0,393,0,0,"sub-sequence", 1, 2, 0);
	trans[0][393]	= settr(392,2,394,119,0,"((place_ConPurse?[ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15]&&place_msg_in?[msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10]))", 1, 2, 0);
	trans[0][394]	= settr(393,2,395,120,120,"place_ConPurse?ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 503, 0);
	trans[0][395]	= settr(394,2,403,121,121,"place_msg_in?msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 506, 0);
	T = trans[0][403] = settr(402,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(402,2,397,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(402,2,399,0,0,"IF", 1, 2, 0);
	T = trans[ 0][397] = settr(396,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(396,0,396,0,0,"sub-sequence", 1, 2, 0);
	trans[0][396]	= settr(395,2,439,122,122,"(((((((msg_in.msg_in_field1==9)&&(ConPurse.ConPurse_field1==msg_in.msg_in_field10))&&(ConPurse.ConPurse_field10==1))&&(ConPurse.ConPurse_field3==13))&&1)&&1))", 1, 2, 0); /* m: 398 -> 439,0 */
	reached0[398] = 1;
	trans[0][398]	= settr(0,0,0,0,0,"readExceptionLogForged_is_enabled = 1",0,0,0);
	trans[0][404]	= settr(403,2,439,1,0,".(goto)", 1, 2, 0);
	trans[0][399]	= settr(398,2,402,2,0,"else", 1, 2, 0);
	T = trans[ 0][402] = settr(401,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(401,0,400,0,0,"sub-sequence", 1, 2, 0);
	trans[0][400]	= settr(399,2,401,123,123,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[0][401]	= settr(400,2,439,124,124,"place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 6, 0);
	T = trans[0][439] = settr(438,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(438,2,406,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(438,2,437,0,0,"IF", 1, 2, 0);
	trans[0][406]	= settr(405,2,436,125,125,"(readExceptionLogForged_is_enabled)", 1, 2, 0);
	T = trans[ 0][436] = settr(435,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(435,0,435,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][435] = settr(434,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(434,0,407,0,0,"sub-sequence", 1, 2, 0);
	trans[0][407]	= settr(406,2,432,126,126,"ConPurse.ConPurse_field1 = ConPurse.ConPurse_field1", 1, 2, 0); /* m: 408 -> 0,432 */
	reached0[408] = 1;
	trans[0][408]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field2 = ConPurse.ConPurse_field2",0,0,0);
	trans[0][409]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field3 = ConPurse.ConPurse_field3",0,0,0);
	trans[0][410]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field4 = ConPurse.ConPurse_field4",0,0,0);
	trans[0][411]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field5 = ConPurse.ConPurse_field5",0,0,0);
	trans[0][412]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field6 = ConPurse.ConPurse_field6",0,0,0);
	trans[0][413]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field7 = ConPurse.ConPurse_field7",0,0,0);
	trans[0][414]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field8 = ConPurse.ConPurse_field8",0,0,0);
	trans[0][415]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field9 = ConPurse.ConPurse_field9",0,0,0);
	trans[0][416]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field10 = ConPurse.ConPurse_field10",0,0,0);
	trans[0][417]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field11 = ConPurse.ConPurse_field11",0,0,0);
	trans[0][418]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field12 = ConPurse.ConPurse_field12",0,0,0);
	trans[0][419]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field13 = ConPurse.ConPurse_field13",0,0,0);
	trans[0][420]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field14 = ConPurse.ConPurse_field14",0,0,0);
	trans[0][421]	= settr(0,0,0,0,0,"ConPurse.ConPurse_field15 = ConPurse.ConPurse_field15",0,0,0);
	trans[0][422]	= settr(0,0,0,0,0,"msg_out.msg_out_field1 = 3",0,0,0);
	trans[0][423]	= settr(0,0,0,0,0,"msg_out.msg_out_field2 = msg_in.msg_in_field2",0,0,0);
	trans[0][424]	= settr(0,0,0,0,0,"msg_out.msg_out_field3 = msg_in.msg_in_field3",0,0,0);
	trans[0][425]	= settr(0,0,0,0,0,"msg_out.msg_out_field4 = msg_in.msg_in_field4",0,0,0);
	trans[0][426]	= settr(0,0,0,0,0,"msg_out.msg_out_field5 = msg_in.msg_in_field5",0,0,0);
	trans[0][427]	= settr(0,0,0,0,0,"msg_out.msg_out_field6 = msg_in.msg_in_field6",0,0,0);
	trans[0][428]	= settr(0,0,0,0,0,"msg_out.msg_out_field7 = msg_in.msg_in_field7",0,0,0);
	trans[0][429]	= settr(0,0,0,0,0,"msg_out.msg_out_field8 = msg_in.msg_in_field8",0,0,0);
	trans[0][430]	= settr(0,0,0,0,0,"msg_out.msg_out_field9 = msg_in.msg_in_field9",0,0,0);
	trans[0][431]	= settr(0,0,0,0,0,"msg_out.msg_out_field10 = msg_in.msg_in_field10",0,0,0);
	trans[0][432]	= settr(431,2,433,127,127,"place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15", 1, 3, 0);
	trans[0][433]	= settr(432,0,475,128,128,"place_msg_out!msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10", 1, 5, 0); /* m: 434 -> 475,0 */
	reached0[434] = 1;
	trans[0][434]	= settr(0,0,0,0,0,"readExceptionLogForged_is_enabled = 0",0,0,0);
	trans[0][440]	= settr(439,0,475,129,129,".(goto)", 1, 2, 0);
	trans[0][437]	= settr(436,2,438,2,0,"else", 1, 2, 0);
	trans[0][438]	= settr(437,0,475,130,130,"(1)", 1, 2, 0); /* m: 440 -> 475,0 */
	reached0[440] = 1;
	T = trans[ 0][474] = settr(473,2,0,0,0,"ATOMIC", 1, 2, 0);
	T->nxt	= settr(473,2,473,0,0,"ATOMIC", 1, 2, 0);
	T = trans[ 0][473] = settr(472,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(472,0,453,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][453] = settr(452,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(452,0,443,0,0,"sub-sequence", 1, 2, 0);
	trans[0][443]	= settr(442,2,444,131,0,"(place_msg_out?[msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10])", 1, 2, 0);
	trans[0][444]	= settr(443,2,451,132,132,"place_msg_out?msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10", 1, 505, 0);
	T = trans[0][451] = settr(450,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(450,2,446,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(450,2,448,0,0,"IF", 1, 2, 0);
	T = trans[ 0][446] = settr(445,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(445,0,445,0,0,"sub-sequence", 1, 2, 0);
	trans[0][445]	= settr(444,2,471,133,133,"(((msg_out.msg_out_field1==3)&&1))", 1, 2, 0); /* m: 447 -> 471,0 */
	reached0[447] = 1;
	trans[0][447]	= settr(0,0,0,0,0,"ether_is_enabled = 1",0,0,0);
	trans[0][452]	= settr(451,2,471,1,0,".(goto)", 1, 2, 0);
	trans[0][448]	= settr(447,2,450,2,0,"else", 1, 2, 0);
	T = trans[ 0][450] = settr(449,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(449,0,449,0,0,"sub-sequence", 1, 2, 0);
	trans[0][449]	= settr(448,2,471,134,134,"place_msg_out!msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10", 1, 5, 0);
	T = trans[0][471] = settr(470,2,0,0,0,"IF", 1, 2, 0);
	T = T->nxt	= settr(470,2,454,0,0,"IF", 1, 2, 0);
	    T->nxt	= settr(470,2,469,0,0,"IF", 1, 2, 0);
	trans[0][454]	= settr(453,2,468,135,135,"(ether_is_enabled)", 1, 2, 0);
	T = trans[ 0][468] = settr(467,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(467,0,467,0,0,"sub-sequence", 1, 2, 0);
	T = trans[ 0][467] = settr(466,0,0,0,0,"sub-sequence", 1, 2, 0);
	T->nxt	= settr(466,0,455,0,0,"sub-sequence", 1, 2, 0);
	trans[0][455]	= settr(454,2,465,136,136,"msg_in.msg_in_field1 = msg_out.msg_out_field1", 1, 2, 0); /* m: 456 -> 0,465 */
	reached0[456] = 1;
	trans[0][456]	= settr(0,0,0,0,0,"msg_in.msg_in_field2 = msg_out.msg_out_field2",0,0,0);
	trans[0][457]	= settr(0,0,0,0,0,"msg_in.msg_in_field3 = msg_out.msg_out_field3",0,0,0);
	trans[0][458]	= settr(0,0,0,0,0,"msg_in.msg_in_field4 = msg_out.msg_out_field4",0,0,0);
	trans[0][459]	= settr(0,0,0,0,0,"msg_in.msg_in_field5 = msg_out.msg_out_field5",0,0,0);
	trans[0][460]	= settr(0,0,0,0,0,"msg_in.msg_in_field6 = msg_out.msg_out_field6",0,0,0);
	trans[0][461]	= settr(0,0,0,0,0,"msg_in.msg_in_field7 = msg_out.msg_out_field7",0,0,0);
	trans[0][462]	= settr(0,0,0,0,0,"msg_in.msg_in_field8 = msg_out.msg_out_field8",0,0,0);
	trans[0][463]	= settr(0,0,0,0,0,"msg_in.msg_in_field9 = msg_out.msg_out_field9",0,0,0);
	trans[0][464]	= settr(0,0,0,0,0,"msg_in.msg_in_field10 = msg_out.msg_out_field10",0,0,0);
	trans[0][465]	= settr(464,0,475,137,137,"place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10", 1, 6, 0); /* m: 466 -> 475,0 */
	reached0[466] = 1;
	trans[0][466]	= settr(0,0,0,0,0,"ether_is_enabled = 0",0,0,0);
	trans[0][472]	= settr(471,0,475,138,138,".(goto)", 1, 2, 0);
	trans[0][469]	= settr(468,2,470,2,0,"else", 1, 2, 0);
	trans[0][470]	= settr(469,0,475,139,139,"(1)", 1, 2, 0); /* m: 472 -> 475,0 */
	reached0[472] = 1;
	trans[0][477]	= settr(476,0,478,1,0,"break", 0, 2, 0);
	trans[0][478]	= settr(477,0,0,140,140,"-end-", 0, 3500, 0);
	/* np_ demon: */
	trans[_NP_] = (Trans **) emalloc(2*sizeof(Trans *));
	T = trans[_NP_][0] = settr(9997,0,1,_T5,0,"(np_)", 1,2,0);
	    T->nxt	  = settr(9998,0,0,_T2,0,"(1)",   0,2,0);
	T = trans[_NP_][1] = settr(9999,0,1,_T5,0,"(np_)", 1,2,0);
}

Trans *
settr(	int t_id, int a, int b, int c, int d,
	char *t, int g, int tpe0, int tpe1)
{	Trans *tmp = (Trans *) emalloc(sizeof(Trans));

	tmp->atom  = a&(6|32);	/* only (2|8|32) have meaning */
	if (!g) tmp->atom |= 8;	/* no global references */
	tmp->st    = b;
	tmp->tpe[0] = tpe0;
	tmp->tpe[1] = tpe1;
	tmp->tp    = t;
	tmp->t_id  = t_id;
	tmp->forw  = c;
	tmp->back  = d;
	return tmp;
}

Trans *
cpytr(Trans *a)
{	Trans *tmp = (Trans *) emalloc(sizeof(Trans));

	int i;
	tmp->atom  = a->atom;
	tmp->st    = a->st;
#ifdef HAS_UNLESS
	tmp->e_trans = a->e_trans;
	for (i = 0; i < HAS_UNLESS; i++)
		tmp->escp[i] = a->escp[i];
#endif
	tmp->tpe[0] = a->tpe[0];
	tmp->tpe[1] = a->tpe[1];
	for (i = 0; i < 6; i++)
	{	tmp->qu[i] = a->qu[i];
		tmp->ty[i] = a->ty[i];
	}
	tmp->tp    = (char *) emalloc(strlen(a->tp)+1);
	strcpy(tmp->tp, a->tp);
	tmp->t_id  = a->t_id;
	tmp->forw  = a->forw;
	tmp->back  = a->back;
	return tmp;
}

#ifndef NOREDUCE
int
srinc_set(int n)
{	if (n <= 2) return LOCAL;
	if (n <= 2+  DELTA) return Q_FULL_F; /* 's' or nfull  */
	if (n <= 2+2*DELTA) return Q_EMPT_F; /* 'r' or nempty */
	if (n <= 2+3*DELTA) return Q_EMPT_T; /* empty */
	if (n <= 2+4*DELTA) return Q_FULL_T; /* full  */
	if (n ==   5*DELTA) return GLOBAL;
	if (n ==   6*DELTA) return TIMEOUT_F;
	if (n ==   7*DELTA) return ALPHA_F;
	Uerror("cannot happen srinc_class");
	return BAD;
}
int
srunc(int n, int m)
{	switch(m) {
	case Q_FULL_F: return n-2;
	case Q_EMPT_F: return n-2-DELTA;
	case Q_EMPT_T: return n-2-2*DELTA;
	case Q_FULL_T: return n-2-3*DELTA;
	case ALPHA_F:
	case TIMEOUT_F: return 257; /* non-zero, and > MAXQ */
	}
	Uerror("cannot happen srunc");
	return 0;
}
#endif
int cnt;
#ifdef HAS_UNLESS
int
isthere(Trans *a, int b)
{	Trans *t;
	for (t = a; t; t = t->nxt)
		if (t->t_id == b)
			return 1;
	return 0;
}
#endif
#ifndef NOREDUCE
int
mark_safety(Trans *t) /* for conditional safety */
{	int g = 0, i, j, k;

	if (!t) return 0;
	if (t->qu[0])
		return (t->qu[1])?2:1;	/* marked */

	for (i = 0; i < 2; i++)
	{	j = srinc_set(t->tpe[i]);
		if (j >= GLOBAL && j != ALPHA_F)
			return -1;
		if (j != LOCAL)
		{	k = srunc(t->tpe[i], j);
			if (g == 0
			||  t->qu[0] != k
			||  t->ty[0] != j)
			{	t->qu[g] = k;
				t->ty[g] = j;
				g++;
	}	}	}
	return g;
}
#endif
void
retrans(int n, int m, int is, short srcln[], uchar reach[], uchar lpstate[])
	/* process n, with m states, is=initial state */
{	Trans *T0, *T1, *T2, *T3;
	Trans *T4, *T5; /* t_reverse or has_unless */
	int i;
#if defined(HAS_UNLESS) || !defined(NOREDUCE)
	int k;
#endif
#ifndef NOREDUCE
	int g, h, j, aa;
#endif
#ifdef HAS_UNLESS
	int p;
#endif
	if (state_tables >= 4)
	{	printf("STEP 1 %s\n", 
			procname[n]);
		for (i = 1; i < m; i++)
		for (T0 = trans[n][i]; T0; T0 = T0->nxt)
			crack(n, i, T0, srcln);
		return;
	}
	do {
		for (i = 1, cnt = 0; i < m; i++)
		{	T2 = trans[n][i];
			T1 = T2?T2->nxt:(Trans *)0;
/* prescan: */		for (T0 = T1; T0; T0 = T0->nxt)
/* choice in choice */	{	if (T0->st && trans[n][T0->st]
				&&  trans[n][T0->st]->nxt)
					break;
			}
#if 0
		if (T0)
		printf("\tstate %d / %d: choice in choice\n",
		i, T0->st);
#endif
			if (T0)
			for (T0 = T1; T0; T0 = T0->nxt)
			{	T3 = trans[n][T0->st];
				if (!T3->nxt)
				{	T2->nxt = cpytr(T0);
					T2 = T2->nxt;
					imed(T2, T0->st, n, i);
					continue;
				}
				do {	T3 = T3->nxt;
					T2->nxt = cpytr(T3);
					T2 = T2->nxt;
					imed(T2, T0->st, n, i);
				} while (T3->nxt);
				cnt++;
			}
		}
	} while (cnt);
	if (state_tables >= 3)
	{	printf("STEP 2 %s\n", 
			procname[n]);
		for (i = 1; i < m; i++)
		for (T0 = trans[n][i]; T0; T0 = T0->nxt)
			crack(n, i, T0, srcln);
		return;
	}
	for (i = 1; i < m; i++)
	{	if (trans[n][i] && trans[n][i]->nxt) /* optimize */
		{	T1 = trans[n][i]->nxt;
#if 0
			printf("\t\tpull %d (%d) to %d\n",
			T1->st, T1->forw, i);
#endif
			srcln[i] = srcln[T1->st];	/* Oyvind Teig, 5.2.0 */

			if (!trans[n][T1->st]) continue;
			T0 = cpytr(trans[n][T1->st]);
			trans[n][i] = T0;
			reach[T1->st] = 1;
			imed(T0, T1->st, n, i);
			for (T1 = T1->nxt; T1; T1 = T1->nxt)
			{
#if 0
			printf("\t\tpull %d (%d) to %d\n",
				T1->st, T1->forw, i);
#endif
		/*		srcln[i] = srcln[T1->st];  gh: not useful */
				if (!trans[n][T1->st]) continue;
				T0->nxt = cpytr(trans[n][T1->st]);
				T0 = T0->nxt;
				reach[T1->st] = 1;
				imed(T0, T1->st, n, i);
	}	}	}
	if (state_tables >= 2)
	{	printf("STEP 3 %s\n", 
			procname[n]);
		for (i = 1; i < m; i++)
		for (T0 = trans[n][i]; T0; T0 = T0->nxt)
			crack(n, i, T0, srcln);
		return;
	}
#ifdef HAS_UNLESS
	for (i = 1; i < m; i++)
	{	if (!trans[n][i]) continue;
		/* check for each state i if an
		 * escape to some state p is defined
		 * if so, copy and mark p's transitions
		 * and prepend them to the transition-
		 * list of state i
		 */
	 if (!like_java) /* the default */
	 {	for (T0 = trans[n][i]; T0; T0 = T0->nxt)
		for (k = HAS_UNLESS-1; k >= 0; k--)
		{	if (p = T0->escp[k])
			for (T1 = trans[n][p]; T1; T1 = T1->nxt)
			{	if (isthere(trans[n][i], T1->t_id))
					continue;
				T2 = cpytr(T1);
				T2->e_trans = p;
				T2->nxt = trans[n][i];
				trans[n][i] = T2;
		}	}
	 } else /* outermost unless checked first */
	 {	T4 = T3 = (Trans *) 0;
		for (T0 = trans[n][i]; T0; T0 = T0->nxt)
		for (k = HAS_UNLESS-1; k >= 0; k--)
		{	if (p = T0->escp[k])
			for (T1 = trans[n][p]; T1; T1 = T1->nxt)
			{	if (isthere(trans[n][i], T1->t_id))
					continue;
				T2 = cpytr(T1);
				T2->nxt = (Trans *) 0;
				T2->e_trans = p;
				if (T3)	T3->nxt = T2;
				else	T4 = T2;
				T3 = T2;
		}	}
		if (T4)
		{	T3->nxt = trans[n][i];
			trans[n][i] = T4;
		}
	 }
	}
#endif
#ifndef NOREDUCE
	for (i = 1; i < m; i++)
	{	if (a_cycles)
		{ /* moves through these states are visible */
	#if PROG_LAB>0 && defined(HAS_NP)
			if (progstate[n][i])
				goto degrade;
			for (T1 = trans[n][i]; T1; T1 = T1->nxt)
				if (progstate[n][T1->st])
					goto degrade;
	#endif
			if (accpstate[n][i] || visstate[n][i])
				goto degrade;
			for (T1 = trans[n][i]; T1; T1 = T1->nxt)
				if (accpstate[n][T1->st])
					goto degrade;
		}
		T1 = trans[n][i];
		if (!T1) continue;
		g = mark_safety(T1);	/* V3.3.1 */
		if (g < 0) goto degrade; /* global */
		/* check if mixing of guards preserves reduction */
		if (T1->nxt)
		{	k = 0;
			for (T0 = T1; T0; T0 = T0->nxt)
			{	if (!(T0->atom&8))
					goto degrade;
				for (aa = 0; aa < 2; aa++)
				{	j = srinc_set(T0->tpe[aa]);
					if (j >= GLOBAL && j != ALPHA_F)
						goto degrade;
					if (T0->tpe[aa]
					&&  T0->tpe[aa]
					!=  T1->tpe[0])
						k = 1;
			}	}
			/* g = 0;	V3.3.1 */
			if (k)	/* non-uniform selection */
			for (T0 = T1; T0; T0 = T0->nxt)
			for (aa = 0; aa < 2; aa++)
			{	j = srinc_set(T0->tpe[aa]);
				if (j != LOCAL)
				{	k = srunc(T0->tpe[aa], j);
					for (h = 0; h < 6; h++)
						if (T1->qu[h] == k
						&&  T1->ty[h] == j)
							break;
					if (h >= 6)
					{	T1->qu[g%6] = k;
						T1->ty[g%6] = j;
						g++;
			}	}	}
			if (g > 6)
			{	T1->qu[0] = 0;	/* turn it off */
				printf("pan: warning, line %d, ",
					srcln[i]);
			 	printf("too many stmnt types (%d)",
					g);
			  	printf(" in selection\n");
			  goto degrade;
			}
		}
		/* mark all options global if >=1 is global */
		for (T1 = trans[n][i]; T1; T1 = T1->nxt)
			if (!(T1->atom&8)) break;
		if (T1)
degrade:	for (T1 = trans[n][i]; T1; T1 = T1->nxt)
			T1->atom &= ~8;	/* mark as unsafe */
		/* can only mix 'r's or 's's if on same chan */
		/* and not mixed with other local operations */
		T1 = trans[n][i];
		if (!T1 || T1->qu[0]) continue;
		j = T1->tpe[0];
		if (T1->nxt && T1->atom&8)
		{ if (j == 5*DELTA)
		  {	printf("warning: line %d ", srcln[i]);
			printf("mixed condition ");
			printf("(defeats reduction)\n");
			goto degrade;
		  }
		  for (T0 = T1; T0; T0 = T0->nxt)
		  for (aa = 0; aa < 2; aa++)
		  if  (T0->tpe[aa] && T0->tpe[aa] != j)
		  {	printf("warning: line %d ", srcln[i]);
			printf("[%d-%d] mixed %stion ",
				T0->tpe[aa], j, 
				(j==5*DELTA)?"condi":"selec");
			printf("(defeats reduction)\n");
			printf("	'%s' <-> '%s'\n",
				T1->tp, T0->tp);
			goto degrade;
		} }
	}
#endif
	for (i = 1; i < m; i++)
	{	T2 = trans[n][i];
		if (!T2
		||  T2->nxt
		||  strncmp(T2->tp, ".(goto)", 7)
		||  !stopstate[n][i])
			continue;
		stopstate[n][T2->st] = 1;
	}
	if (state_tables && !verbose)
	{	if (dodot)
		{	char buf[256], *q = buf, *p = procname[n];
			while (*p != '\0')
			{	if (*p != ':')
				{	*q++ = *p;
				}
				p++;
			}
			*q = '\0';
			printf("digraph ");
			switch (Btypes[n]) {
			case I_PROC:  printf("init {\n"); break;
			case N_CLAIM: printf("claim_%s {\n", buf); break;
			case E_TRACE: printf("notrace {\n"); break;
			case N_TRACE: printf("trace {\n"); break;
			default:      printf("p_%s {\n", buf); break;
			}
			printf("size=\"8,10\";\n");
			printf("  GT [shape=box,style=dotted,label=\"%s\"];\n", buf);
		} else
		{	switch (Btypes[n]) {
			case I_PROC:  printf("init\n"); break;
			case N_CLAIM: printf("claim %s\n", procname[n]); break;
			case E_TRACE: printf("notrace assertion\n"); break;
			case N_TRACE: printf("trace assertion\n"); break;
			default:      printf("proctype %s\n", procname[n]); break;
		}	}
		for (i = 1; i < m; i++)
		{	reach[i] = 1;
		}
		tagtable(n, m, is, srcln, reach);
		if (dodot) printf("}\n");
	} else
	for (i = 1; i < m; i++)
	{	int nrelse;
		if (Btypes[n] != N_CLAIM)
		{	for (T0 = trans[n][i]; T0; T0 = T0->nxt)
			{	if (T0->st == i
				&& strcmp(T0->tp, "(1)") == 0)
				{	printf("error: proctype '%s' ",
						procname[n]);
		  			printf("line %d, state %d: has un",
						srcln[i], i);
					printf("conditional self-loop\n");
					pan_exit(1);
		}	}	}
		nrelse = 0;
		for (T0 = trans[n][i]; T0; T0 = T0->nxt)
		{	if (strcmp(T0->tp, "else") == 0)
				nrelse++;
		}
		if (nrelse > 1)
		{	printf("error: proctype '%s' state",
				procname[n]);
		  	printf(" %d, inherits %d", i, nrelse);
		  	printf(" 'else' stmnts\n");
			pan_exit(1);
	}	}
#if !defined(LOOPSTATE) && !defined(BFS_PAR)
	if (state_tables)
#endif
	do_dfs(n, m, is, srcln, reach, lpstate);

	if (!t_reverse)
	{	return;
	}
	/* process n, with m states, is=initial state -- reverse list */
	if (!state_tables && Btypes[n] != N_CLAIM)
	{	for (i = 1; i < m; i++)
		{	Trans *Tx = (Trans *) 0; /* list of escapes */
			Trans *Ty = (Trans *) 0; /* its tail element */
			T1 = (Trans *) 0; /* reversed list */
			T2 = (Trans *) 0; /* its tail */
			T3 = (Trans *) 0; /* remembers possible 'else' */

			/* find unless-escapes, they should go first */
			T4 = T5 = T0 = trans[n][i];
	#ifdef HAS_UNLESS
			while (T4 && T4->e_trans) /* escapes are first in orig list */
			{	T5 = T4;	  /* remember predecessor */
				T4 = T4->nxt;
			}
	#endif
			/* T4 points to first non-escape, T5 to its parent, T0 to original list */
			if (T4 != T0)		 /* there was at least one escape */
			{	T3 = T5->nxt;		 /* start of non-escapes */
				T5->nxt = (Trans *) 0;	 /* separate */
				Tx = T0;		 /* start of the escapes */
				Ty = T5;		 /* its tail */
				T0 = T3;		 /* the rest, to be reversed */
			}
			/* T0 points to first non-escape, Tx to the list of escapes, Ty to its tail */

			/* first tail-add non-escape transitions, reversed */
			T3 = (Trans *) 0;
			for (T5 = T0; T5; T5 = T4)
			{	T4 = T5->nxt;
	#ifdef HAS_UNLESS
				if (T5->e_trans)
				{	printf("error: cannot happen!\n");
					continue;
				}
	#endif
				if (strcmp(T5->tp, "else") == 0)
				{	T3 = T5;
					T5->nxt = (Trans *) 0;
				} else
				{	T5->nxt = T1;
					if (!T1) { T2 = T5; }
					T1 = T5;
			}	}
			/* T3 points to a possible else, which is removed from the list */
			/* T1 points to the reversed list so far (without escapes) */
			/* T2 points to the tail element -- where the else should go */
			if (T2 && T3)
			{	T2->nxt = T3;	/* add else */
			} else
			{	if (T3) /* there was an else, but there's no tail */
				{	if (!T1)	/* and no reversed list */
					{	T1 = T3; /* odd, but possible */
					} else		/* even stranger */
					{	T1->nxt = T3;
			}	}	}

			/* add in the escapes, to that they appear at the front */
			if (Tx && Ty) { Ty->nxt = T1; T1 = Tx; }

			trans[n][i] = T1;
			/* reversed, with escapes first and else last */
	}	}
	if (state_tables && verbose)
	{	printf("FINAL proctype %s\n", 
			procname[n]);
		for (i = 1; i < m; i++)
		for (T0 = trans[n][i]; T0; T0 = T0->nxt)
			crack(n, i, T0, srcln);
	}
}
void
imed(Trans *T, int v, int n, int j)	/* set intermediate state */
{	progstate[n][T->st] |= progstate[n][v];
	accpstate[n][T->st] |= accpstate[n][v];
	stopstate[n][T->st] |= stopstate[n][v];
	mapstate[n][j] = T->st;
}
void
tagtable(int n, int m, int is, short srcln[], uchar reach[])
{	Trans *z;

	if (is >= m || !trans[n][is]
	||  is <= 0 || reach[is] == 0)
		return;
	reach[is] = 0;
	if (state_tables)
	for (z = trans[n][is]; z; z = z->nxt)
	{	if (dodot)
			dot_crack(n, is, z);
		else
			crack(n, is, z, srcln);
	}

	for (z = trans[n][is]; z; z = z->nxt)
	{
#ifdef HAS_UNLESS
		int i, j;
#endif
		tagtable(n, m, z->st, srcln, reach);
#ifdef HAS_UNLESS
		for (i = 0; i < HAS_UNLESS; i++)
		{	j = trans[n][is]->escp[i];
			if (!j) break;
			tagtable(n, m, j, srcln, reach);
		}
#endif
	}
}

extern Trans *t_id_lkup[];

void
dfs_table(int n, int m, int is, short srcln[], uchar reach[], uchar lpstate[])
{	Trans *z;

	if (is >= m || is <= 0 || !trans[n][is])
		return;
	if ((reach[is] & (4|8|16)) != 0)
	{	if ((reach[is] & (8|16)) == 16)	/* on stack, not yet recorded */
		{	lpstate[is] = 1;
			reach[is] |= 8; /* recorded */
			if (state_tables && verbose)
			{	printf("state %d line %d is a loopstate\n", is, srcln[is]);
		}	}
		return;
	}
	reach[is] |= (4|16);	/* visited | onstack */
	for (z = trans[n][is]; z; z = z->nxt)
	{	t_id_lkup[z->t_id] = z;
#ifdef HAS_UNLESS
		int i, j;
#endif
		dfs_table(n, m, z->st, srcln, reach, lpstate);
#ifdef HAS_UNLESS
		for (i = 0; i < HAS_UNLESS; i++)
		{	j = trans[n][is]->escp[i];
			if (!j) break;
			dfs_table(n, m, j, srcln, reach, lpstate);
		}
#endif
	}
	reach[is] &= ~16; /* no longer on stack */
}
void
do_dfs(int n, int m, int is, short srcln[], uchar reach[], uchar lpstate[])
{	int i;
	dfs_table(n, m, is, srcln, reach, lpstate);
	for (i = 0; i < m; i++)
		reach[i] &= ~(4|8|16);
}
void
crack(int n, int j, Trans *z, short srcln[])
{	int i;

	if (!z) return;
	printf("	state %3d -(tr %3d)-> state %3d  ",
		j, z->forw, z->st);
	printf("[id %3d tp %3d", z->t_id, z->tpe[0]);
	if (z->tpe[1]) printf(",%d", z->tpe[1]);
#ifdef HAS_UNLESS
	if (z->e_trans)
		printf(" org %3d", z->e_trans);
	else if (state_tables >= 2)
	for (i = 0; i < HAS_UNLESS; i++)
	{	if (!z->escp[i]) break;
		printf(" esc %d", z->escp[i]);
	}
#endif
	printf("]");
	printf(" [%s%s%s%s%s] %s:%d => ",
		z->atom&6?"A":z->atom&32?"D":"-",
		accpstate[n][j]?"a" :"-",
		stopstate[n][j]?"e" : "-",
		progstate[n][j]?"p" : "-",
		z->atom & 8 ?"L":"G",
		PanSource, srcln[j]);
	for (i = 0; z->tp[i]; i++)
		if (z->tp[i] == '\n')
			printf("\\n");
		else
			putchar(z->tp[i]);
	if (verbose && z->qu[0])
	{	printf("\t[");
		for (i = 0; i < 6; i++)
			if (z->qu[i])
				printf("(%d,%d)",
				z->qu[i], z->ty[i]);
		printf("]");
	}
	printf("\n");
	fflush(stdout);
}
/* spin -a m.pml; cc -o pan pan.c; ./pan -D | dot -Tps > foo.ps; ps2pdf foo.ps */
void
dot_crack(int n, int j, Trans *z)
{	int i;

	if (!z) return;
	printf("	S%d -> S%d  [color=black", j, z->st);

	if (z->atom&6) printf(",style=dashed");
	else if (z->atom&32) printf(",style=dotted");
	else if (z->atom&8) printf(",style=solid");
	else printf(",style=bold");

	printf(",label=\"");
	for (i = 0; z->tp[i]; i++)
	{	if (z->tp[i] == '\\'
		&&  z->tp[i+1] == 'n')
		{	i++; printf(" ");
		} else
		{	putchar(z->tp[i]);
	}	}
	printf("\"];\n");
	if (accpstate[n][j]) printf("  S%d [color=red,style=bold];\n", j);
	else if (progstate[n][j]) printf("  S%d [color=green,style=bold];\n", j);
	if (stopstate[n][j]) printf("  S%d [color=blue,style=bold,shape=box];\n", j);
}

#ifdef VAR_RANGES
#define BYTESIZE	32	/* 2^8 : 2^3 = 256:8 = 32 */

typedef struct Vr_Ptr {
	char	*nm;
	uchar	vals[BYTESIZE];
	struct Vr_Ptr *nxt;
} Vr_Ptr;
Vr_Ptr *ranges = (Vr_Ptr *) 0;

void
logval(char *s, int v)
{	Vr_Ptr *tmp;

	if (v<0 || v > 255) return;
	for (tmp = ranges; tmp; tmp = tmp->nxt)
		if (!strcmp(tmp->nm, s))
			goto found;
	tmp = (Vr_Ptr *) emalloc(sizeof(Vr_Ptr));
	tmp->nxt = ranges;
	ranges = tmp;
	tmp->nm = s;
found:
	tmp->vals[(v)/8] |= 1<<((v)%8);
}

void
dumpval(uchar X[], int range)
{	int w, x, i, j = -1;

	for (w = i = 0; w < range; w++)
	for (x = 0; x < 8; x++, i++)
	{
from:		if ((X[w] & (1<<x)))
		{	printf("%d", i);
			j = i;
			goto upto;
	}	}
	return;
	for (w = 0; w < range; w++)
	for (x = 0; x < 8; x++, i++)
	{
upto:		if (!(X[w] & (1<<x)))
		{	if (i-1 == j)
				printf(", ");
			else
				printf("-%d, ", i-1);
			goto from;
	}	}
	if (j >= 0 && j != 255)
		printf("-255");
}

void
dumpranges(void)
{	Vr_Ptr *tmp;
	printf("\nValues assigned within ");
	printf("interval [0..255]:\n");
	for (tmp = ranges; tmp; tmp = tmp->nxt)
	{	printf("\t%s\t: ", tmp->nm);
		dumpval(tmp->vals, BYTESIZE);
		printf("\n");
	}
}
#endif
