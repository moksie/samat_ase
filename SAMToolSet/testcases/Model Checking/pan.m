#define rand	pan_rand
#if defined(HAS_CODE) && defined(VERBOSE)
	#ifdef BFS_PAR
		bfs_printf("Pr: %d Tr: %d\n", II, t->forw);
	#else
		cpu_printf("Pr: %d Tr: %d\n", II, t->forw);
	#endif
#endif
	switch (t->forw) {
	default: Uerror("bad forward move");
	case 0:	/* if without executable clauses */
		continue;
	case 1: /* generic 'goto' or 'skip' */
		IfNotBlocked
		_m = 3; goto P999;
	case 2: /* generic 'else' */
		IfNotBlocked
		if (trpt->o_pm&1) continue;
		_m = 3; goto P999;

		 /* CLAIM f */
	case 3: /* STATE 1 - _spin_nvr.tmp:3 - [(!((sum<=300)))] (6:0:0 - 1) */
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[3][1] = 1;
		if (!( !((now.sum<=300))))
			continue;
		/* merge: assert(!(!((sum<=300))))(0, 2, 6) */
		reached[3][2] = 1;
		spin_assert( !( !((now.sum<=300))), " !( !((sum<=300)))", II, tt, t);
		/* merge: .(goto)(0, 7, 6) */
		reached[3][7] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 4: /* STATE 10 - _spin_nvr.tmp:8 - [-end-] (0:0:0 - 1) */
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported10 = 0;
			if (verbose && !reported10)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported10 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported10 = 0;
			if (verbose && !reported10)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported10 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[3][10] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC :init: */
	case 5: /* STATE 1 - mondex.pml:596 - [ConPurse.ConPurse_field1 = 0] (0:16:15 - 1) */
		IfNotBlocked
		reached[2][1] = 1;
		(trpt+1)->bup.ovals = grab_ints(15);
		(trpt+1)->bup.ovals[0] = ((P2 *)this)->ConPurse.ConPurse_field1;
		((P2 *)this)->ConPurse.ConPurse_field1 = 0;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field1", ((P2 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		/* merge: ConPurse.ConPurse_field2 = 100(16, 2, 16) */
		reached[2][2] = 1;
		(trpt+1)->bup.ovals[1] = ((P2 *)this)->ConPurse.ConPurse_field2;
		((P2 *)this)->ConPurse.ConPurse_field2 = 100;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field2", ((P2 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		/* merge: ConPurse.ConPurse_field3 = 10(16, 3, 16) */
		reached[2][3] = 1;
		(trpt+1)->bup.ovals[2] = ((P2 *)this)->ConPurse.ConPurse_field3;
		((P2 *)this)->ConPurse.ConPurse_field3 = 10;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field3", ((P2 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		/* merge: ConPurse.ConPurse_field4 = 1(16, 4, 16) */
		reached[2][4] = 1;
		(trpt+1)->bup.ovals[3] = ((P2 *)this)->ConPurse.ConPurse_field4;
		((P2 *)this)->ConPurse.ConPurse_field4 = 1;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field4", ((P2 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		/* merge: ConPurse.ConPurse_field5 = 1(16, 5, 16) */
		reached[2][5] = 1;
		(trpt+1)->bup.ovals[4] = ((P2 *)this)->ConPurse.ConPurse_field5;
		((P2 *)this)->ConPurse.ConPurse_field5 = 1;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field5", ((P2 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		/* merge: ConPurse.ConPurse_field6 = 0(16, 6, 16) */
		reached[2][6] = 1;
		(trpt+1)->bup.ovals[5] = ((P2 *)this)->ConPurse.ConPurse_field6;
		((P2 *)this)->ConPurse.ConPurse_field6 = 0;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field6", ((P2 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		/* merge: ConPurse.ConPurse_field7 = 50(16, 7, 16) */
		reached[2][7] = 1;
		(trpt+1)->bup.ovals[6] = ((P2 *)this)->ConPurse.ConPurse_field7;
		((P2 *)this)->ConPurse.ConPurse_field7 = 50;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field7", ((P2 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		/* merge: ConPurse.ConPurse_field8 = 1(16, 8, 16) */
		reached[2][8] = 1;
		(trpt+1)->bup.ovals[7] = ((P2 *)this)->ConPurse.ConPurse_field8;
		((P2 *)this)->ConPurse.ConPurse_field8 = 1;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field8", ((P2 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		/* merge: ConPurse.ConPurse_field9 = 1(16, 9, 16) */
		reached[2][9] = 1;
		(trpt+1)->bup.ovals[8] = ((P2 *)this)->ConPurse.ConPurse_field9;
		((P2 *)this)->ConPurse.ConPurse_field9 = 1;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field9", ((P2 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		/* merge: ConPurse.ConPurse_field10 = 1(16, 10, 16) */
		reached[2][10] = 1;
		(trpt+1)->bup.ovals[9] = ((P2 *)this)->ConPurse.ConPurse_field10;
		((P2 *)this)->ConPurse.ConPurse_field10 = 1;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field10", ((P2 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		/* merge: ConPurse.ConPurse_field11 = 10(16, 11, 16) */
		reached[2][11] = 1;
		(trpt+1)->bup.ovals[10] = ((P2 *)this)->ConPurse.ConPurse_field11;
		((P2 *)this)->ConPurse.ConPurse_field11 = 10;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field11", ((P2 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		/* merge: ConPurse.ConPurse_field12 = 2(16, 12, 16) */
		reached[2][12] = 1;
		(trpt+1)->bup.ovals[11] = ((P2 *)this)->ConPurse.ConPurse_field12;
		((P2 *)this)->ConPurse.ConPurse_field12 = 2;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field12", ((P2 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		/* merge: ConPurse.ConPurse_field13 = 0(16, 13, 16) */
		reached[2][13] = 1;
		(trpt+1)->bup.ovals[12] = ((P2 *)this)->ConPurse.ConPurse_field13;
		((P2 *)this)->ConPurse.ConPurse_field13 = 0;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field13", ((P2 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		/* merge: ConPurse.ConPurse_field14 = 1(16, 14, 16) */
		reached[2][14] = 1;
		(trpt+1)->bup.ovals[13] = ((P2 *)this)->ConPurse.ConPurse_field14;
		((P2 *)this)->ConPurse.ConPurse_field14 = 1;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field14", ((P2 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		/* merge: ConPurse.ConPurse_field15 = 1(16, 15, 16) */
		reached[2][15] = 1;
		(trpt+1)->bup.ovals[14] = ((P2 *)this)->ConPurse.ConPurse_field15;
		((P2 *)this)->ConPurse.ConPurse_field15 = 1;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field15", ((P2 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		_m = 3; goto P999; /* 14 */
	case 6: /* STATE 16 - mondex.pml:611 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[2][16] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P2 *)this)->ConPurse.ConPurse_field1, ((P2 *)this)->ConPurse.ConPurse_field2, ((P2 *)this)->ConPurse.ConPurse_field3, ((P2 *)this)->ConPurse.ConPurse_field4, ((P2 *)this)->ConPurse.ConPurse_field5, ((P2 *)this)->ConPurse.ConPurse_field6, ((P2 *)this)->ConPurse.ConPurse_field7, ((P2 *)this)->ConPurse.ConPurse_field8, ((P2 *)this)->ConPurse.ConPurse_field9, ((P2 *)this)->ConPurse.ConPurse_field10, ((P2 *)this)->ConPurse.ConPurse_field11, ((P2 *)this)->ConPurse.ConPurse_field12, ((P2 *)this)->ConPurse.ConPurse_field13, ((P2 *)this)->ConPurse.ConPurse_field14, ((P2 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 7: /* STATE 17 - mondex.pml:612 - [ConPurse.ConPurse_field1 = 11] (0:32:15 - 1) */
		IfNotBlocked
		reached[2][17] = 1;
		(trpt+1)->bup.ovals = grab_ints(15);
		(trpt+1)->bup.ovals[0] = ((P2 *)this)->ConPurse.ConPurse_field1;
		((P2 *)this)->ConPurse.ConPurse_field1 = 11;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field1", ((P2 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		/* merge: ConPurse.ConPurse_field2 = 200(32, 18, 32) */
		reached[2][18] = 1;
		(trpt+1)->bup.ovals[1] = ((P2 *)this)->ConPurse.ConPurse_field2;
		((P2 *)this)->ConPurse.ConPurse_field2 = 200;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field2", ((P2 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		/* merge: ConPurse.ConPurse_field3 = 10(32, 19, 32) */
		reached[2][19] = 1;
		(trpt+1)->bup.ovals[2] = ((P2 *)this)->ConPurse.ConPurse_field3;
		((P2 *)this)->ConPurse.ConPurse_field3 = 10;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field3", ((P2 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		/* merge: ConPurse.ConPurse_field4 = 1(32, 20, 32) */
		reached[2][20] = 1;
		(trpt+1)->bup.ovals[3] = ((P2 *)this)->ConPurse.ConPurse_field4;
		((P2 *)this)->ConPurse.ConPurse_field4 = 1;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field4", ((P2 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		/* merge: ConPurse.ConPurse_field5 = 3(32, 21, 32) */
		reached[2][21] = 1;
		(trpt+1)->bup.ovals[4] = ((P2 *)this)->ConPurse.ConPurse_field5;
		((P2 *)this)->ConPurse.ConPurse_field5 = 3;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field5", ((P2 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		/* merge: ConPurse.ConPurse_field6 = 0(32, 22, 32) */
		reached[2][22] = 1;
		(trpt+1)->bup.ovals[5] = ((P2 *)this)->ConPurse.ConPurse_field6;
		((P2 *)this)->ConPurse.ConPurse_field6 = 0;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field6", ((P2 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		/* merge: ConPurse.ConPurse_field7 = 50(32, 23, 32) */
		reached[2][23] = 1;
		(trpt+1)->bup.ovals[6] = ((P2 *)this)->ConPurse.ConPurse_field7;
		((P2 *)this)->ConPurse.ConPurse_field7 = 50;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field7", ((P2 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		/* merge: ConPurse.ConPurse_field8 = 1(32, 24, 32) */
		reached[2][24] = 1;
		(trpt+1)->bup.ovals[7] = ((P2 *)this)->ConPurse.ConPurse_field8;
		((P2 *)this)->ConPurse.ConPurse_field8 = 1;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field8", ((P2 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		/* merge: ConPurse.ConPurse_field9 = 1(32, 25, 32) */
		reached[2][25] = 1;
		(trpt+1)->bup.ovals[8] = ((P2 *)this)->ConPurse.ConPurse_field9;
		((P2 *)this)->ConPurse.ConPurse_field9 = 1;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field9", ((P2 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		/* merge: ConPurse.ConPurse_field10 = 1(32, 26, 32) */
		reached[2][26] = 1;
		(trpt+1)->bup.ovals[9] = ((P2 *)this)->ConPurse.ConPurse_field10;
		((P2 *)this)->ConPurse.ConPurse_field10 = 1;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field10", ((P2 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		/* merge: ConPurse.ConPurse_field11 = 2(32, 27, 32) */
		reached[2][27] = 1;
		(trpt+1)->bup.ovals[10] = ((P2 *)this)->ConPurse.ConPurse_field11;
		((P2 *)this)->ConPurse.ConPurse_field11 = 2;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field11", ((P2 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		/* merge: ConPurse.ConPurse_field12 = 2(32, 28, 32) */
		reached[2][28] = 1;
		(trpt+1)->bup.ovals[11] = ((P2 *)this)->ConPurse.ConPurse_field12;
		((P2 *)this)->ConPurse.ConPurse_field12 = 2;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field12", ((P2 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		/* merge: ConPurse.ConPurse_field13 = 0(32, 29, 32) */
		reached[2][29] = 1;
		(trpt+1)->bup.ovals[12] = ((P2 *)this)->ConPurse.ConPurse_field13;
		((P2 *)this)->ConPurse.ConPurse_field13 = 0;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field13", ((P2 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		/* merge: ConPurse.ConPurse_field14 = 1(32, 30, 32) */
		reached[2][30] = 1;
		(trpt+1)->bup.ovals[13] = ((P2 *)this)->ConPurse.ConPurse_field14;
		((P2 *)this)->ConPurse.ConPurse_field14 = 1;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field14", ((P2 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		/* merge: ConPurse.ConPurse_field15 = 1(32, 31, 32) */
		reached[2][31] = 1;
		(trpt+1)->bup.ovals[14] = ((P2 *)this)->ConPurse.ConPurse_field15;
		((P2 *)this)->ConPurse.ConPurse_field15 = 1;
#ifdef VAR_RANGES
		logval(":init::ConPurse.ConPurse_field15", ((P2 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		_m = 3; goto P999; /* 14 */
	case 8: /* STATE 32 - mondex.pml:627 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[2][32] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P2 *)this)->ConPurse.ConPurse_field1, ((P2 *)this)->ConPurse.ConPurse_field2, ((P2 *)this)->ConPurse.ConPurse_field3, ((P2 *)this)->ConPurse.ConPurse_field4, ((P2 *)this)->ConPurse.ConPurse_field5, ((P2 *)this)->ConPurse.ConPurse_field6, ((P2 *)this)->ConPurse.ConPurse_field7, ((P2 *)this)->ConPurse.ConPurse_field8, ((P2 *)this)->ConPurse.ConPurse_field9, ((P2 *)this)->ConPurse.ConPurse_field10, ((P2 *)this)->ConPurse.ConPurse_field11, ((P2 *)this)->ConPurse.ConPurse_field12, ((P2 *)this)->ConPurse.ConPurse_field13, ((P2 *)this)->ConPurse.ConPurse_field14, ((P2 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 9: /* STATE 33 - mondex.pml:629 - [] (0:46:10 - 1) */
		IfNotBlocked
		reached[2][33] = 1;
		;
		/* merge: (46, 34, 46) */
		reached[2][34] = 1;
		;
		/* merge: (46, 35, 46) */
		reached[2][35] = 1;
		;
		/* merge: msg_in.msg_in_field1 = 0(46, 36, 46) */
		reached[2][36] = 1;
		(trpt+1)->bup.ovals = grab_ints(10);
		(trpt+1)->bup.ovals[0] = ((P2 *)this)->msg_in.msg_in_field1;
		((P2 *)this)->msg_in.msg_in_field1 = 0;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field1", ((P2 *)this)->msg_in.msg_in_field1);
#endif
		;
		/* merge: msg_in.msg_in_field2 = 3(46, 37, 46) */
		reached[2][37] = 1;
		(trpt+1)->bup.ovals[1] = ((P2 *)this)->msg_in.msg_in_field2;
		((P2 *)this)->msg_in.msg_in_field2 = 3;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field2", ((P2 *)this)->msg_in.msg_in_field2);
#endif
		;
		/* merge: msg_in.msg_in_field3 = 50(46, 38, 46) */
		reached[2][38] = 1;
		(trpt+1)->bup.ovals[2] = ((P2 *)this)->msg_in.msg_in_field3;
		((P2 *)this)->msg_in.msg_in_field3 = 50;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field3", ((P2 *)this)->msg_in.msg_in_field3);
#endif
		;
		/* merge: msg_in.msg_in_field4 = 1(46, 39, 46) */
		reached[2][39] = 1;
		(trpt+1)->bup.ovals[3] = ((P2 *)this)->msg_in.msg_in_field4;
		((P2 *)this)->msg_in.msg_in_field4 = 1;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field4", ((P2 *)this)->msg_in.msg_in_field4);
#endif
		;
		/* merge: msg_in.msg_in_field5 = 0(46, 40, 46) */
		reached[2][40] = 1;
		(trpt+1)->bup.ovals[4] = ((P2 *)this)->msg_in.msg_in_field5;
		((P2 *)this)->msg_in.msg_in_field5 = 0;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field5", ((P2 *)this)->msg_in.msg_in_field5);
#endif
		;
		/* merge: msg_in.msg_in_field6 = 3(46, 41, 46) */
		reached[2][41] = 1;
		(trpt+1)->bup.ovals[5] = ((P2 *)this)->msg_in.msg_in_field6;
		((P2 *)this)->msg_in.msg_in_field6 = 3;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field6", ((P2 *)this)->msg_in.msg_in_field6);
#endif
		;
		/* merge: msg_in.msg_in_field7 = 50(46, 42, 46) */
		reached[2][42] = 1;
		(trpt+1)->bup.ovals[6] = ((P2 *)this)->msg_in.msg_in_field7;
		((P2 *)this)->msg_in.msg_in_field7 = 50;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field7", ((P2 *)this)->msg_in.msg_in_field7);
#endif
		;
		/* merge: msg_in.msg_in_field8 = 1(46, 43, 46) */
		reached[2][43] = 1;
		(trpt+1)->bup.ovals[7] = ((P2 *)this)->msg_in.msg_in_field8;
		((P2 *)this)->msg_in.msg_in_field8 = 1;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field8", ((P2 *)this)->msg_in.msg_in_field8);
#endif
		;
		/* merge: msg_in.msg_in_field9 = 1(46, 44, 46) */
		reached[2][44] = 1;
		(trpt+1)->bup.ovals[8] = ((P2 *)this)->msg_in.msg_in_field9;
		((P2 *)this)->msg_in.msg_in_field9 = 1;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field9", ((P2 *)this)->msg_in.msg_in_field9);
#endif
		;
		/* merge: msg_in.msg_in_field10 = 0(46, 45, 46) */
		reached[2][45] = 1;
		(trpt+1)->bup.ovals[9] = ((P2 *)this)->msg_in.msg_in_field10;
		((P2 *)this)->msg_in.msg_in_field10 = 0;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field10", ((P2 *)this)->msg_in.msg_in_field10);
#endif
		;
		_m = 3; goto P999; /* 12 */
	case 10: /* STATE 46 - mondex.pml:641 - [place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:0 - 1) */
		IfNotBlocked
		reached[2][46] = 1;
		if (q_full(now.place_msg_in))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_in);
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_in, 0, ((P2 *)this)->msg_in.msg_in_field1, ((P2 *)this)->msg_in.msg_in_field2, ((P2 *)this)->msg_in.msg_in_field3, ((P2 *)this)->msg_in.msg_in_field4, ((P2 *)this)->msg_in.msg_in_field5, ((P2 *)this)->msg_in.msg_in_field6, ((P2 *)this)->msg_in.msg_in_field7, ((P2 *)this)->msg_in.msg_in_field8, ((P2 *)this)->msg_in.msg_in_field9, ((P2 *)this)->msg_in.msg_in_field10, 0, 0, 0, 0, 0, 10);
		_m = 2; goto P999; /* 0 */
	case 11: /* STATE 47 - mondex.pml:642 - [msg_in.msg_in_field1 = 7] (0:57:10 - 1) */
		IfNotBlocked
		reached[2][47] = 1;
		(trpt+1)->bup.ovals = grab_ints(10);
		(trpt+1)->bup.ovals[0] = ((P2 *)this)->msg_in.msg_in_field1;
		((P2 *)this)->msg_in.msg_in_field1 = 7;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field1", ((P2 *)this)->msg_in.msg_in_field1);
#endif
		;
		/* merge: msg_in.msg_in_field2 = 0(57, 48, 57) */
		reached[2][48] = 1;
		(trpt+1)->bup.ovals[1] = ((P2 *)this)->msg_in.msg_in_field2;
		((P2 *)this)->msg_in.msg_in_field2 = 0;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field2", ((P2 *)this)->msg_in.msg_in_field2);
#endif
		;
		/* merge: msg_in.msg_in_field3 = 50(57, 49, 57) */
		reached[2][49] = 1;
		(trpt+1)->bup.ovals[2] = ((P2 *)this)->msg_in.msg_in_field3;
		((P2 *)this)->msg_in.msg_in_field3 = 50;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field3", ((P2 *)this)->msg_in.msg_in_field3);
#endif
		;
		/* merge: msg_in.msg_in_field4 = 1(57, 50, 57) */
		reached[2][50] = 1;
		(trpt+1)->bup.ovals[3] = ((P2 *)this)->msg_in.msg_in_field4;
		((P2 *)this)->msg_in.msg_in_field4 = 1;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field4", ((P2 *)this)->msg_in.msg_in_field4);
#endif
		;
		/* merge: msg_in.msg_in_field5 = 0(57, 51, 57) */
		reached[2][51] = 1;
		(trpt+1)->bup.ovals[4] = ((P2 *)this)->msg_in.msg_in_field5;
		((P2 *)this)->msg_in.msg_in_field5 = 0;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field5", ((P2 *)this)->msg_in.msg_in_field5);
#endif
		;
		/* merge: msg_in.msg_in_field6 = 3(57, 52, 57) */
		reached[2][52] = 1;
		(trpt+1)->bup.ovals[5] = ((P2 *)this)->msg_in.msg_in_field6;
		((P2 *)this)->msg_in.msg_in_field6 = 3;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field6", ((P2 *)this)->msg_in.msg_in_field6);
#endif
		;
		/* merge: msg_in.msg_in_field7 = 50(57, 53, 57) */
		reached[2][53] = 1;
		(trpt+1)->bup.ovals[6] = ((P2 *)this)->msg_in.msg_in_field7;
		((P2 *)this)->msg_in.msg_in_field7 = 50;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field7", ((P2 *)this)->msg_in.msg_in_field7);
#endif
		;
		/* merge: msg_in.msg_in_field8 = 1(57, 54, 57) */
		reached[2][54] = 1;
		(trpt+1)->bup.ovals[7] = ((P2 *)this)->msg_in.msg_in_field8;
		((P2 *)this)->msg_in.msg_in_field8 = 1;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field8", ((P2 *)this)->msg_in.msg_in_field8);
#endif
		;
		/* merge: msg_in.msg_in_field9 = 1(57, 55, 57) */
		reached[2][55] = 1;
		(trpt+1)->bup.ovals[8] = ((P2 *)this)->msg_in.msg_in_field9;
		((P2 *)this)->msg_in.msg_in_field9 = 1;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field9", ((P2 *)this)->msg_in.msg_in_field9);
#endif
		;
		/* merge: msg_in.msg_in_field10 = 3(57, 56, 57) */
		reached[2][56] = 1;
		(trpt+1)->bup.ovals[9] = ((P2 *)this)->msg_in.msg_in_field10;
		((P2 *)this)->msg_in.msg_in_field10 = 3;
#ifdef VAR_RANGES
		logval(":init::msg_in.msg_in_field10", ((P2 *)this)->msg_in.msg_in_field10);
#endif
		;
		_m = 3; goto P999; /* 9 */
	case 12: /* STATE 57 - mondex.pml:652 - [place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:0 - 1) */
		IfNotBlocked
		reached[2][57] = 1;
		if (q_full(now.place_msg_in))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_in);
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P2 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_in, 0, ((P2 *)this)->msg_in.msg_in_field1, ((P2 *)this)->msg_in.msg_in_field2, ((P2 *)this)->msg_in.msg_in_field3, ((P2 *)this)->msg_in.msg_in_field4, ((P2 *)this)->msg_in.msg_in_field5, ((P2 *)this)->msg_in.msg_in_field6, ((P2 *)this)->msg_in.msg_in_field7, ((P2 *)this)->msg_in.msg_in_field8, ((P2 *)this)->msg_in.msg_in_field9, ((P2 *)this)->msg_in.msg_in_field10, 0, 0, 0, 0, 0, 10);
		_m = 2; goto P999; /* 0 */
	case 13: /* STATE 58 - mondex.pml:653 - [(run Main())] (0:0:0 - 1) */
		IfNotBlocked
		reached[2][58] = 1;
		if (!(addproc(II, 0)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 14: /* STATE 59 - mondex.pml:654 - [-end-] (0:0:0 - 1) */
		IfNotBlocked
		reached[2][59] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC check */
	case 15: /* STATE 1 - mondex.pml:588 - [place_ConPurse??<purse1.ConPurse_field1,purse1.ConPurse_field2,purse1.ConPurse_field3,purse1.ConPurse_field4,purse1.ConPurse_field5,purse1.ConPurse_field6,purse1.ConPurse_field7,purse1.ConPurse_field8,purse1.ConPurse_field9,purse1.ConPurse_field10,purse1.ConPurse_field11,purse1.ConPurse_field12,purse1.ConPurse_field13,purse1.ConPurse_field14,purse1.ConPurse_field15>] (0:0:16 - 1) */
		reached[1][1] = 1;
		if (q_len(now.place_ConPurse) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.place_ConPurse, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))) continue;
		(trpt+1)->bup.ovals = grab_ints(16);
		(trpt+1)->bup.ovals[0] = XX;
		(trpt+1)->bup.ovals[1] = now.purse1.ConPurse_field1;
		(trpt+1)->bup.ovals[2] = now.purse1.ConPurse_field2;
		(trpt+1)->bup.ovals[3] = now.purse1.ConPurse_field3;
		(trpt+1)->bup.ovals[4] = now.purse1.ConPurse_field4;
		(trpt+1)->bup.ovals[5] = now.purse1.ConPurse_field5;
		(trpt+1)->bup.ovals[6] = now.purse1.ConPurse_field6;
		(trpt+1)->bup.ovals[7] = now.purse1.ConPurse_field7;
		(trpt+1)->bup.ovals[8] = now.purse1.ConPurse_field8;
		(trpt+1)->bup.ovals[9] = now.purse1.ConPurse_field9;
		(trpt+1)->bup.ovals[10] = now.purse1.ConPurse_field10;
		(trpt+1)->bup.ovals[11] = now.purse1.ConPurse_field11;
		(trpt+1)->bup.ovals[12] = now.purse1.ConPurse_field12;
		(trpt+1)->bup.ovals[13] = now.purse1.ConPurse_field13;
		(trpt+1)->bup.ovals[14] = now.purse1.ConPurse_field14;
		(trpt+1)->bup.ovals[15] = now.purse1.ConPurse_field15;
		;
		now.purse1.ConPurse_field1 = qrecv(now.place_ConPurse, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("purse1.ConPurse_field1", now.purse1.ConPurse_field1);
#endif
		;
		now.purse1.ConPurse_field2 = qrecv(now.place_ConPurse, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("purse1.ConPurse_field2", now.purse1.ConPurse_field2);
#endif
		;
		now.purse1.ConPurse_field3 = qrecv(now.place_ConPurse, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("purse1.ConPurse_field3", now.purse1.ConPurse_field3);
#endif
		;
		now.purse1.ConPurse_field4 = qrecv(now.place_ConPurse, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("purse1.ConPurse_field4", now.purse1.ConPurse_field4);
#endif
		;
		now.purse1.ConPurse_field5 = qrecv(now.place_ConPurse, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("purse1.ConPurse_field5", now.purse1.ConPurse_field5);
#endif
		;
		now.purse1.ConPurse_field6 = qrecv(now.place_ConPurse, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("purse1.ConPurse_field6", now.purse1.ConPurse_field6);
#endif
		;
		now.purse1.ConPurse_field7 = qrecv(now.place_ConPurse, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("purse1.ConPurse_field7", now.purse1.ConPurse_field7);
#endif
		;
		now.purse1.ConPurse_field8 = qrecv(now.place_ConPurse, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("purse1.ConPurse_field8", now.purse1.ConPurse_field8);
#endif
		;
		now.purse1.ConPurse_field9 = qrecv(now.place_ConPurse, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("purse1.ConPurse_field9", now.purse1.ConPurse_field9);
#endif
		;
		now.purse1.ConPurse_field10 = qrecv(now.place_ConPurse, XX-1, 9, 0);
#ifdef VAR_RANGES
		logval("purse1.ConPurse_field10", now.purse1.ConPurse_field10);
#endif
		;
		now.purse1.ConPurse_field11 = qrecv(now.place_ConPurse, XX-1, 10, 0);
#ifdef VAR_RANGES
		logval("purse1.ConPurse_field11", now.purse1.ConPurse_field11);
#endif
		;
		now.purse1.ConPurse_field12 = qrecv(now.place_ConPurse, XX-1, 11, 0);
#ifdef VAR_RANGES
		logval("purse1.ConPurse_field12", now.purse1.ConPurse_field12);
#endif
		;
		now.purse1.ConPurse_field13 = qrecv(now.place_ConPurse, XX-1, 12, 0);
#ifdef VAR_RANGES
		logval("purse1.ConPurse_field13", now.purse1.ConPurse_field13);
#endif
		;
		now.purse1.ConPurse_field14 = qrecv(now.place_ConPurse, XX-1, 13, 0);
#ifdef VAR_RANGES
		logval("purse1.ConPurse_field14", now.purse1.ConPurse_field14);
#endif
		;
		now.purse1.ConPurse_field15 = qrecv(now.place_ConPurse, XX-1, 14, 0);
#ifdef VAR_RANGES
		logval("purse1.ConPurse_field15", now.purse1.ConPurse_field15);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_ConPurse);
		sprintf(simtmp, "%d", now.purse1.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse1.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse1.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse1.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse1.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse1.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse1.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse1.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse1.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse1.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse1.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse1.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse1.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse1.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse1.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 16: /* STATE 2 - mondex.pml:589 - [place_ConPurse??<purse2.ConPurse_field1,purse2.ConPurse_field2,purse2.ConPurse_field3,purse2.ConPurse_field4,purse2.ConPurse_field5,purse2.ConPurse_field6,purse2.ConPurse_field7,purse2.ConPurse_field8,purse2.ConPurse_field9,purse2.ConPurse_field10,purse2.ConPurse_field11,purse2.ConPurse_field12,purse2.ConPurse_field13,purse2.ConPurse_field14,purse2.ConPurse_field15>] (0:0:16 - 1) */
		reached[1][2] = 1;
		if (q_len(now.place_ConPurse) == 0) continue;

		XX=1;
		if (!(XX = Q_has(now.place_ConPurse, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))) continue;
		(trpt+1)->bup.ovals = grab_ints(16);
		(trpt+1)->bup.ovals[0] = XX;
		(trpt+1)->bup.ovals[1] = now.purse2.ConPurse_field1;
		(trpt+1)->bup.ovals[2] = now.purse2.ConPurse_field2;
		(trpt+1)->bup.ovals[3] = now.purse2.ConPurse_field3;
		(trpt+1)->bup.ovals[4] = now.purse2.ConPurse_field4;
		(trpt+1)->bup.ovals[5] = now.purse2.ConPurse_field5;
		(trpt+1)->bup.ovals[6] = now.purse2.ConPurse_field6;
		(trpt+1)->bup.ovals[7] = now.purse2.ConPurse_field7;
		(trpt+1)->bup.ovals[8] = now.purse2.ConPurse_field8;
		(trpt+1)->bup.ovals[9] = now.purse2.ConPurse_field9;
		(trpt+1)->bup.ovals[10] = now.purse2.ConPurse_field10;
		(trpt+1)->bup.ovals[11] = now.purse2.ConPurse_field11;
		(trpt+1)->bup.ovals[12] = now.purse2.ConPurse_field12;
		(trpt+1)->bup.ovals[13] = now.purse2.ConPurse_field13;
		(trpt+1)->bup.ovals[14] = now.purse2.ConPurse_field14;
		(trpt+1)->bup.ovals[15] = now.purse2.ConPurse_field15;
		;
		now.purse2.ConPurse_field1 = qrecv(now.place_ConPurse, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("purse2.ConPurse_field1", now.purse2.ConPurse_field1);
#endif
		;
		now.purse2.ConPurse_field2 = qrecv(now.place_ConPurse, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("purse2.ConPurse_field2", now.purse2.ConPurse_field2);
#endif
		;
		now.purse2.ConPurse_field3 = qrecv(now.place_ConPurse, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("purse2.ConPurse_field3", now.purse2.ConPurse_field3);
#endif
		;
		now.purse2.ConPurse_field4 = qrecv(now.place_ConPurse, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("purse2.ConPurse_field4", now.purse2.ConPurse_field4);
#endif
		;
		now.purse2.ConPurse_field5 = qrecv(now.place_ConPurse, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("purse2.ConPurse_field5", now.purse2.ConPurse_field5);
#endif
		;
		now.purse2.ConPurse_field6 = qrecv(now.place_ConPurse, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("purse2.ConPurse_field6", now.purse2.ConPurse_field6);
#endif
		;
		now.purse2.ConPurse_field7 = qrecv(now.place_ConPurse, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("purse2.ConPurse_field7", now.purse2.ConPurse_field7);
#endif
		;
		now.purse2.ConPurse_field8 = qrecv(now.place_ConPurse, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("purse2.ConPurse_field8", now.purse2.ConPurse_field8);
#endif
		;
		now.purse2.ConPurse_field9 = qrecv(now.place_ConPurse, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("purse2.ConPurse_field9", now.purse2.ConPurse_field9);
#endif
		;
		now.purse2.ConPurse_field10 = qrecv(now.place_ConPurse, XX-1, 9, 0);
#ifdef VAR_RANGES
		logval("purse2.ConPurse_field10", now.purse2.ConPurse_field10);
#endif
		;
		now.purse2.ConPurse_field11 = qrecv(now.place_ConPurse, XX-1, 10, 0);
#ifdef VAR_RANGES
		logval("purse2.ConPurse_field11", now.purse2.ConPurse_field11);
#endif
		;
		now.purse2.ConPurse_field12 = qrecv(now.place_ConPurse, XX-1, 11, 0);
#ifdef VAR_RANGES
		logval("purse2.ConPurse_field12", now.purse2.ConPurse_field12);
#endif
		;
		now.purse2.ConPurse_field13 = qrecv(now.place_ConPurse, XX-1, 12, 0);
#ifdef VAR_RANGES
		logval("purse2.ConPurse_field13", now.purse2.ConPurse_field13);
#endif
		;
		now.purse2.ConPurse_field14 = qrecv(now.place_ConPurse, XX-1, 13, 0);
#ifdef VAR_RANGES
		logval("purse2.ConPurse_field14", now.purse2.ConPurse_field14);
#endif
		;
		now.purse2.ConPurse_field15 = qrecv(now.place_ConPurse, XX-1, 14, 0);
#ifdef VAR_RANGES
		logval("purse2.ConPurse_field15", now.purse2.ConPurse_field15);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_ConPurse);
		sprintf(simtmp, "%d", now.purse2.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse2.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse2.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse2.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse2.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse2.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse2.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse2.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse2.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse2.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse2.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse2.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse2.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse2.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", now.purse2.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 17: /* STATE 3 - mondex.pml:590 - [((purse1.ConPurse_field1!=purse2.ConPurse_field1))] (0:0:0 - 1) */
		IfNotBlocked
		reached[1][3] = 1;
		if (!((now.purse1.ConPurse_field1!=now.purse2.ConPurse_field1)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 18: /* STATE 4 - mondex.pml:590 - [sum = (purse1.ConPurse_field2+purse2.ConPurse_field2)] (0:0:1 - 1) */
		IfNotBlocked
		reached[1][4] = 1;
		(trpt+1)->bup.oval = now.sum;
		now.sum = (now.purse1.ConPurse_field2+now.purse2.ConPurse_field2);
#ifdef VAR_RANGES
		logval("sum", now.sum);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 19: /* STATE 5 - mondex.pml:591 - [((sum>300))] (0:0:0 - 1) */
		IfNotBlocked
		reached[1][5] = 1;
		if (!((now.sum>300)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 20: /* STATE 10 - mondex.pml:593 - [-end-] (0:0:0 - 3) */
		IfNotBlocked
		reached[1][10] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC Main */
	case 21: /* STATE 1 - mondex.pml:76 - [((place_ConPurse?[ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15]&&place_msg_in?[msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10]))] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][1] = 1;
		if (!(((q_len(now.place_ConPurse) > 0)&&(q_len(now.place_msg_in) > 0))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 22: /* STATE 2 - mondex.pml:78 - [place_ConPurse?ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:15 - 1) */
		reached[0][2] = 1;
		if (q_len(now.place_ConPurse) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(15);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->ConPurse.ConPurse_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->ConPurse.ConPurse_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->ConPurse.ConPurse_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->ConPurse.ConPurse_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->ConPurse.ConPurse_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->ConPurse.ConPurse_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->ConPurse.ConPurse_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->ConPurse.ConPurse_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->ConPurse.ConPurse_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->ConPurse.ConPurse_field10;
		(trpt+1)->bup.ovals[10] = ((P0 *)this)->ConPurse.ConPurse_field11;
		(trpt+1)->bup.ovals[11] = ((P0 *)this)->ConPurse.ConPurse_field12;
		(trpt+1)->bup.ovals[12] = ((P0 *)this)->ConPurse.ConPurse_field13;
		(trpt+1)->bup.ovals[13] = ((P0 *)this)->ConPurse.ConPurse_field14;
		(trpt+1)->bup.ovals[14] = ((P0 *)this)->ConPurse.ConPurse_field15;
		;
		((P0 *)this)->ConPurse.ConPurse_field1 = qrecv(now.place_ConPurse, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field1", ((P0 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field2 = qrecv(now.place_ConPurse, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field2", ((P0 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field3 = qrecv(now.place_ConPurse, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field3", ((P0 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field4 = qrecv(now.place_ConPurse, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field4", ((P0 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field5 = qrecv(now.place_ConPurse, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field5", ((P0 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field6 = qrecv(now.place_ConPurse, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field6", ((P0 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field7 = qrecv(now.place_ConPurse, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field7", ((P0 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field8 = qrecv(now.place_ConPurse, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field8", ((P0 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field9 = qrecv(now.place_ConPurse, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field9", ((P0 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field10 = qrecv(now.place_ConPurse, XX-1, 9, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field10", ((P0 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field11 = qrecv(now.place_ConPurse, XX-1, 10, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field11", ((P0 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field12 = qrecv(now.place_ConPurse, XX-1, 11, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field12", ((P0 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field13 = qrecv(now.place_ConPurse, XX-1, 12, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field13", ((P0 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field14 = qrecv(now.place_ConPurse, XX-1, 13, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field14", ((P0 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field15 = qrecv(now.place_ConPurse, XX-1, 14, 1);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field15", ((P0 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 23: /* STATE 3 - mondex.pml:79 - [place_msg_in?msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:10 - 1) */
		reached[0][3] = 1;
		if (q_len(now.place_msg_in) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(10);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->msg_in.msg_in_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->msg_in.msg_in_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->msg_in.msg_in_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->msg_in.msg_in_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->msg_in.msg_in_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->msg_in.msg_in_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->msg_in.msg_in_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->msg_in.msg_in_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->msg_in.msg_in_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->msg_in.msg_in_field10;
		;
		((P0 *)this)->msg_in.msg_in_field1 = qrecv(now.place_msg_in, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field1", ((P0 *)this)->msg_in.msg_in_field1);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field2 = qrecv(now.place_msg_in, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field2", ((P0 *)this)->msg_in.msg_in_field2);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field3 = qrecv(now.place_msg_in, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field3", ((P0 *)this)->msg_in.msg_in_field3);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field4 = qrecv(now.place_msg_in, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field4", ((P0 *)this)->msg_in.msg_in_field4);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field5 = qrecv(now.place_msg_in, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field5", ((P0 *)this)->msg_in.msg_in_field5);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field6 = qrecv(now.place_msg_in, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field6", ((P0 *)this)->msg_in.msg_in_field6);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field7 = qrecv(now.place_msg_in, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field7", ((P0 *)this)->msg_in.msg_in_field7);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field8 = qrecv(now.place_msg_in, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field8", ((P0 *)this)->msg_in.msg_in_field8);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field9 = qrecv(now.place_msg_in, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field9", ((P0 *)this)->msg_in.msg_in_field9);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field10 = qrecv(now.place_msg_in, XX-1, 9, 1);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field10", ((P0 *)this)->msg_in.msg_in_field10);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 24: /* STATE 4 - mondex.pml:81 - [((((((((msg_in.msg_in_field1==0)&&(ConPurse.ConPurse_field1==msg_in.msg_in_field10))&&(ConPurse.ConPurse_field1==msg_in.msg_in_field2))&&(ConPurse.ConPurse_field2>=msg_in.msg_in_field3))&&(ConPurse.ConPurse_field10==1))&&1)&&1))] (47:0:1 - 1) */
		IfNotBlocked
		reached[0][4] = 1;
		if (!((((((((((P0 *)this)->msg_in.msg_in_field1==0)&&(((P0 *)this)->ConPurse.ConPurse_field1==((P0 *)this)->msg_in.msg_in_field10))&&(((P0 *)this)->ConPurse.ConPurse_field1==((P0 *)this)->msg_in.msg_in_field2))&&(((P0 *)this)->ConPurse.ConPurse_field2>=((P0 *)this)->msg_in.msg_in_field3))&&(((P0 *)this)->ConPurse.ConPurse_field10==1))&&1)&&1)))
			continue;
		/* merge: startFrom_is_enabled = 1(0, 6, 47) */
		reached[0][6] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->startFrom_is_enabled);
		((P0 *)this)->startFrom_is_enabled = 1;
#ifdef VAR_RANGES
		logval("Main:startFrom_is_enabled", ((int)((P0 *)this)->startFrom_is_enabled));
#endif
		;
		/* merge: .(goto)(0, 12, 47) */
		reached[0][12] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 25: /* STATE 8 - mondex.pml:83 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][8] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P0 *)this)->ConPurse.ConPurse_field1, ((P0 *)this)->ConPurse.ConPurse_field2, ((P0 *)this)->ConPurse.ConPurse_field3, ((P0 *)this)->ConPurse.ConPurse_field4, ((P0 *)this)->ConPurse.ConPurse_field5, ((P0 *)this)->ConPurse.ConPurse_field6, ((P0 *)this)->ConPurse.ConPurse_field7, ((P0 *)this)->ConPurse.ConPurse_field8, ((P0 *)this)->ConPurse.ConPurse_field9, ((P0 *)this)->ConPurse.ConPurse_field10, ((P0 *)this)->ConPurse.ConPurse_field11, ((P0 *)this)->ConPurse.ConPurse_field12, ((P0 *)this)->ConPurse.ConPurse_field13, ((P0 *)this)->ConPurse.ConPurse_field14, ((P0 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 26: /* STATE 9 - mondex.pml:84 - [place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][9] = 1;
		if (q_full(now.place_msg_in))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_in, 0, ((P0 *)this)->msg_in.msg_in_field1, ((P0 *)this)->msg_in.msg_in_field2, ((P0 *)this)->msg_in.msg_in_field3, ((P0 *)this)->msg_in.msg_in_field4, ((P0 *)this)->msg_in.msg_in_field5, ((P0 *)this)->msg_in.msg_in_field6, ((P0 *)this)->msg_in.msg_in_field7, ((P0 *)this)->msg_in.msg_in_field8, ((P0 *)this)->msg_in.msg_in_field9, ((P0 *)this)->msg_in.msg_in_field10, 0, 0, 0, 0, 0, 10);
		_m = 2; goto P999; /* 0 */
	case 27: /* STATE 14 - mondex.pml:488 - [(startFrom_is_enabled)] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][14] = 1;
		if (!(((int)((P0 *)this)->startFrom_is_enabled)))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: startFrom_is_enabled */  (trpt+1)->bup.oval = ((P0 *)this)->startFrom_is_enabled;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)this)->startFrom_is_enabled = 0;
		_m = 3; goto P999; /* 0 */
	case 28: /* STATE 15 - mondex.pml:196 - [ConPurse.ConPurse_field1 = ConPurse.ConPurse_field1] (0:40:25 - 1) */
		IfNotBlocked
		reached[0][15] = 1;
		(trpt+1)->bup.ovals = grab_ints(25);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->ConPurse.ConPurse_field1;
		((P0 *)this)->ConPurse.ConPurse_field1 = ((P0 *)this)->ConPurse.ConPurse_field1;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field1", ((P0 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		/* merge: ConPurse.ConPurse_field2 = ConPurse.ConPurse_field2(40, 16, 40) */
		reached[0][16] = 1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->ConPurse.ConPurse_field2;
		((P0 *)this)->ConPurse.ConPurse_field2 = ((P0 *)this)->ConPurse.ConPurse_field2;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field2", ((P0 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		/* merge: ConPurse.ConPurse_field3 = ConPurse.ConPurse_field3(40, 17, 40) */
		reached[0][17] = 1;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->ConPurse.ConPurse_field3;
		((P0 *)this)->ConPurse.ConPurse_field3 = ((P0 *)this)->ConPurse.ConPurse_field3;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field3", ((P0 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		/* merge: ConPurse.ConPurse_field4 = (ConPurse.ConPurse_field4+1)(40, 18, 40) */
		reached[0][18] = 1;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->ConPurse.ConPurse_field4;
		((P0 *)this)->ConPurse.ConPurse_field4 = (((P0 *)this)->ConPurse.ConPurse_field4+1);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field4", ((P0 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		/* merge: ConPurse.ConPurse_field5 = ConPurse.ConPurse_field1(40, 19, 40) */
		reached[0][19] = 1;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->ConPurse.ConPurse_field5;
		((P0 *)this)->ConPurse.ConPurse_field5 = ((P0 *)this)->ConPurse.ConPurse_field1;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field5", ((P0 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		/* merge: ConPurse.ConPurse_field6 = msg_in.msg_in_field2(40, 20, 40) */
		reached[0][20] = 1;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->ConPurse.ConPurse_field6;
		((P0 *)this)->ConPurse.ConPurse_field6 = ((P0 *)this)->msg_in.msg_in_field2;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field6", ((P0 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		/* merge: ConPurse.ConPurse_field7 = msg_in.msg_in_field3(40, 21, 40) */
		reached[0][21] = 1;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->ConPurse.ConPurse_field7;
		((P0 *)this)->ConPurse.ConPurse_field7 = ((P0 *)this)->msg_in.msg_in_field3;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field7", ((P0 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		/* merge: ConPurse.ConPurse_field8 = ConPurse.ConPurse_field4(40, 22, 40) */
		reached[0][22] = 1;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->ConPurse.ConPurse_field8;
		((P0 *)this)->ConPurse.ConPurse_field8 = ((P0 *)this)->ConPurse.ConPurse_field4;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field8", ((P0 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		/* merge: ConPurse.ConPurse_field9 = msg_in.msg_in_field4(40, 23, 40) */
		reached[0][23] = 1;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->ConPurse.ConPurse_field9;
		((P0 *)this)->ConPurse.ConPurse_field9 = ((P0 *)this)->msg_in.msg_in_field4;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field9", ((P0 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		/* merge: ConPurse.ConPurse_field10 = 2(40, 24, 40) */
		reached[0][24] = 1;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->ConPurse.ConPurse_field10;
		((P0 *)this)->ConPurse.ConPurse_field10 = 2;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field10", ((P0 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		/* merge: ConPurse.ConPurse_field11 = ConPurse.ConPurse_field11(40, 25, 40) */
		reached[0][25] = 1;
		(trpt+1)->bup.ovals[10] = ((P0 *)this)->ConPurse.ConPurse_field11;
		((P0 *)this)->ConPurse.ConPurse_field11 = ((P0 *)this)->ConPurse.ConPurse_field11;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field11", ((P0 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		/* merge: ConPurse.ConPurse_field12 = ConPurse.ConPurse_field12(40, 26, 40) */
		reached[0][26] = 1;
		(trpt+1)->bup.ovals[11] = ((P0 *)this)->ConPurse.ConPurse_field12;
		((P0 *)this)->ConPurse.ConPurse_field12 = ((P0 *)this)->ConPurse.ConPurse_field12;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field12", ((P0 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		/* merge: ConPurse.ConPurse_field13 = ConPurse.ConPurse_field13(40, 27, 40) */
		reached[0][27] = 1;
		(trpt+1)->bup.ovals[12] = ((P0 *)this)->ConPurse.ConPurse_field13;
		((P0 *)this)->ConPurse.ConPurse_field13 = ((P0 *)this)->ConPurse.ConPurse_field13;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field13", ((P0 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		/* merge: ConPurse.ConPurse_field14 = ConPurse.ConPurse_field14(40, 28, 40) */
		reached[0][28] = 1;
		(trpt+1)->bup.ovals[13] = ((P0 *)this)->ConPurse.ConPurse_field14;
		((P0 *)this)->ConPurse.ConPurse_field14 = ((P0 *)this)->ConPurse.ConPurse_field14;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field14", ((P0 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		/* merge: ConPurse.ConPurse_field15 = ConPurse.ConPurse_field15(40, 29, 40) */
		reached[0][29] = 1;
		(trpt+1)->bup.ovals[14] = ((P0 *)this)->ConPurse.ConPurse_field15;
		((P0 *)this)->ConPurse.ConPurse_field15 = ((P0 *)this)->ConPurse.ConPurse_field15;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field15", ((P0 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		/* merge: msg_out.msg_out_field1 = 3(40, 30, 40) */
		reached[0][30] = 1;
		(trpt+1)->bup.ovals[15] = ((P0 *)this)->msg_out.msg_out_field1;
		((P0 *)this)->msg_out.msg_out_field1 = 3;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field1", ((P0 *)this)->msg_out.msg_out_field1);
#endif
		;
		/* merge: msg_out.msg_out_field2 = msg_in.msg_in_field2(40, 31, 40) */
		reached[0][31] = 1;
		(trpt+1)->bup.ovals[16] = ((P0 *)this)->msg_out.msg_out_field2;
		((P0 *)this)->msg_out.msg_out_field2 = ((P0 *)this)->msg_in.msg_in_field2;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field2", ((P0 *)this)->msg_out.msg_out_field2);
#endif
		;
		/* merge: msg_out.msg_out_field3 = msg_in.msg_in_field3(40, 32, 40) */
		reached[0][32] = 1;
		(trpt+1)->bup.ovals[17] = ((P0 *)this)->msg_out.msg_out_field3;
		((P0 *)this)->msg_out.msg_out_field3 = ((P0 *)this)->msg_in.msg_in_field3;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field3", ((P0 *)this)->msg_out.msg_out_field3);
#endif
		;
		/* merge: msg_out.msg_out_field4 = msg_in.msg_in_field4(40, 33, 40) */
		reached[0][33] = 1;
		(trpt+1)->bup.ovals[18] = ((P0 *)this)->msg_out.msg_out_field4;
		((P0 *)this)->msg_out.msg_out_field4 = ((P0 *)this)->msg_in.msg_in_field4;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field4", ((P0 *)this)->msg_out.msg_out_field4);
#endif
		;
		/* merge: msg_out.msg_out_field5 = msg_in.msg_in_field5(40, 34, 40) */
		reached[0][34] = 1;
		(trpt+1)->bup.ovals[19] = ((P0 *)this)->msg_out.msg_out_field5;
		((P0 *)this)->msg_out.msg_out_field5 = ((P0 *)this)->msg_in.msg_in_field5;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field5", ((P0 *)this)->msg_out.msg_out_field5);
#endif
		;
		/* merge: msg_out.msg_out_field6 = msg_in.msg_in_field6(40, 35, 40) */
		reached[0][35] = 1;
		(trpt+1)->bup.ovals[20] = ((P0 *)this)->msg_out.msg_out_field6;
		((P0 *)this)->msg_out.msg_out_field6 = ((P0 *)this)->msg_in.msg_in_field6;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field6", ((P0 *)this)->msg_out.msg_out_field6);
#endif
		;
		/* merge: msg_out.msg_out_field7 = msg_in.msg_in_field7(40, 36, 40) */
		reached[0][36] = 1;
		(trpt+1)->bup.ovals[21] = ((P0 *)this)->msg_out.msg_out_field7;
		((P0 *)this)->msg_out.msg_out_field7 = ((P0 *)this)->msg_in.msg_in_field7;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field7", ((P0 *)this)->msg_out.msg_out_field7);
#endif
		;
		/* merge: msg_out.msg_out_field8 = msg_in.msg_in_field8(40, 37, 40) */
		reached[0][37] = 1;
		(trpt+1)->bup.ovals[22] = ((P0 *)this)->msg_out.msg_out_field8;
		((P0 *)this)->msg_out.msg_out_field8 = ((P0 *)this)->msg_in.msg_in_field8;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field8", ((P0 *)this)->msg_out.msg_out_field8);
#endif
		;
		/* merge: msg_out.msg_out_field9 = msg_in.msg_in_field9(40, 38, 40) */
		reached[0][38] = 1;
		(trpt+1)->bup.ovals[23] = ((P0 *)this)->msg_out.msg_out_field9;
		((P0 *)this)->msg_out.msg_out_field9 = ((P0 *)this)->msg_in.msg_in_field9;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field9", ((P0 *)this)->msg_out.msg_out_field9);
#endif
		;
		/* merge: msg_out.msg_out_field10 = msg_in.msg_in_field10(40, 39, 40) */
		reached[0][39] = 1;
		(trpt+1)->bup.ovals[24] = ((P0 *)this)->msg_out.msg_out_field10;
		((P0 *)this)->msg_out.msg_out_field10 = ((P0 *)this)->msg_in.msg_in_field10;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field10", ((P0 *)this)->msg_out.msg_out_field10);
#endif
		;
		_m = 3; goto P999; /* 24 */
	case 29: /* STATE 40 - mondex.pml:223 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][40] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P0 *)this)->ConPurse.ConPurse_field1, ((P0 *)this)->ConPurse.ConPurse_field2, ((P0 *)this)->ConPurse.ConPurse_field3, ((P0 *)this)->ConPurse.ConPurse_field4, ((P0 *)this)->ConPurse.ConPurse_field5, ((P0 *)this)->ConPurse.ConPurse_field6, ((P0 *)this)->ConPurse.ConPurse_field7, ((P0 *)this)->ConPurse.ConPurse_field8, ((P0 *)this)->ConPurse.ConPurse_field9, ((P0 *)this)->ConPurse.ConPurse_field10, ((P0 *)this)->ConPurse.ConPurse_field11, ((P0 *)this)->ConPurse.ConPurse_field12, ((P0 *)this)->ConPurse.ConPurse_field13, ((P0 *)this)->ConPurse.ConPurse_field14, ((P0 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 30: /* STATE 41 - mondex.pml:224 - [place_msg_out!msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10] (475:0:1 - 1) */
		IfNotBlocked
		reached[0][41] = 1;
		if (q_full(now.place_msg_out))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_out);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_out, 0, ((P0 *)this)->msg_out.msg_out_field1, ((P0 *)this)->msg_out.msg_out_field2, ((P0 *)this)->msg_out.msg_out_field3, ((P0 *)this)->msg_out.msg_out_field4, ((P0 *)this)->msg_out.msg_out_field5, ((P0 *)this)->msg_out.msg_out_field6, ((P0 *)this)->msg_out.msg_out_field7, ((P0 *)this)->msg_out.msg_out_field8, ((P0 *)this)->msg_out.msg_out_field9, ((P0 *)this)->msg_out.msg_out_field10, 0, 0, 0, 0, 0, 10);
		/* merge: startFrom_is_enabled = 0(475, 42, 475) */
		reached[0][42] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->startFrom_is_enabled);
		((P0 *)this)->startFrom_is_enabled = 0;
#ifdef VAR_RANGES
		logval("Main:startFrom_is_enabled", ((int)((P0 *)this)->startFrom_is_enabled));
#endif
		;
		/* merge: .(goto)(475, 48, 475) */
		reached[0][48] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 2; goto P999; /* 3 */
	case 31: /* STATE 48 - mondex.pml:491 - [.(goto)] (0:475:0 - 2) */
		IfNotBlocked
		reached[0][48] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 1 */
	case 32: /* STATE 46 - mondex.pml:489 - [(1)] (475:0:0 - 1) */
		IfNotBlocked
		reached[0][46] = 1;
		if (!(1))
			continue;
		/* merge: .(goto)(475, 48, 475) */
		reached[0][48] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 33: /* STATE 51 - mondex.pml:88 - [((place_ConPurse?[ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15]&&place_msg_in?[msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10]))] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][51] = 1;
		if (!(((q_len(now.place_ConPurse) > 0)&&(q_len(now.place_msg_in) > 0))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 34: /* STATE 52 - mondex.pml:90 - [place_ConPurse?ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:15 - 1) */
		reached[0][52] = 1;
		if (q_len(now.place_ConPurse) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(15);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->ConPurse.ConPurse_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->ConPurse.ConPurse_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->ConPurse.ConPurse_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->ConPurse.ConPurse_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->ConPurse.ConPurse_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->ConPurse.ConPurse_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->ConPurse.ConPurse_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->ConPurse.ConPurse_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->ConPurse.ConPurse_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->ConPurse.ConPurse_field10;
		(trpt+1)->bup.ovals[10] = ((P0 *)this)->ConPurse.ConPurse_field11;
		(trpt+1)->bup.ovals[11] = ((P0 *)this)->ConPurse.ConPurse_field12;
		(trpt+1)->bup.ovals[12] = ((P0 *)this)->ConPurse.ConPurse_field13;
		(trpt+1)->bup.ovals[13] = ((P0 *)this)->ConPurse.ConPurse_field14;
		(trpt+1)->bup.ovals[14] = ((P0 *)this)->ConPurse.ConPurse_field15;
		;
		((P0 *)this)->ConPurse.ConPurse_field1 = qrecv(now.place_ConPurse, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field1", ((P0 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field2 = qrecv(now.place_ConPurse, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field2", ((P0 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field3 = qrecv(now.place_ConPurse, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field3", ((P0 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field4 = qrecv(now.place_ConPurse, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field4", ((P0 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field5 = qrecv(now.place_ConPurse, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field5", ((P0 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field6 = qrecv(now.place_ConPurse, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field6", ((P0 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field7 = qrecv(now.place_ConPurse, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field7", ((P0 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field8 = qrecv(now.place_ConPurse, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field8", ((P0 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field9 = qrecv(now.place_ConPurse, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field9", ((P0 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field10 = qrecv(now.place_ConPurse, XX-1, 9, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field10", ((P0 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field11 = qrecv(now.place_ConPurse, XX-1, 10, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field11", ((P0 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field12 = qrecv(now.place_ConPurse, XX-1, 11, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field12", ((P0 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field13 = qrecv(now.place_ConPurse, XX-1, 12, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field13", ((P0 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field14 = qrecv(now.place_ConPurse, XX-1, 13, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field14", ((P0 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field15 = qrecv(now.place_ConPurse, XX-1, 14, 1);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field15", ((P0 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 35: /* STATE 53 - mondex.pml:91 - [place_msg_in?msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:10 - 1) */
		reached[0][53] = 1;
		if (q_len(now.place_msg_in) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(10);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->msg_in.msg_in_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->msg_in.msg_in_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->msg_in.msg_in_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->msg_in.msg_in_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->msg_in.msg_in_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->msg_in.msg_in_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->msg_in.msg_in_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->msg_in.msg_in_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->msg_in.msg_in_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->msg_in.msg_in_field10;
		;
		((P0 *)this)->msg_in.msg_in_field1 = qrecv(now.place_msg_in, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field1", ((P0 *)this)->msg_in.msg_in_field1);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field2 = qrecv(now.place_msg_in, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field2", ((P0 *)this)->msg_in.msg_in_field2);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field3 = qrecv(now.place_msg_in, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field3", ((P0 *)this)->msg_in.msg_in_field3);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field4 = qrecv(now.place_msg_in, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field4", ((P0 *)this)->msg_in.msg_in_field4);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field5 = qrecv(now.place_msg_in, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field5", ((P0 *)this)->msg_in.msg_in_field5);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field6 = qrecv(now.place_msg_in, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field6", ((P0 *)this)->msg_in.msg_in_field6);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field7 = qrecv(now.place_msg_in, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field7", ((P0 *)this)->msg_in.msg_in_field7);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field8 = qrecv(now.place_msg_in, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field8", ((P0 *)this)->msg_in.msg_in_field8);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field9 = qrecv(now.place_msg_in, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field9", ((P0 *)this)->msg_in.msg_in_field9);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field10 = qrecv(now.place_msg_in, XX-1, 9, 1);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field10", ((P0 *)this)->msg_in.msg_in_field10);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 36: /* STATE 54 - mondex.pml:93 - [((((((msg_in.msg_in_field1==4)&&(ConPurse.ConPurse_field1==msg_in.msg_in_field10))&&(ConPurse.ConPurse_field10==2))&&1)&&1))] (97:0:1 - 1) */
		IfNotBlocked
		reached[0][54] = 1;
		if (!((((((((P0 *)this)->msg_in.msg_in_field1==4)&&(((P0 *)this)->ConPurse.ConPurse_field1==((P0 *)this)->msg_in.msg_in_field10))&&(((P0 *)this)->ConPurse.ConPurse_field10==2))&&1)&&1)))
			continue;
		/* merge: req_is_enabled = 1(0, 56, 97) */
		reached[0][56] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->req_is_enabled);
		((P0 *)this)->req_is_enabled = 1;
#ifdef VAR_RANGES
		logval("Main:req_is_enabled", ((int)((P0 *)this)->req_is_enabled));
#endif
		;
		/* merge: .(goto)(0, 62, 97) */
		reached[0][62] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 37: /* STATE 58 - mondex.pml:95 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][58] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P0 *)this)->ConPurse.ConPurse_field1, ((P0 *)this)->ConPurse.ConPurse_field2, ((P0 *)this)->ConPurse.ConPurse_field3, ((P0 *)this)->ConPurse.ConPurse_field4, ((P0 *)this)->ConPurse.ConPurse_field5, ((P0 *)this)->ConPurse.ConPurse_field6, ((P0 *)this)->ConPurse.ConPurse_field7, ((P0 *)this)->ConPurse.ConPurse_field8, ((P0 *)this)->ConPurse.ConPurse_field9, ((P0 *)this)->ConPurse.ConPurse_field10, ((P0 *)this)->ConPurse.ConPurse_field11, ((P0 *)this)->ConPurse.ConPurse_field12, ((P0 *)this)->ConPurse.ConPurse_field13, ((P0 *)this)->ConPurse.ConPurse_field14, ((P0 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 38: /* STATE 59 - mondex.pml:96 - [place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][59] = 1;
		if (q_full(now.place_msg_in))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_in, 0, ((P0 *)this)->msg_in.msg_in_field1, ((P0 *)this)->msg_in.msg_in_field2, ((P0 *)this)->msg_in.msg_in_field3, ((P0 *)this)->msg_in.msg_in_field4, ((P0 *)this)->msg_in.msg_in_field5, ((P0 *)this)->msg_in.msg_in_field6, ((P0 *)this)->msg_in.msg_in_field7, ((P0 *)this)->msg_in.msg_in_field8, ((P0 *)this)->msg_in.msg_in_field9, ((P0 *)this)->msg_in.msg_in_field10, 0, 0, 0, 0, 0, 10);
		_m = 2; goto P999; /* 0 */
	case 39: /* STATE 64 - mondex.pml:495 - [(req_is_enabled)] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][64] = 1;
		if (!(((int)((P0 *)this)->req_is_enabled)))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: req_is_enabled */  (trpt+1)->bup.oval = ((P0 *)this)->req_is_enabled;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)this)->req_is_enabled = 0;
		_m = 3; goto P999; /* 0 */
	case 40: /* STATE 65 - mondex.pml:228 - [ConPurse.ConPurse_field1 = ConPurse.ConPurse_field1] (0:90:25 - 1) */
		IfNotBlocked
		reached[0][65] = 1;
		(trpt+1)->bup.ovals = grab_ints(25);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->ConPurse.ConPurse_field1;
		((P0 *)this)->ConPurse.ConPurse_field1 = ((P0 *)this)->ConPurse.ConPurse_field1;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field1", ((P0 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		/* merge: ConPurse.ConPurse_field2 = (ConPurse.ConPurse_field2-msg_in.msg_in_field7)(90, 66, 90) */
		reached[0][66] = 1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->ConPurse.ConPurse_field2;
		((P0 *)this)->ConPurse.ConPurse_field2 = (((P0 *)this)->ConPurse.ConPurse_field2-((P0 *)this)->msg_in.msg_in_field7);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field2", ((P0 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		/* merge: ConPurse.ConPurse_field3 = ConPurse.ConPurse_field3(90, 67, 90) */
		reached[0][67] = 1;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->ConPurse.ConPurse_field3;
		((P0 *)this)->ConPurse.ConPurse_field3 = ((P0 *)this)->ConPurse.ConPurse_field3;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field3", ((P0 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		/* merge: ConPurse.ConPurse_field4 = ConPurse.ConPurse_field4(90, 68, 90) */
		reached[0][68] = 1;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->ConPurse.ConPurse_field4;
		((P0 *)this)->ConPurse.ConPurse_field4 = ((P0 *)this)->ConPurse.ConPurse_field4;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field4", ((P0 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		/* merge: ConPurse.ConPurse_field5 = ConPurse.ConPurse_field5(90, 69, 90) */
		reached[0][69] = 1;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->ConPurse.ConPurse_field5;
		((P0 *)this)->ConPurse.ConPurse_field5 = ((P0 *)this)->ConPurse.ConPurse_field5;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field5", ((P0 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		/* merge: ConPurse.ConPurse_field6 = ConPurse.ConPurse_field6(90, 70, 90) */
		reached[0][70] = 1;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->ConPurse.ConPurse_field6;
		((P0 *)this)->ConPurse.ConPurse_field6 = ((P0 *)this)->ConPurse.ConPurse_field6;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field6", ((P0 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		/* merge: ConPurse.ConPurse_field7 = ConPurse.ConPurse_field7(90, 71, 90) */
		reached[0][71] = 1;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->ConPurse.ConPurse_field7;
		((P0 *)this)->ConPurse.ConPurse_field7 = ((P0 *)this)->ConPurse.ConPurse_field7;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field7", ((P0 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		/* merge: ConPurse.ConPurse_field8 = ConPurse.ConPurse_field8(90, 72, 90) */
		reached[0][72] = 1;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->ConPurse.ConPurse_field8;
		((P0 *)this)->ConPurse.ConPurse_field8 = ((P0 *)this)->ConPurse.ConPurse_field8;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field8", ((P0 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		/* merge: ConPurse.ConPurse_field9 = ConPurse.ConPurse_field9(90, 73, 90) */
		reached[0][73] = 1;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->ConPurse.ConPurse_field9;
		((P0 *)this)->ConPurse.ConPurse_field9 = ((P0 *)this)->ConPurse.ConPurse_field9;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field9", ((P0 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		/* merge: ConPurse.ConPurse_field10 = 5(90, 74, 90) */
		reached[0][74] = 1;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->ConPurse.ConPurse_field10;
		((P0 *)this)->ConPurse.ConPurse_field10 = 5;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field10", ((P0 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		/* merge: ConPurse.ConPurse_field11 = ConPurse.ConPurse_field11(90, 75, 90) */
		reached[0][75] = 1;
		(trpt+1)->bup.ovals[10] = ((P0 *)this)->ConPurse.ConPurse_field11;
		((P0 *)this)->ConPurse.ConPurse_field11 = ((P0 *)this)->ConPurse.ConPurse_field11;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field11", ((P0 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		/* merge: ConPurse.ConPurse_field12 = ConPurse.ConPurse_field12(90, 76, 90) */
		reached[0][76] = 1;
		(trpt+1)->bup.ovals[11] = ((P0 *)this)->ConPurse.ConPurse_field12;
		((P0 *)this)->ConPurse.ConPurse_field12 = ((P0 *)this)->ConPurse.ConPurse_field12;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field12", ((P0 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		/* merge: ConPurse.ConPurse_field13 = ConPurse.ConPurse_field13(90, 77, 90) */
		reached[0][77] = 1;
		(trpt+1)->bup.ovals[12] = ((P0 *)this)->ConPurse.ConPurse_field13;
		((P0 *)this)->ConPurse.ConPurse_field13 = ((P0 *)this)->ConPurse.ConPurse_field13;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field13", ((P0 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		/* merge: ConPurse.ConPurse_field14 = ConPurse.ConPurse_field14(90, 78, 90) */
		reached[0][78] = 1;
		(trpt+1)->bup.ovals[13] = ((P0 *)this)->ConPurse.ConPurse_field14;
		((P0 *)this)->ConPurse.ConPurse_field14 = ((P0 *)this)->ConPurse.ConPurse_field14;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field14", ((P0 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		/* merge: ConPurse.ConPurse_field15 = ConPurse.ConPurse_field15(90, 79, 90) */
		reached[0][79] = 1;
		(trpt+1)->bup.ovals[14] = ((P0 *)this)->ConPurse.ConPurse_field15;
		((P0 *)this)->ConPurse.ConPurse_field15 = ((P0 *)this)->ConPurse.ConPurse_field15;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field15", ((P0 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		/* merge: msg_out.msg_out_field1 = 6(90, 80, 90) */
		reached[0][80] = 1;
		(trpt+1)->bup.ovals[15] = ((P0 *)this)->msg_out.msg_out_field1;
		((P0 *)this)->msg_out.msg_out_field1 = 6;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field1", ((P0 *)this)->msg_out.msg_out_field1);
#endif
		;
		/* merge: msg_out.msg_out_field2 = msg_in.msg_in_field2(90, 81, 90) */
		reached[0][81] = 1;
		(trpt+1)->bup.ovals[16] = ((P0 *)this)->msg_out.msg_out_field2;
		((P0 *)this)->msg_out.msg_out_field2 = ((P0 *)this)->msg_in.msg_in_field2;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field2", ((P0 *)this)->msg_out.msg_out_field2);
#endif
		;
		/* merge: msg_out.msg_out_field3 = msg_in.msg_in_field3(90, 82, 90) */
		reached[0][82] = 1;
		(trpt+1)->bup.ovals[17] = ((P0 *)this)->msg_out.msg_out_field3;
		((P0 *)this)->msg_out.msg_out_field3 = ((P0 *)this)->msg_in.msg_in_field3;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field3", ((P0 *)this)->msg_out.msg_out_field3);
#endif
		;
		/* merge: msg_out.msg_out_field4 = msg_in.msg_in_field4(90, 83, 90) */
		reached[0][83] = 1;
		(trpt+1)->bup.ovals[18] = ((P0 *)this)->msg_out.msg_out_field4;
		((P0 *)this)->msg_out.msg_out_field4 = ((P0 *)this)->msg_in.msg_in_field4;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field4", ((P0 *)this)->msg_out.msg_out_field4);
#endif
		;
		/* merge: msg_out.msg_out_field5 = ConPurse.ConPurse_field5(90, 84, 90) */
		reached[0][84] = 1;
		(trpt+1)->bup.ovals[19] = ((P0 *)this)->msg_out.msg_out_field5;
		((P0 *)this)->msg_out.msg_out_field5 = ((P0 *)this)->ConPurse.ConPurse_field5;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field5", ((P0 *)this)->msg_out.msg_out_field5);
#endif
		;
		/* merge: msg_out.msg_out_field6 = ConPurse.ConPurse_field6(90, 85, 90) */
		reached[0][85] = 1;
		(trpt+1)->bup.ovals[20] = ((P0 *)this)->msg_out.msg_out_field6;
		((P0 *)this)->msg_out.msg_out_field6 = ((P0 *)this)->ConPurse.ConPurse_field6;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field6", ((P0 *)this)->msg_out.msg_out_field6);
#endif
		;
		/* merge: msg_out.msg_out_field7 = ConPurse.ConPurse_field7(90, 86, 90) */
		reached[0][86] = 1;
		(trpt+1)->bup.ovals[21] = ((P0 *)this)->msg_out.msg_out_field7;
		((P0 *)this)->msg_out.msg_out_field7 = ((P0 *)this)->ConPurse.ConPurse_field7;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field7", ((P0 *)this)->msg_out.msg_out_field7);
#endif
		;
		/* merge: msg_out.msg_out_field8 = ConPurse.ConPurse_field8(90, 87, 90) */
		reached[0][87] = 1;
		(trpt+1)->bup.ovals[22] = ((P0 *)this)->msg_out.msg_out_field8;
		((P0 *)this)->msg_out.msg_out_field8 = ((P0 *)this)->ConPurse.ConPurse_field8;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field8", ((P0 *)this)->msg_out.msg_out_field8);
#endif
		;
		/* merge: msg_out.msg_out_field9 = ConPurse.ConPurse_field9(90, 88, 90) */
		reached[0][88] = 1;
		(trpt+1)->bup.ovals[23] = ((P0 *)this)->msg_out.msg_out_field9;
		((P0 *)this)->msg_out.msg_out_field9 = ((P0 *)this)->ConPurse.ConPurse_field9;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field9", ((P0 *)this)->msg_out.msg_out_field9);
#endif
		;
		/* merge: msg_out.msg_out_field10 = msg_in.msg_in_field6(90, 89, 90) */
		reached[0][89] = 1;
		(trpt+1)->bup.ovals[24] = ((P0 *)this)->msg_out.msg_out_field10;
		((P0 *)this)->msg_out.msg_out_field10 = ((P0 *)this)->msg_in.msg_in_field6;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field10", ((P0 *)this)->msg_out.msg_out_field10);
#endif
		;
		_m = 3; goto P999; /* 24 */
	case 41: /* STATE 90 - mondex.pml:255 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][90] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P0 *)this)->ConPurse.ConPurse_field1, ((P0 *)this)->ConPurse.ConPurse_field2, ((P0 *)this)->ConPurse.ConPurse_field3, ((P0 *)this)->ConPurse.ConPurse_field4, ((P0 *)this)->ConPurse.ConPurse_field5, ((P0 *)this)->ConPurse.ConPurse_field6, ((P0 *)this)->ConPurse.ConPurse_field7, ((P0 *)this)->ConPurse.ConPurse_field8, ((P0 *)this)->ConPurse.ConPurse_field9, ((P0 *)this)->ConPurse.ConPurse_field10, ((P0 *)this)->ConPurse.ConPurse_field11, ((P0 *)this)->ConPurse.ConPurse_field12, ((P0 *)this)->ConPurse.ConPurse_field13, ((P0 *)this)->ConPurse.ConPurse_field14, ((P0 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 42: /* STATE 91 - mondex.pml:256 - [place_msg_out!msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10] (475:0:1 - 1) */
		IfNotBlocked
		reached[0][91] = 1;
		if (q_full(now.place_msg_out))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_out);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_out, 0, ((P0 *)this)->msg_out.msg_out_field1, ((P0 *)this)->msg_out.msg_out_field2, ((P0 *)this)->msg_out.msg_out_field3, ((P0 *)this)->msg_out.msg_out_field4, ((P0 *)this)->msg_out.msg_out_field5, ((P0 *)this)->msg_out.msg_out_field6, ((P0 *)this)->msg_out.msg_out_field7, ((P0 *)this)->msg_out.msg_out_field8, ((P0 *)this)->msg_out.msg_out_field9, ((P0 *)this)->msg_out.msg_out_field10, 0, 0, 0, 0, 0, 10);
		/* merge: req_is_enabled = 0(475, 92, 475) */
		reached[0][92] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->req_is_enabled);
		((P0 *)this)->req_is_enabled = 0;
#ifdef VAR_RANGES
		logval("Main:req_is_enabled", ((int)((P0 *)this)->req_is_enabled));
#endif
		;
		/* merge: .(goto)(475, 98, 475) */
		reached[0][98] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 2; goto P999; /* 3 */
	case 43: /* STATE 98 - mondex.pml:498 - [.(goto)] (0:475:0 - 2) */
		IfNotBlocked
		reached[0][98] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 1 */
	case 44: /* STATE 96 - mondex.pml:496 - [(1)] (475:0:0 - 1) */
		IfNotBlocked
		reached[0][96] = 1;
		if (!(1))
			continue;
		/* merge: .(goto)(475, 98, 475) */
		reached[0][98] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 45: /* STATE 101 - mondex.pml:100 - [((place_ConPurse?[ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15]&&place_msg_in?[msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10]))] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][101] = 1;
		if (!(((q_len(now.place_ConPurse) > 0)&&(q_len(now.place_msg_in) > 0))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 46: /* STATE 102 - mondex.pml:102 - [place_ConPurse?ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:15 - 1) */
		reached[0][102] = 1;
		if (q_len(now.place_ConPurse) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(15);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->ConPurse.ConPurse_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->ConPurse.ConPurse_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->ConPurse.ConPurse_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->ConPurse.ConPurse_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->ConPurse.ConPurse_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->ConPurse.ConPurse_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->ConPurse.ConPurse_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->ConPurse.ConPurse_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->ConPurse.ConPurse_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->ConPurse.ConPurse_field10;
		(trpt+1)->bup.ovals[10] = ((P0 *)this)->ConPurse.ConPurse_field11;
		(trpt+1)->bup.ovals[11] = ((P0 *)this)->ConPurse.ConPurse_field12;
		(trpt+1)->bup.ovals[12] = ((P0 *)this)->ConPurse.ConPurse_field13;
		(trpt+1)->bup.ovals[13] = ((P0 *)this)->ConPurse.ConPurse_field14;
		(trpt+1)->bup.ovals[14] = ((P0 *)this)->ConPurse.ConPurse_field15;
		;
		((P0 *)this)->ConPurse.ConPurse_field1 = qrecv(now.place_ConPurse, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field1", ((P0 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field2 = qrecv(now.place_ConPurse, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field2", ((P0 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field3 = qrecv(now.place_ConPurse, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field3", ((P0 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field4 = qrecv(now.place_ConPurse, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field4", ((P0 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field5 = qrecv(now.place_ConPurse, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field5", ((P0 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field6 = qrecv(now.place_ConPurse, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field6", ((P0 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field7 = qrecv(now.place_ConPurse, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field7", ((P0 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field8 = qrecv(now.place_ConPurse, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field8", ((P0 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field9 = qrecv(now.place_ConPurse, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field9", ((P0 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field10 = qrecv(now.place_ConPurse, XX-1, 9, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field10", ((P0 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field11 = qrecv(now.place_ConPurse, XX-1, 10, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field11", ((P0 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field12 = qrecv(now.place_ConPurse, XX-1, 11, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field12", ((P0 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field13 = qrecv(now.place_ConPurse, XX-1, 12, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field13", ((P0 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field14 = qrecv(now.place_ConPurse, XX-1, 13, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field14", ((P0 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field15 = qrecv(now.place_ConPurse, XX-1, 14, 1);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field15", ((P0 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 47: /* STATE 103 - mondex.pml:103 - [place_msg_in?msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:10 - 1) */
		reached[0][103] = 1;
		if (q_len(now.place_msg_in) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(10);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->msg_in.msg_in_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->msg_in.msg_in_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->msg_in.msg_in_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->msg_in.msg_in_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->msg_in.msg_in_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->msg_in.msg_in_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->msg_in.msg_in_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->msg_in.msg_in_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->msg_in.msg_in_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->msg_in.msg_in_field10;
		;
		((P0 *)this)->msg_in.msg_in_field1 = qrecv(now.place_msg_in, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field1", ((P0 *)this)->msg_in.msg_in_field1);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field2 = qrecv(now.place_msg_in, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field2", ((P0 *)this)->msg_in.msg_in_field2);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field3 = qrecv(now.place_msg_in, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field3", ((P0 *)this)->msg_in.msg_in_field3);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field4 = qrecv(now.place_msg_in, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field4", ((P0 *)this)->msg_in.msg_in_field4);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field5 = qrecv(now.place_msg_in, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field5", ((P0 *)this)->msg_in.msg_in_field5);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field6 = qrecv(now.place_msg_in, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field6", ((P0 *)this)->msg_in.msg_in_field6);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field7 = qrecv(now.place_msg_in, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field7", ((P0 *)this)->msg_in.msg_in_field7);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field8 = qrecv(now.place_msg_in, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field8", ((P0 *)this)->msg_in.msg_in_field8);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field9 = qrecv(now.place_msg_in, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field9", ((P0 *)this)->msg_in.msg_in_field9);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field10 = qrecv(now.place_msg_in, XX-1, 9, 1);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field10", ((P0 *)this)->msg_in.msg_in_field10);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 48: /* STATE 104 - mondex.pml:105 - [((((((((msg_in.msg_in_field1==7)&&(ConPurse.ConPurse_field1==msg_in.msg_in_field10))&&(ConPurse.ConPurse_field1==msg_in.msg_in_field2))&&(ConPurse.ConPurse_field2>=msg_in.msg_in_field3))&&(ConPurse.ConPurse_field10==1))&&1)&&1))] (147:0:1 - 1) */
		IfNotBlocked
		reached[0][104] = 1;
		if (!((((((((((P0 *)this)->msg_in.msg_in_field1==7)&&(((P0 *)this)->ConPurse.ConPurse_field1==((P0 *)this)->msg_in.msg_in_field10))&&(((P0 *)this)->ConPurse.ConPurse_field1==((P0 *)this)->msg_in.msg_in_field2))&&(((P0 *)this)->ConPurse.ConPurse_field2>=((P0 *)this)->msg_in.msg_in_field3))&&(((P0 *)this)->ConPurse.ConPurse_field10==1))&&1)&&1)))
			continue;
		/* merge: startTo_is_enabled = 1(0, 106, 147) */
		reached[0][106] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->startTo_is_enabled);
		((P0 *)this)->startTo_is_enabled = 1;
#ifdef VAR_RANGES
		logval("Main:startTo_is_enabled", ((int)((P0 *)this)->startTo_is_enabled));
#endif
		;
		/* merge: .(goto)(0, 112, 147) */
		reached[0][112] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 49: /* STATE 108 - mondex.pml:107 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][108] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P0 *)this)->ConPurse.ConPurse_field1, ((P0 *)this)->ConPurse.ConPurse_field2, ((P0 *)this)->ConPurse.ConPurse_field3, ((P0 *)this)->ConPurse.ConPurse_field4, ((P0 *)this)->ConPurse.ConPurse_field5, ((P0 *)this)->ConPurse.ConPurse_field6, ((P0 *)this)->ConPurse.ConPurse_field7, ((P0 *)this)->ConPurse.ConPurse_field8, ((P0 *)this)->ConPurse.ConPurse_field9, ((P0 *)this)->ConPurse.ConPurse_field10, ((P0 *)this)->ConPurse.ConPurse_field11, ((P0 *)this)->ConPurse.ConPurse_field12, ((P0 *)this)->ConPurse.ConPurse_field13, ((P0 *)this)->ConPurse.ConPurse_field14, ((P0 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 50: /* STATE 109 - mondex.pml:108 - [place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][109] = 1;
		if (q_full(now.place_msg_in))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_in, 0, ((P0 *)this)->msg_in.msg_in_field1, ((P0 *)this)->msg_in.msg_in_field2, ((P0 *)this)->msg_in.msg_in_field3, ((P0 *)this)->msg_in.msg_in_field4, ((P0 *)this)->msg_in.msg_in_field5, ((P0 *)this)->msg_in.msg_in_field6, ((P0 *)this)->msg_in.msg_in_field7, ((P0 *)this)->msg_in.msg_in_field8, ((P0 *)this)->msg_in.msg_in_field9, ((P0 *)this)->msg_in.msg_in_field10, 0, 0, 0, 0, 0, 10);
		_m = 2; goto P999; /* 0 */
	case 51: /* STATE 114 - mondex.pml:502 - [(startTo_is_enabled)] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][114] = 1;
		if (!(((int)((P0 *)this)->startTo_is_enabled)))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: startTo_is_enabled */  (trpt+1)->bup.oval = ((P0 *)this)->startTo_is_enabled;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)this)->startTo_is_enabled = 0;
		_m = 3; goto P999; /* 0 */
	case 52: /* STATE 115 - mondex.pml:260 - [ConPurse.ConPurse_field1 = ConPurse.ConPurse_field1] (0:140:25 - 1) */
		IfNotBlocked
		reached[0][115] = 1;
		(trpt+1)->bup.ovals = grab_ints(25);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->ConPurse.ConPurse_field1;
		((P0 *)this)->ConPurse.ConPurse_field1 = ((P0 *)this)->ConPurse.ConPurse_field1;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field1", ((P0 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		/* merge: ConPurse.ConPurse_field2 = ConPurse.ConPurse_field2(140, 116, 140) */
		reached[0][116] = 1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->ConPurse.ConPurse_field2;
		((P0 *)this)->ConPurse.ConPurse_field2 = ((P0 *)this)->ConPurse.ConPurse_field2;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field2", ((P0 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		/* merge: ConPurse.ConPurse_field3 = ConPurse.ConPurse_field3(140, 117, 140) */
		reached[0][117] = 1;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->ConPurse.ConPurse_field3;
		((P0 *)this)->ConPurse.ConPurse_field3 = ((P0 *)this)->ConPurse.ConPurse_field3;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field3", ((P0 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		/* merge: ConPurse.ConPurse_field4 = (ConPurse.ConPurse_field4+1)(140, 118, 140) */
		reached[0][118] = 1;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->ConPurse.ConPurse_field4;
		((P0 *)this)->ConPurse.ConPurse_field4 = (((P0 *)this)->ConPurse.ConPurse_field4+1);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field4", ((P0 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		/* merge: ConPurse.ConPurse_field5 = ConPurse.ConPurse_field1(140, 119, 140) */
		reached[0][119] = 1;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->ConPurse.ConPurse_field5;
		((P0 *)this)->ConPurse.ConPurse_field5 = ((P0 *)this)->ConPurse.ConPurse_field1;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field5", ((P0 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		/* merge: ConPurse.ConPurse_field6 = msg_in.msg_in_field2(140, 120, 140) */
		reached[0][120] = 1;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->ConPurse.ConPurse_field6;
		((P0 *)this)->ConPurse.ConPurse_field6 = ((P0 *)this)->msg_in.msg_in_field2;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field6", ((P0 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		/* merge: ConPurse.ConPurse_field7 = msg_in.msg_in_field3(140, 121, 140) */
		reached[0][121] = 1;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->ConPurse.ConPurse_field7;
		((P0 *)this)->ConPurse.ConPurse_field7 = ((P0 *)this)->msg_in.msg_in_field3;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field7", ((P0 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		/* merge: ConPurse.ConPurse_field8 = ConPurse.ConPurse_field4(140, 122, 140) */
		reached[0][122] = 1;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->ConPurse.ConPurse_field8;
		((P0 *)this)->ConPurse.ConPurse_field8 = ((P0 *)this)->ConPurse.ConPurse_field4;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field8", ((P0 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		/* merge: ConPurse.ConPurse_field9 = msg_in.msg_in_field4(140, 123, 140) */
		reached[0][123] = 1;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->ConPurse.ConPurse_field9;
		((P0 *)this)->ConPurse.ConPurse_field9 = ((P0 *)this)->msg_in.msg_in_field4;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field9", ((P0 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		/* merge: ConPurse.ConPurse_field10 = 8(140, 124, 140) */
		reached[0][124] = 1;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->ConPurse.ConPurse_field10;
		((P0 *)this)->ConPurse.ConPurse_field10 = 8;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field10", ((P0 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		/* merge: ConPurse.ConPurse_field11 = ConPurse.ConPurse_field11(140, 125, 140) */
		reached[0][125] = 1;
		(trpt+1)->bup.ovals[10] = ((P0 *)this)->ConPurse.ConPurse_field11;
		((P0 *)this)->ConPurse.ConPurse_field11 = ((P0 *)this)->ConPurse.ConPurse_field11;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field11", ((P0 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		/* merge: ConPurse.ConPurse_field12 = ConPurse.ConPurse_field12(140, 126, 140) */
		reached[0][126] = 1;
		(trpt+1)->bup.ovals[11] = ((P0 *)this)->ConPurse.ConPurse_field12;
		((P0 *)this)->ConPurse.ConPurse_field12 = ((P0 *)this)->ConPurse.ConPurse_field12;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field12", ((P0 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		/* merge: ConPurse.ConPurse_field13 = ConPurse.ConPurse_field13(140, 127, 140) */
		reached[0][127] = 1;
		(trpt+1)->bup.ovals[12] = ((P0 *)this)->ConPurse.ConPurse_field13;
		((P0 *)this)->ConPurse.ConPurse_field13 = ((P0 *)this)->ConPurse.ConPurse_field13;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field13", ((P0 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		/* merge: ConPurse.ConPurse_field14 = ConPurse.ConPurse_field14(140, 128, 140) */
		reached[0][128] = 1;
		(trpt+1)->bup.ovals[13] = ((P0 *)this)->ConPurse.ConPurse_field14;
		((P0 *)this)->ConPurse.ConPurse_field14 = ((P0 *)this)->ConPurse.ConPurse_field14;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field14", ((P0 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		/* merge: ConPurse.ConPurse_field15 = ConPurse.ConPurse_field15(140, 129, 140) */
		reached[0][129] = 1;
		(trpt+1)->bup.ovals[14] = ((P0 *)this)->ConPurse.ConPurse_field15;
		((P0 *)this)->ConPurse.ConPurse_field15 = ((P0 *)this)->ConPurse.ConPurse_field15;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field15", ((P0 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		/* merge: msg_out.msg_out_field1 = 4(140, 130, 140) */
		reached[0][130] = 1;
		(trpt+1)->bup.ovals[15] = ((P0 *)this)->msg_out.msg_out_field1;
		((P0 *)this)->msg_out.msg_out_field1 = 4;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field1", ((P0 *)this)->msg_out.msg_out_field1);
#endif
		;
		/* merge: msg_out.msg_out_field2 = msg_in.msg_in_field2(140, 131, 140) */
		reached[0][131] = 1;
		(trpt+1)->bup.ovals[16] = ((P0 *)this)->msg_out.msg_out_field2;
		((P0 *)this)->msg_out.msg_out_field2 = ((P0 *)this)->msg_in.msg_in_field2;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field2", ((P0 *)this)->msg_out.msg_out_field2);
#endif
		;
		/* merge: msg_out.msg_out_field3 = msg_in.msg_in_field3(140, 132, 140) */
		reached[0][132] = 1;
		(trpt+1)->bup.ovals[17] = ((P0 *)this)->msg_out.msg_out_field3;
		((P0 *)this)->msg_out.msg_out_field3 = ((P0 *)this)->msg_in.msg_in_field3;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field3", ((P0 *)this)->msg_out.msg_out_field3);
#endif
		;
		/* merge: msg_out.msg_out_field4 = msg_in.msg_in_field4(140, 133, 140) */
		reached[0][133] = 1;
		(trpt+1)->bup.ovals[18] = ((P0 *)this)->msg_out.msg_out_field4;
		((P0 *)this)->msg_out.msg_out_field4 = ((P0 *)this)->msg_in.msg_in_field4;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field4", ((P0 *)this)->msg_out.msg_out_field4);
#endif
		;
		/* merge: msg_out.msg_out_field5 = msg_in.msg_in_field2(140, 134, 140) */
		reached[0][134] = 1;
		(trpt+1)->bup.ovals[19] = ((P0 *)this)->msg_out.msg_out_field5;
		((P0 *)this)->msg_out.msg_out_field5 = ((P0 *)this)->msg_in.msg_in_field2;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field5", ((P0 *)this)->msg_out.msg_out_field5);
#endif
		;
		/* merge: msg_out.msg_out_field6 = ConPurse.ConPurse_field1(140, 135, 140) */
		reached[0][135] = 1;
		(trpt+1)->bup.ovals[20] = ((P0 *)this)->msg_out.msg_out_field6;
		((P0 *)this)->msg_out.msg_out_field6 = ((P0 *)this)->ConPurse.ConPurse_field1;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field6", ((P0 *)this)->msg_out.msg_out_field6);
#endif
		;
		/* merge: msg_out.msg_out_field7 = msg_in.msg_in_field3(140, 136, 140) */
		reached[0][136] = 1;
		(trpt+1)->bup.ovals[21] = ((P0 *)this)->msg_out.msg_out_field7;
		((P0 *)this)->msg_out.msg_out_field7 = ((P0 *)this)->msg_in.msg_in_field3;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field7", ((P0 *)this)->msg_out.msg_out_field7);
#endif
		;
		/* merge: msg_out.msg_out_field8 = ConPurse.ConPurse_field4(140, 137, 140) */
		reached[0][137] = 1;
		(trpt+1)->bup.ovals[22] = ((P0 *)this)->msg_out.msg_out_field8;
		((P0 *)this)->msg_out.msg_out_field8 = ((P0 *)this)->ConPurse.ConPurse_field4;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field8", ((P0 *)this)->msg_out.msg_out_field8);
#endif
		;
		/* merge: msg_out.msg_out_field9 = msg_in.msg_in_field4(140, 138, 140) */
		reached[0][138] = 1;
		(trpt+1)->bup.ovals[23] = ((P0 *)this)->msg_out.msg_out_field9;
		((P0 *)this)->msg_out.msg_out_field9 = ((P0 *)this)->msg_in.msg_in_field4;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field9", ((P0 *)this)->msg_out.msg_out_field9);
#endif
		;
		/* merge: msg_out.msg_out_field10 = msg_in.msg_in_field2(140, 139, 140) */
		reached[0][139] = 1;
		(trpt+1)->bup.ovals[24] = ((P0 *)this)->msg_out.msg_out_field10;
		((P0 *)this)->msg_out.msg_out_field10 = ((P0 *)this)->msg_in.msg_in_field2;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field10", ((P0 *)this)->msg_out.msg_out_field10);
#endif
		;
		_m = 3; goto P999; /* 24 */
	case 53: /* STATE 140 - mondex.pml:287 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][140] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P0 *)this)->ConPurse.ConPurse_field1, ((P0 *)this)->ConPurse.ConPurse_field2, ((P0 *)this)->ConPurse.ConPurse_field3, ((P0 *)this)->ConPurse.ConPurse_field4, ((P0 *)this)->ConPurse.ConPurse_field5, ((P0 *)this)->ConPurse.ConPurse_field6, ((P0 *)this)->ConPurse.ConPurse_field7, ((P0 *)this)->ConPurse.ConPurse_field8, ((P0 *)this)->ConPurse.ConPurse_field9, ((P0 *)this)->ConPurse.ConPurse_field10, ((P0 *)this)->ConPurse.ConPurse_field11, ((P0 *)this)->ConPurse.ConPurse_field12, ((P0 *)this)->ConPurse.ConPurse_field13, ((P0 *)this)->ConPurse.ConPurse_field14, ((P0 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 54: /* STATE 141 - mondex.pml:288 - [place_msg_out!msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10] (475:0:1 - 1) */
		IfNotBlocked
		reached[0][141] = 1;
		if (q_full(now.place_msg_out))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_out);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_out, 0, ((P0 *)this)->msg_out.msg_out_field1, ((P0 *)this)->msg_out.msg_out_field2, ((P0 *)this)->msg_out.msg_out_field3, ((P0 *)this)->msg_out.msg_out_field4, ((P0 *)this)->msg_out.msg_out_field5, ((P0 *)this)->msg_out.msg_out_field6, ((P0 *)this)->msg_out.msg_out_field7, ((P0 *)this)->msg_out.msg_out_field8, ((P0 *)this)->msg_out.msg_out_field9, ((P0 *)this)->msg_out.msg_out_field10, 0, 0, 0, 0, 0, 10);
		/* merge: startTo_is_enabled = 0(475, 142, 475) */
		reached[0][142] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->startTo_is_enabled);
		((P0 *)this)->startTo_is_enabled = 0;
#ifdef VAR_RANGES
		logval("Main:startTo_is_enabled", ((int)((P0 *)this)->startTo_is_enabled));
#endif
		;
		/* merge: .(goto)(475, 148, 475) */
		reached[0][148] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 2; goto P999; /* 3 */
	case 55: /* STATE 148 - mondex.pml:505 - [.(goto)] (0:475:0 - 2) */
		IfNotBlocked
		reached[0][148] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 1 */
	case 56: /* STATE 146 - mondex.pml:503 - [(1)] (475:0:0 - 1) */
		IfNotBlocked
		reached[0][146] = 1;
		if (!(1))
			continue;
		/* merge: .(goto)(475, 148, 475) */
		reached[0][148] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 57: /* STATE 151 - mondex.pml:112 - [((place_ConPurse?[ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15]&&place_msg_in?[msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10]))] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][151] = 1;
		if (!(((q_len(now.place_ConPurse) > 0)&&(q_len(now.place_msg_in) > 0))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 58: /* STATE 152 - mondex.pml:114 - [place_ConPurse?ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:15 - 1) */
		reached[0][152] = 1;
		if (q_len(now.place_ConPurse) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(15);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->ConPurse.ConPurse_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->ConPurse.ConPurse_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->ConPurse.ConPurse_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->ConPurse.ConPurse_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->ConPurse.ConPurse_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->ConPurse.ConPurse_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->ConPurse.ConPurse_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->ConPurse.ConPurse_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->ConPurse.ConPurse_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->ConPurse.ConPurse_field10;
		(trpt+1)->bup.ovals[10] = ((P0 *)this)->ConPurse.ConPurse_field11;
		(trpt+1)->bup.ovals[11] = ((P0 *)this)->ConPurse.ConPurse_field12;
		(trpt+1)->bup.ovals[12] = ((P0 *)this)->ConPurse.ConPurse_field13;
		(trpt+1)->bup.ovals[13] = ((P0 *)this)->ConPurse.ConPurse_field14;
		(trpt+1)->bup.ovals[14] = ((P0 *)this)->ConPurse.ConPurse_field15;
		;
		((P0 *)this)->ConPurse.ConPurse_field1 = qrecv(now.place_ConPurse, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field1", ((P0 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field2 = qrecv(now.place_ConPurse, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field2", ((P0 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field3 = qrecv(now.place_ConPurse, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field3", ((P0 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field4 = qrecv(now.place_ConPurse, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field4", ((P0 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field5 = qrecv(now.place_ConPurse, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field5", ((P0 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field6 = qrecv(now.place_ConPurse, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field6", ((P0 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field7 = qrecv(now.place_ConPurse, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field7", ((P0 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field8 = qrecv(now.place_ConPurse, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field8", ((P0 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field9 = qrecv(now.place_ConPurse, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field9", ((P0 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field10 = qrecv(now.place_ConPurse, XX-1, 9, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field10", ((P0 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field11 = qrecv(now.place_ConPurse, XX-1, 10, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field11", ((P0 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field12 = qrecv(now.place_ConPurse, XX-1, 11, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field12", ((P0 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field13 = qrecv(now.place_ConPurse, XX-1, 12, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field13", ((P0 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field14 = qrecv(now.place_ConPurse, XX-1, 13, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field14", ((P0 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field15 = qrecv(now.place_ConPurse, XX-1, 14, 1);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field15", ((P0 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 59: /* STATE 153 - mondex.pml:115 - [place_msg_in?msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:10 - 1) */
		reached[0][153] = 1;
		if (q_len(now.place_msg_in) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(10);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->msg_in.msg_in_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->msg_in.msg_in_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->msg_in.msg_in_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->msg_in.msg_in_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->msg_in.msg_in_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->msg_in.msg_in_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->msg_in.msg_in_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->msg_in.msg_in_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->msg_in.msg_in_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->msg_in.msg_in_field10;
		;
		((P0 *)this)->msg_in.msg_in_field1 = qrecv(now.place_msg_in, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field1", ((P0 *)this)->msg_in.msg_in_field1);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field2 = qrecv(now.place_msg_in, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field2", ((P0 *)this)->msg_in.msg_in_field2);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field3 = qrecv(now.place_msg_in, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field3", ((P0 *)this)->msg_in.msg_in_field3);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field4 = qrecv(now.place_msg_in, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field4", ((P0 *)this)->msg_in.msg_in_field4);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field5 = qrecv(now.place_msg_in, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field5", ((P0 *)this)->msg_in.msg_in_field5);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field6 = qrecv(now.place_msg_in, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field6", ((P0 *)this)->msg_in.msg_in_field6);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field7 = qrecv(now.place_msg_in, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field7", ((P0 *)this)->msg_in.msg_in_field7);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field8 = qrecv(now.place_msg_in, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field8", ((P0 *)this)->msg_in.msg_in_field8);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field9 = qrecv(now.place_msg_in, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field9", ((P0 *)this)->msg_in.msg_in_field9);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field10 = qrecv(now.place_msg_in, XX-1, 9, 1);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field10", ((P0 *)this)->msg_in.msg_in_field10);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 60: /* STATE 154 - mondex.pml:117 - [(((((((msg_in.msg_in_field1==9)&&(ConPurse.ConPurse_field1==msg_in.msg_in_field10))&&(ConPurse.ConPurse_field10==1))&&(ConPurse.ConPurse_field3==10))&&1)&&1))] (197:0:1 - 1) */
		IfNotBlocked
		reached[0][154] = 1;
		if (!(((((((((P0 *)this)->msg_in.msg_in_field1==9)&&(((P0 *)this)->ConPurse.ConPurse_field1==((P0 *)this)->msg_in.msg_in_field10))&&(((P0 *)this)->ConPurse.ConPurse_field10==1))&&(((P0 *)this)->ConPurse.ConPurse_field3==10))&&1)&&1)))
			continue;
		/* merge: readExceptionLog_is_enabled = 1(0, 156, 197) */
		reached[0][156] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->readExceptionLog_is_enabled);
		((P0 *)this)->readExceptionLog_is_enabled = 1;
#ifdef VAR_RANGES
		logval("Main:readExceptionLog_is_enabled", ((int)((P0 *)this)->readExceptionLog_is_enabled));
#endif
		;
		/* merge: .(goto)(0, 162, 197) */
		reached[0][162] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 61: /* STATE 158 - mondex.pml:119 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][158] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P0 *)this)->ConPurse.ConPurse_field1, ((P0 *)this)->ConPurse.ConPurse_field2, ((P0 *)this)->ConPurse.ConPurse_field3, ((P0 *)this)->ConPurse.ConPurse_field4, ((P0 *)this)->ConPurse.ConPurse_field5, ((P0 *)this)->ConPurse.ConPurse_field6, ((P0 *)this)->ConPurse.ConPurse_field7, ((P0 *)this)->ConPurse.ConPurse_field8, ((P0 *)this)->ConPurse.ConPurse_field9, ((P0 *)this)->ConPurse.ConPurse_field10, ((P0 *)this)->ConPurse.ConPurse_field11, ((P0 *)this)->ConPurse.ConPurse_field12, ((P0 *)this)->ConPurse.ConPurse_field13, ((P0 *)this)->ConPurse.ConPurse_field14, ((P0 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 62: /* STATE 159 - mondex.pml:120 - [place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][159] = 1;
		if (q_full(now.place_msg_in))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_in, 0, ((P0 *)this)->msg_in.msg_in_field1, ((P0 *)this)->msg_in.msg_in_field2, ((P0 *)this)->msg_in.msg_in_field3, ((P0 *)this)->msg_in.msg_in_field4, ((P0 *)this)->msg_in.msg_in_field5, ((P0 *)this)->msg_in.msg_in_field6, ((P0 *)this)->msg_in.msg_in_field7, ((P0 *)this)->msg_in.msg_in_field8, ((P0 *)this)->msg_in.msg_in_field9, ((P0 *)this)->msg_in.msg_in_field10, 0, 0, 0, 0, 0, 10);
		_m = 2; goto P999; /* 0 */
	case 63: /* STATE 164 - mondex.pml:509 - [(readExceptionLog_is_enabled)] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][164] = 1;
		if (!(((int)((P0 *)this)->readExceptionLog_is_enabled)))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: readExceptionLog_is_enabled */  (trpt+1)->bup.oval = ((P0 *)this)->readExceptionLog_is_enabled;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)this)->readExceptionLog_is_enabled = 0;
		_m = 3; goto P999; /* 0 */
	case 64: /* STATE 165 - mondex.pml:292 - [ConPurse.ConPurse_field1 = ConPurse.ConPurse_field1] (0:190:25 - 1) */
		IfNotBlocked
		reached[0][165] = 1;
		(trpt+1)->bup.ovals = grab_ints(25);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->ConPurse.ConPurse_field1;
		((P0 *)this)->ConPurse.ConPurse_field1 = ((P0 *)this)->ConPurse.ConPurse_field1;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field1", ((P0 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		/* merge: ConPurse.ConPurse_field2 = ConPurse.ConPurse_field2(190, 166, 190) */
		reached[0][166] = 1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->ConPurse.ConPurse_field2;
		((P0 *)this)->ConPurse.ConPurse_field2 = ((P0 *)this)->ConPurse.ConPurse_field2;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field2", ((P0 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		/* merge: ConPurse.ConPurse_field3 = ConPurse.ConPurse_field3(190, 167, 190) */
		reached[0][167] = 1;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->ConPurse.ConPurse_field3;
		((P0 *)this)->ConPurse.ConPurse_field3 = ((P0 *)this)->ConPurse.ConPurse_field3;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field3", ((P0 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		/* merge: ConPurse.ConPurse_field4 = ConPurse.ConPurse_field4(190, 168, 190) */
		reached[0][168] = 1;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->ConPurse.ConPurse_field4;
		((P0 *)this)->ConPurse.ConPurse_field4 = ((P0 *)this)->ConPurse.ConPurse_field4;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field4", ((P0 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		/* merge: ConPurse.ConPurse_field5 = ConPurse.ConPurse_field5(190, 169, 190) */
		reached[0][169] = 1;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->ConPurse.ConPurse_field5;
		((P0 *)this)->ConPurse.ConPurse_field5 = ((P0 *)this)->ConPurse.ConPurse_field5;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field5", ((P0 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		/* merge: ConPurse.ConPurse_field6 = ConPurse.ConPurse_field6(190, 170, 190) */
		reached[0][170] = 1;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->ConPurse.ConPurse_field6;
		((P0 *)this)->ConPurse.ConPurse_field6 = ((P0 *)this)->ConPurse.ConPurse_field6;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field6", ((P0 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		/* merge: ConPurse.ConPurse_field7 = ConPurse.ConPurse_field7(190, 171, 190) */
		reached[0][171] = 1;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->ConPurse.ConPurse_field7;
		((P0 *)this)->ConPurse.ConPurse_field7 = ((P0 *)this)->ConPurse.ConPurse_field7;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field7", ((P0 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		/* merge: ConPurse.ConPurse_field8 = ConPurse.ConPurse_field8(190, 172, 190) */
		reached[0][172] = 1;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->ConPurse.ConPurse_field8;
		((P0 *)this)->ConPurse.ConPurse_field8 = ((P0 *)this)->ConPurse.ConPurse_field8;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field8", ((P0 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		/* merge: ConPurse.ConPurse_field9 = ConPurse.ConPurse_field9(190, 173, 190) */
		reached[0][173] = 1;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->ConPurse.ConPurse_field9;
		((P0 *)this)->ConPurse.ConPurse_field9 = ((P0 *)this)->ConPurse.ConPurse_field9;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field9", ((P0 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		/* merge: ConPurse.ConPurse_field10 = ConPurse.ConPurse_field10(190, 174, 190) */
		reached[0][174] = 1;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->ConPurse.ConPurse_field10;
		((P0 *)this)->ConPurse.ConPurse_field10 = ((P0 *)this)->ConPurse.ConPurse_field10;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field10", ((P0 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		/* merge: ConPurse.ConPurse_field11 = ConPurse.ConPurse_field11(190, 175, 190) */
		reached[0][175] = 1;
		(trpt+1)->bup.ovals[10] = ((P0 *)this)->ConPurse.ConPurse_field11;
		((P0 *)this)->ConPurse.ConPurse_field11 = ((P0 *)this)->ConPurse.ConPurse_field11;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field11", ((P0 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		/* merge: ConPurse.ConPurse_field12 = ConPurse.ConPurse_field12(190, 176, 190) */
		reached[0][176] = 1;
		(trpt+1)->bup.ovals[11] = ((P0 *)this)->ConPurse.ConPurse_field12;
		((P0 *)this)->ConPurse.ConPurse_field12 = ((P0 *)this)->ConPurse.ConPurse_field12;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field12", ((P0 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		/* merge: ConPurse.ConPurse_field13 = ConPurse.ConPurse_field13(190, 177, 190) */
		reached[0][177] = 1;
		(trpt+1)->bup.ovals[12] = ((P0 *)this)->ConPurse.ConPurse_field13;
		((P0 *)this)->ConPurse.ConPurse_field13 = ((P0 *)this)->ConPurse.ConPurse_field13;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field13", ((P0 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		/* merge: ConPurse.ConPurse_field14 = ConPurse.ConPurse_field14(190, 178, 190) */
		reached[0][178] = 1;
		(trpt+1)->bup.ovals[13] = ((P0 *)this)->ConPurse.ConPurse_field14;
		((P0 *)this)->ConPurse.ConPurse_field14 = ((P0 *)this)->ConPurse.ConPurse_field14;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field14", ((P0 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		/* merge: ConPurse.ConPurse_field15 = ConPurse.ConPurse_field15(190, 179, 190) */
		reached[0][179] = 1;
		(trpt+1)->bup.ovals[14] = ((P0 *)this)->ConPurse.ConPurse_field15;
		((P0 *)this)->ConPurse.ConPurse_field15 = ((P0 *)this)->ConPurse.ConPurse_field15;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field15", ((P0 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		/* merge: msg_out.msg_out_field1 = 11(190, 180, 190) */
		reached[0][180] = 1;
		(trpt+1)->bup.ovals[15] = ((P0 *)this)->msg_out.msg_out_field1;
		((P0 *)this)->msg_out.msg_out_field1 = 11;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field1", ((P0 *)this)->msg_out.msg_out_field1);
#endif
		;
		/* merge: msg_out.msg_out_field2 = msg_in.msg_in_field2(190, 181, 190) */
		reached[0][181] = 1;
		(trpt+1)->bup.ovals[16] = ((P0 *)this)->msg_out.msg_out_field2;
		((P0 *)this)->msg_out.msg_out_field2 = ((P0 *)this)->msg_in.msg_in_field2;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field2", ((P0 *)this)->msg_out.msg_out_field2);
#endif
		;
		/* merge: msg_out.msg_out_field3 = msg_in.msg_in_field3(190, 182, 190) */
		reached[0][182] = 1;
		(trpt+1)->bup.ovals[17] = ((P0 *)this)->msg_out.msg_out_field3;
		((P0 *)this)->msg_out.msg_out_field3 = ((P0 *)this)->msg_in.msg_in_field3;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field3", ((P0 *)this)->msg_out.msg_out_field3);
#endif
		;
		/* merge: msg_out.msg_out_field4 = msg_in.msg_in_field4(190, 183, 190) */
		reached[0][183] = 1;
		(trpt+1)->bup.ovals[18] = ((P0 *)this)->msg_out.msg_out_field4;
		((P0 *)this)->msg_out.msg_out_field4 = ((P0 *)this)->msg_in.msg_in_field4;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field4", ((P0 *)this)->msg_out.msg_out_field4);
#endif
		;
		/* merge: msg_out.msg_out_field5 = ConPurse.ConPurse_field11(190, 184, 190) */
		reached[0][184] = 1;
		(trpt+1)->bup.ovals[19] = ((P0 *)this)->msg_out.msg_out_field5;
		((P0 *)this)->msg_out.msg_out_field5 = ((P0 *)this)->ConPurse.ConPurse_field11;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field5", ((P0 *)this)->msg_out.msg_out_field5);
#endif
		;
		/* merge: msg_out.msg_out_field6 = ConPurse.ConPurse_field12(190, 185, 190) */
		reached[0][185] = 1;
		(trpt+1)->bup.ovals[20] = ((P0 *)this)->msg_out.msg_out_field6;
		((P0 *)this)->msg_out.msg_out_field6 = ((P0 *)this)->ConPurse.ConPurse_field12;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field6", ((P0 *)this)->msg_out.msg_out_field6);
#endif
		;
		/* merge: msg_out.msg_out_field7 = ConPurse.ConPurse_field13(190, 186, 190) */
		reached[0][186] = 1;
		(trpt+1)->bup.ovals[21] = ((P0 *)this)->msg_out.msg_out_field7;
		((P0 *)this)->msg_out.msg_out_field7 = ((P0 *)this)->ConPurse.ConPurse_field13;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field7", ((P0 *)this)->msg_out.msg_out_field7);
#endif
		;
		/* merge: msg_out.msg_out_field8 = ConPurse.ConPurse_field14(190, 187, 190) */
		reached[0][187] = 1;
		(trpt+1)->bup.ovals[22] = ((P0 *)this)->msg_out.msg_out_field8;
		((P0 *)this)->msg_out.msg_out_field8 = ((P0 *)this)->ConPurse.ConPurse_field14;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field8", ((P0 *)this)->msg_out.msg_out_field8);
#endif
		;
		/* merge: msg_out.msg_out_field9 = ConPurse.ConPurse_field15(190, 188, 190) */
		reached[0][188] = 1;
		(trpt+1)->bup.ovals[23] = ((P0 *)this)->msg_out.msg_out_field9;
		((P0 *)this)->msg_out.msg_out_field9 = ((P0 *)this)->ConPurse.ConPurse_field15;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field9", ((P0 *)this)->msg_out.msg_out_field9);
#endif
		;
		/* merge: msg_out.msg_out_field10 = msg_in.msg_in_field10(190, 189, 190) */
		reached[0][189] = 1;
		(trpt+1)->bup.ovals[24] = ((P0 *)this)->msg_out.msg_out_field10;
		((P0 *)this)->msg_out.msg_out_field10 = ((P0 *)this)->msg_in.msg_in_field10;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field10", ((P0 *)this)->msg_out.msg_out_field10);
#endif
		;
		_m = 3; goto P999; /* 24 */
	case 65: /* STATE 190 - mondex.pml:318 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][190] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P0 *)this)->ConPurse.ConPurse_field1, ((P0 *)this)->ConPurse.ConPurse_field2, ((P0 *)this)->ConPurse.ConPurse_field3, ((P0 *)this)->ConPurse.ConPurse_field4, ((P0 *)this)->ConPurse.ConPurse_field5, ((P0 *)this)->ConPurse.ConPurse_field6, ((P0 *)this)->ConPurse.ConPurse_field7, ((P0 *)this)->ConPurse.ConPurse_field8, ((P0 *)this)->ConPurse.ConPurse_field9, ((P0 *)this)->ConPurse.ConPurse_field10, ((P0 *)this)->ConPurse.ConPurse_field11, ((P0 *)this)->ConPurse.ConPurse_field12, ((P0 *)this)->ConPurse.ConPurse_field13, ((P0 *)this)->ConPurse.ConPurse_field14, ((P0 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 66: /* STATE 191 - mondex.pml:319 - [place_msg_out!msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10] (475:0:1 - 1) */
		IfNotBlocked
		reached[0][191] = 1;
		if (q_full(now.place_msg_out))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_out);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_out, 0, ((P0 *)this)->msg_out.msg_out_field1, ((P0 *)this)->msg_out.msg_out_field2, ((P0 *)this)->msg_out.msg_out_field3, ((P0 *)this)->msg_out.msg_out_field4, ((P0 *)this)->msg_out.msg_out_field5, ((P0 *)this)->msg_out.msg_out_field6, ((P0 *)this)->msg_out.msg_out_field7, ((P0 *)this)->msg_out.msg_out_field8, ((P0 *)this)->msg_out.msg_out_field9, ((P0 *)this)->msg_out.msg_out_field10, 0, 0, 0, 0, 0, 10);
		/* merge: readExceptionLog_is_enabled = 0(475, 192, 475) */
		reached[0][192] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->readExceptionLog_is_enabled);
		((P0 *)this)->readExceptionLog_is_enabled = 0;
#ifdef VAR_RANGES
		logval("Main:readExceptionLog_is_enabled", ((int)((P0 *)this)->readExceptionLog_is_enabled));
#endif
		;
		/* merge: .(goto)(475, 198, 475) */
		reached[0][198] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 2; goto P999; /* 3 */
	case 67: /* STATE 198 - mondex.pml:512 - [.(goto)] (0:475:0 - 2) */
		IfNotBlocked
		reached[0][198] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 1 */
	case 68: /* STATE 196 - mondex.pml:510 - [(1)] (475:0:0 - 1) */
		IfNotBlocked
		reached[0][196] = 1;
		if (!(1))
			continue;
		/* merge: .(goto)(475, 198, 475) */
		reached[0][198] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 69: /* STATE 201 - mondex.pml:124 - [((place_ConPurse?[ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15]&&place_msg_in?[msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10]))] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][201] = 1;
		if (!(((q_len(now.place_ConPurse) > 0)&&(q_len(now.place_msg_in) > 0))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 70: /* STATE 202 - mondex.pml:126 - [place_ConPurse?ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:15 - 1) */
		reached[0][202] = 1;
		if (q_len(now.place_ConPurse) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(15);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->ConPurse.ConPurse_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->ConPurse.ConPurse_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->ConPurse.ConPurse_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->ConPurse.ConPurse_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->ConPurse.ConPurse_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->ConPurse.ConPurse_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->ConPurse.ConPurse_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->ConPurse.ConPurse_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->ConPurse.ConPurse_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->ConPurse.ConPurse_field10;
		(trpt+1)->bup.ovals[10] = ((P0 *)this)->ConPurse.ConPurse_field11;
		(trpt+1)->bup.ovals[11] = ((P0 *)this)->ConPurse.ConPurse_field12;
		(trpt+1)->bup.ovals[12] = ((P0 *)this)->ConPurse.ConPurse_field13;
		(trpt+1)->bup.ovals[13] = ((P0 *)this)->ConPurse.ConPurse_field14;
		(trpt+1)->bup.ovals[14] = ((P0 *)this)->ConPurse.ConPurse_field15;
		;
		((P0 *)this)->ConPurse.ConPurse_field1 = qrecv(now.place_ConPurse, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field1", ((P0 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field2 = qrecv(now.place_ConPurse, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field2", ((P0 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field3 = qrecv(now.place_ConPurse, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field3", ((P0 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field4 = qrecv(now.place_ConPurse, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field4", ((P0 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field5 = qrecv(now.place_ConPurse, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field5", ((P0 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field6 = qrecv(now.place_ConPurse, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field6", ((P0 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field7 = qrecv(now.place_ConPurse, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field7", ((P0 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field8 = qrecv(now.place_ConPurse, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field8", ((P0 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field9 = qrecv(now.place_ConPurse, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field9", ((P0 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field10 = qrecv(now.place_ConPurse, XX-1, 9, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field10", ((P0 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field11 = qrecv(now.place_ConPurse, XX-1, 10, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field11", ((P0 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field12 = qrecv(now.place_ConPurse, XX-1, 11, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field12", ((P0 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field13 = qrecv(now.place_ConPurse, XX-1, 12, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field13", ((P0 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field14 = qrecv(now.place_ConPurse, XX-1, 13, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field14", ((P0 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field15 = qrecv(now.place_ConPurse, XX-1, 14, 1);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field15", ((P0 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 71: /* STATE 203 - mondex.pml:127 - [place_msg_in?msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:10 - 1) */
		reached[0][203] = 1;
		if (q_len(now.place_msg_in) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(10);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->msg_in.msg_in_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->msg_in.msg_in_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->msg_in.msg_in_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->msg_in.msg_in_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->msg_in.msg_in_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->msg_in.msg_in_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->msg_in.msg_in_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->msg_in.msg_in_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->msg_in.msg_in_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->msg_in.msg_in_field10;
		;
		((P0 *)this)->msg_in.msg_in_field1 = qrecv(now.place_msg_in, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field1", ((P0 *)this)->msg_in.msg_in_field1);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field2 = qrecv(now.place_msg_in, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field2", ((P0 *)this)->msg_in.msg_in_field2);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field3 = qrecv(now.place_msg_in, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field3", ((P0 *)this)->msg_in.msg_in_field3);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field4 = qrecv(now.place_msg_in, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field4", ((P0 *)this)->msg_in.msg_in_field4);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field5 = qrecv(now.place_msg_in, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field5", ((P0 *)this)->msg_in.msg_in_field5);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field6 = qrecv(now.place_msg_in, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field6", ((P0 *)this)->msg_in.msg_in_field6);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field7 = qrecv(now.place_msg_in, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field7", ((P0 *)this)->msg_in.msg_in_field7);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field8 = qrecv(now.place_msg_in, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field8", ((P0 *)this)->msg_in.msg_in_field8);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field9 = qrecv(now.place_msg_in, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field9", ((P0 *)this)->msg_in.msg_in_field9);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field10 = qrecv(now.place_msg_in, XX-1, 9, 1);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field10", ((P0 *)this)->msg_in.msg_in_field10);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 72: /* STATE 204 - mondex.pml:129 - [(((((((msg_in.msg_in_field1==12)&&(ConPurse.ConPurse_field1==msg_in.msg_in_field10))&&(ConPurse.ConPurse_field10==1))&&(ConPurse.ConPurse_field3==10))&&1)&&1))] (247:0:1 - 1) */
		IfNotBlocked
		reached[0][204] = 1;
		if (!(((((((((P0 *)this)->msg_in.msg_in_field1==12)&&(((P0 *)this)->ConPurse.ConPurse_field1==((P0 *)this)->msg_in.msg_in_field10))&&(((P0 *)this)->ConPurse.ConPurse_field10==1))&&(((P0 *)this)->ConPurse.ConPurse_field3==10))&&1)&&1)))
			continue;
		/* merge: clearExceptionLog_is_enabled = 1(0, 206, 247) */
		reached[0][206] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->clearExceptionLog_is_enabled);
		((P0 *)this)->clearExceptionLog_is_enabled = 1;
#ifdef VAR_RANGES
		logval("Main:clearExceptionLog_is_enabled", ((int)((P0 *)this)->clearExceptionLog_is_enabled));
#endif
		;
		/* merge: .(goto)(0, 212, 247) */
		reached[0][212] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 73: /* STATE 208 - mondex.pml:131 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][208] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P0 *)this)->ConPurse.ConPurse_field1, ((P0 *)this)->ConPurse.ConPurse_field2, ((P0 *)this)->ConPurse.ConPurse_field3, ((P0 *)this)->ConPurse.ConPurse_field4, ((P0 *)this)->ConPurse.ConPurse_field5, ((P0 *)this)->ConPurse.ConPurse_field6, ((P0 *)this)->ConPurse.ConPurse_field7, ((P0 *)this)->ConPurse.ConPurse_field8, ((P0 *)this)->ConPurse.ConPurse_field9, ((P0 *)this)->ConPurse.ConPurse_field10, ((P0 *)this)->ConPurse.ConPurse_field11, ((P0 *)this)->ConPurse.ConPurse_field12, ((P0 *)this)->ConPurse.ConPurse_field13, ((P0 *)this)->ConPurse.ConPurse_field14, ((P0 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 74: /* STATE 209 - mondex.pml:132 - [place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][209] = 1;
		if (q_full(now.place_msg_in))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_in, 0, ((P0 *)this)->msg_in.msg_in_field1, ((P0 *)this)->msg_in.msg_in_field2, ((P0 *)this)->msg_in.msg_in_field3, ((P0 *)this)->msg_in.msg_in_field4, ((P0 *)this)->msg_in.msg_in_field5, ((P0 *)this)->msg_in.msg_in_field6, ((P0 *)this)->msg_in.msg_in_field7, ((P0 *)this)->msg_in.msg_in_field8, ((P0 *)this)->msg_in.msg_in_field9, ((P0 *)this)->msg_in.msg_in_field10, 0, 0, 0, 0, 0, 10);
		_m = 2; goto P999; /* 0 */
	case 75: /* STATE 214 - mondex.pml:516 - [(clearExceptionLog_is_enabled)] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][214] = 1;
		if (!(((int)((P0 *)this)->clearExceptionLog_is_enabled)))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: clearExceptionLog_is_enabled */  (trpt+1)->bup.oval = ((P0 *)this)->clearExceptionLog_is_enabled;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)this)->clearExceptionLog_is_enabled = 0;
		_m = 3; goto P999; /* 0 */
	case 76: /* STATE 215 - mondex.pml:323 - [ConPurse.ConPurse_field1 = ConPurse.ConPurse_field1] (0:240:25 - 1) */
		IfNotBlocked
		reached[0][215] = 1;
		(trpt+1)->bup.ovals = grab_ints(25);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->ConPurse.ConPurse_field1;
		((P0 *)this)->ConPurse.ConPurse_field1 = ((P0 *)this)->ConPurse.ConPurse_field1;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field1", ((P0 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		/* merge: ConPurse.ConPurse_field2 = ConPurse.ConPurse_field2(240, 216, 240) */
		reached[0][216] = 1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->ConPurse.ConPurse_field2;
		((P0 *)this)->ConPurse.ConPurse_field2 = ((P0 *)this)->ConPurse.ConPurse_field2;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field2", ((P0 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		/* merge: ConPurse.ConPurse_field3 = 13(240, 217, 240) */
		reached[0][217] = 1;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->ConPurse.ConPurse_field3;
		((P0 *)this)->ConPurse.ConPurse_field3 = 13;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field3", ((P0 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		/* merge: ConPurse.ConPurse_field4 = ConPurse.ConPurse_field4(240, 218, 240) */
		reached[0][218] = 1;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->ConPurse.ConPurse_field4;
		((P0 *)this)->ConPurse.ConPurse_field4 = ((P0 *)this)->ConPurse.ConPurse_field4;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field4", ((P0 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		/* merge: ConPurse.ConPurse_field5 = ConPurse.ConPurse_field5(240, 219, 240) */
		reached[0][219] = 1;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->ConPurse.ConPurse_field5;
		((P0 *)this)->ConPurse.ConPurse_field5 = ((P0 *)this)->ConPurse.ConPurse_field5;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field5", ((P0 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		/* merge: ConPurse.ConPurse_field6 = ConPurse.ConPurse_field6(240, 220, 240) */
		reached[0][220] = 1;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->ConPurse.ConPurse_field6;
		((P0 *)this)->ConPurse.ConPurse_field6 = ((P0 *)this)->ConPurse.ConPurse_field6;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field6", ((P0 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		/* merge: ConPurse.ConPurse_field7 = ConPurse.ConPurse_field7(240, 221, 240) */
		reached[0][221] = 1;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->ConPurse.ConPurse_field7;
		((P0 *)this)->ConPurse.ConPurse_field7 = ((P0 *)this)->ConPurse.ConPurse_field7;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field7", ((P0 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		/* merge: ConPurse.ConPurse_field8 = ConPurse.ConPurse_field8(240, 222, 240) */
		reached[0][222] = 1;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->ConPurse.ConPurse_field8;
		((P0 *)this)->ConPurse.ConPurse_field8 = ((P0 *)this)->ConPurse.ConPurse_field8;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field8", ((P0 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		/* merge: ConPurse.ConPurse_field9 = ConPurse.ConPurse_field9(240, 223, 240) */
		reached[0][223] = 1;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->ConPurse.ConPurse_field9;
		((P0 *)this)->ConPurse.ConPurse_field9 = ((P0 *)this)->ConPurse.ConPurse_field9;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field9", ((P0 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		/* merge: ConPurse.ConPurse_field10 = ConPurse.ConPurse_field10(240, 224, 240) */
		reached[0][224] = 1;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->ConPurse.ConPurse_field10;
		((P0 *)this)->ConPurse.ConPurse_field10 = ((P0 *)this)->ConPurse.ConPurse_field10;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field10", ((P0 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		/* merge: ConPurse.ConPurse_field11 = ConPurse.ConPurse_field11(240, 225, 240) */
		reached[0][225] = 1;
		(trpt+1)->bup.ovals[10] = ((P0 *)this)->ConPurse.ConPurse_field11;
		((P0 *)this)->ConPurse.ConPurse_field11 = ((P0 *)this)->ConPurse.ConPurse_field11;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field11", ((P0 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		/* merge: ConPurse.ConPurse_field12 = ConPurse.ConPurse_field12(240, 226, 240) */
		reached[0][226] = 1;
		(trpt+1)->bup.ovals[11] = ((P0 *)this)->ConPurse.ConPurse_field12;
		((P0 *)this)->ConPurse.ConPurse_field12 = ((P0 *)this)->ConPurse.ConPurse_field12;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field12", ((P0 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		/* merge: ConPurse.ConPurse_field13 = ConPurse.ConPurse_field13(240, 227, 240) */
		reached[0][227] = 1;
		(trpt+1)->bup.ovals[12] = ((P0 *)this)->ConPurse.ConPurse_field13;
		((P0 *)this)->ConPurse.ConPurse_field13 = ((P0 *)this)->ConPurse.ConPurse_field13;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field13", ((P0 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		/* merge: ConPurse.ConPurse_field14 = ConPurse.ConPurse_field14(240, 228, 240) */
		reached[0][228] = 1;
		(trpt+1)->bup.ovals[13] = ((P0 *)this)->ConPurse.ConPurse_field14;
		((P0 *)this)->ConPurse.ConPurse_field14 = ((P0 *)this)->ConPurse.ConPurse_field14;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field14", ((P0 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		/* merge: ConPurse.ConPurse_field15 = ConPurse.ConPurse_field15(240, 229, 240) */
		reached[0][229] = 1;
		(trpt+1)->bup.ovals[14] = ((P0 *)this)->ConPurse.ConPurse_field15;
		((P0 *)this)->ConPurse.ConPurse_field15 = ((P0 *)this)->ConPurse.ConPurse_field15;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field15", ((P0 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		/* merge: msg_out.msg_out_field1 = 3(240, 230, 240) */
		reached[0][230] = 1;
		(trpt+1)->bup.ovals[15] = ((P0 *)this)->msg_out.msg_out_field1;
		((P0 *)this)->msg_out.msg_out_field1 = 3;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field1", ((P0 *)this)->msg_out.msg_out_field1);
#endif
		;
		/* merge: msg_out.msg_out_field2 = msg_in.msg_in_field2(240, 231, 240) */
		reached[0][231] = 1;
		(trpt+1)->bup.ovals[16] = ((P0 *)this)->msg_out.msg_out_field2;
		((P0 *)this)->msg_out.msg_out_field2 = ((P0 *)this)->msg_in.msg_in_field2;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field2", ((P0 *)this)->msg_out.msg_out_field2);
#endif
		;
		/* merge: msg_out.msg_out_field3 = msg_in.msg_in_field3(240, 232, 240) */
		reached[0][232] = 1;
		(trpt+1)->bup.ovals[17] = ((P0 *)this)->msg_out.msg_out_field3;
		((P0 *)this)->msg_out.msg_out_field3 = ((P0 *)this)->msg_in.msg_in_field3;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field3", ((P0 *)this)->msg_out.msg_out_field3);
#endif
		;
		/* merge: msg_out.msg_out_field4 = msg_in.msg_in_field4(240, 233, 240) */
		reached[0][233] = 1;
		(trpt+1)->bup.ovals[18] = ((P0 *)this)->msg_out.msg_out_field4;
		((P0 *)this)->msg_out.msg_out_field4 = ((P0 *)this)->msg_in.msg_in_field4;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field4", ((P0 *)this)->msg_out.msg_out_field4);
#endif
		;
		/* merge: msg_out.msg_out_field5 = msg_in.msg_in_field5(240, 234, 240) */
		reached[0][234] = 1;
		(trpt+1)->bup.ovals[19] = ((P0 *)this)->msg_out.msg_out_field5;
		((P0 *)this)->msg_out.msg_out_field5 = ((P0 *)this)->msg_in.msg_in_field5;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field5", ((P0 *)this)->msg_out.msg_out_field5);
#endif
		;
		/* merge: msg_out.msg_out_field6 = msg_in.msg_in_field6(240, 235, 240) */
		reached[0][235] = 1;
		(trpt+1)->bup.ovals[20] = ((P0 *)this)->msg_out.msg_out_field6;
		((P0 *)this)->msg_out.msg_out_field6 = ((P0 *)this)->msg_in.msg_in_field6;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field6", ((P0 *)this)->msg_out.msg_out_field6);
#endif
		;
		/* merge: msg_out.msg_out_field7 = msg_in.msg_in_field7(240, 236, 240) */
		reached[0][236] = 1;
		(trpt+1)->bup.ovals[21] = ((P0 *)this)->msg_out.msg_out_field7;
		((P0 *)this)->msg_out.msg_out_field7 = ((P0 *)this)->msg_in.msg_in_field7;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field7", ((P0 *)this)->msg_out.msg_out_field7);
#endif
		;
		/* merge: msg_out.msg_out_field8 = msg_in.msg_in_field8(240, 237, 240) */
		reached[0][237] = 1;
		(trpt+1)->bup.ovals[22] = ((P0 *)this)->msg_out.msg_out_field8;
		((P0 *)this)->msg_out.msg_out_field8 = ((P0 *)this)->msg_in.msg_in_field8;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field8", ((P0 *)this)->msg_out.msg_out_field8);
#endif
		;
		/* merge: msg_out.msg_out_field9 = msg_in.msg_in_field9(240, 238, 240) */
		reached[0][238] = 1;
		(trpt+1)->bup.ovals[23] = ((P0 *)this)->msg_out.msg_out_field9;
		((P0 *)this)->msg_out.msg_out_field9 = ((P0 *)this)->msg_in.msg_in_field9;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field9", ((P0 *)this)->msg_out.msg_out_field9);
#endif
		;
		/* merge: msg_out.msg_out_field10 = msg_in.msg_in_field10(240, 239, 240) */
		reached[0][239] = 1;
		(trpt+1)->bup.ovals[24] = ((P0 *)this)->msg_out.msg_out_field10;
		((P0 *)this)->msg_out.msg_out_field10 = ((P0 *)this)->msg_in.msg_in_field10;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field10", ((P0 *)this)->msg_out.msg_out_field10);
#endif
		;
		_m = 3; goto P999; /* 24 */
	case 77: /* STATE 240 - mondex.pml:350 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][240] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P0 *)this)->ConPurse.ConPurse_field1, ((P0 *)this)->ConPurse.ConPurse_field2, ((P0 *)this)->ConPurse.ConPurse_field3, ((P0 *)this)->ConPurse.ConPurse_field4, ((P0 *)this)->ConPurse.ConPurse_field5, ((P0 *)this)->ConPurse.ConPurse_field6, ((P0 *)this)->ConPurse.ConPurse_field7, ((P0 *)this)->ConPurse.ConPurse_field8, ((P0 *)this)->ConPurse.ConPurse_field9, ((P0 *)this)->ConPurse.ConPurse_field10, ((P0 *)this)->ConPurse.ConPurse_field11, ((P0 *)this)->ConPurse.ConPurse_field12, ((P0 *)this)->ConPurse.ConPurse_field13, ((P0 *)this)->ConPurse.ConPurse_field14, ((P0 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 78: /* STATE 241 - mondex.pml:351 - [place_msg_out!msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10] (475:0:1 - 1) */
		IfNotBlocked
		reached[0][241] = 1;
		if (q_full(now.place_msg_out))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_out);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_out, 0, ((P0 *)this)->msg_out.msg_out_field1, ((P0 *)this)->msg_out.msg_out_field2, ((P0 *)this)->msg_out.msg_out_field3, ((P0 *)this)->msg_out.msg_out_field4, ((P0 *)this)->msg_out.msg_out_field5, ((P0 *)this)->msg_out.msg_out_field6, ((P0 *)this)->msg_out.msg_out_field7, ((P0 *)this)->msg_out.msg_out_field8, ((P0 *)this)->msg_out.msg_out_field9, ((P0 *)this)->msg_out.msg_out_field10, 0, 0, 0, 0, 0, 10);
		/* merge: clearExceptionLog_is_enabled = 0(475, 242, 475) */
		reached[0][242] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->clearExceptionLog_is_enabled);
		((P0 *)this)->clearExceptionLog_is_enabled = 0;
#ifdef VAR_RANGES
		logval("Main:clearExceptionLog_is_enabled", ((int)((P0 *)this)->clearExceptionLog_is_enabled));
#endif
		;
		/* merge: .(goto)(475, 248, 475) */
		reached[0][248] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 2; goto P999; /* 3 */
	case 79: /* STATE 248 - mondex.pml:519 - [.(goto)] (0:475:0 - 2) */
		IfNotBlocked
		reached[0][248] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 1 */
	case 80: /* STATE 246 - mondex.pml:517 - [(1)] (475:0:0 - 1) */
		IfNotBlocked
		reached[0][246] = 1;
		if (!(1))
			continue;
		/* merge: .(goto)(475, 248, 475) */
		reached[0][248] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 81: /* STATE 251 - mondex.pml:136 - [((place_ConPurse?[ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15]&&place_msg_in?[msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10]))] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][251] = 1;
		if (!(((q_len(now.place_ConPurse) > 0)&&(q_len(now.place_msg_in) > 0))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 82: /* STATE 252 - mondex.pml:138 - [place_ConPurse?ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:15 - 1) */
		reached[0][252] = 1;
		if (q_len(now.place_ConPurse) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(15);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->ConPurse.ConPurse_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->ConPurse.ConPurse_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->ConPurse.ConPurse_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->ConPurse.ConPurse_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->ConPurse.ConPurse_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->ConPurse.ConPurse_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->ConPurse.ConPurse_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->ConPurse.ConPurse_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->ConPurse.ConPurse_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->ConPurse.ConPurse_field10;
		(trpt+1)->bup.ovals[10] = ((P0 *)this)->ConPurse.ConPurse_field11;
		(trpt+1)->bup.ovals[11] = ((P0 *)this)->ConPurse.ConPurse_field12;
		(trpt+1)->bup.ovals[12] = ((P0 *)this)->ConPurse.ConPurse_field13;
		(trpt+1)->bup.ovals[13] = ((P0 *)this)->ConPurse.ConPurse_field14;
		(trpt+1)->bup.ovals[14] = ((P0 *)this)->ConPurse.ConPurse_field15;
		;
		((P0 *)this)->ConPurse.ConPurse_field1 = qrecv(now.place_ConPurse, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field1", ((P0 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field2 = qrecv(now.place_ConPurse, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field2", ((P0 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field3 = qrecv(now.place_ConPurse, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field3", ((P0 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field4 = qrecv(now.place_ConPurse, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field4", ((P0 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field5 = qrecv(now.place_ConPurse, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field5", ((P0 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field6 = qrecv(now.place_ConPurse, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field6", ((P0 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field7 = qrecv(now.place_ConPurse, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field7", ((P0 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field8 = qrecv(now.place_ConPurse, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field8", ((P0 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field9 = qrecv(now.place_ConPurse, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field9", ((P0 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field10 = qrecv(now.place_ConPurse, XX-1, 9, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field10", ((P0 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field11 = qrecv(now.place_ConPurse, XX-1, 10, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field11", ((P0 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field12 = qrecv(now.place_ConPurse, XX-1, 11, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field12", ((P0 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field13 = qrecv(now.place_ConPurse, XX-1, 12, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field13", ((P0 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field14 = qrecv(now.place_ConPurse, XX-1, 13, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field14", ((P0 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field15 = qrecv(now.place_ConPurse, XX-1, 14, 1);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field15", ((P0 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 83: /* STATE 253 - mondex.pml:139 - [place_msg_in?msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:10 - 1) */
		reached[0][253] = 1;
		if (q_len(now.place_msg_in) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(10);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->msg_in.msg_in_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->msg_in.msg_in_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->msg_in.msg_in_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->msg_in.msg_in_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->msg_in.msg_in_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->msg_in.msg_in_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->msg_in.msg_in_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->msg_in.msg_in_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->msg_in.msg_in_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->msg_in.msg_in_field10;
		;
		((P0 *)this)->msg_in.msg_in_field1 = qrecv(now.place_msg_in, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field1", ((P0 *)this)->msg_in.msg_in_field1);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field2 = qrecv(now.place_msg_in, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field2", ((P0 *)this)->msg_in.msg_in_field2);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field3 = qrecv(now.place_msg_in, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field3", ((P0 *)this)->msg_in.msg_in_field3);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field4 = qrecv(now.place_msg_in, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field4", ((P0 *)this)->msg_in.msg_in_field4);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field5 = qrecv(now.place_msg_in, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field5", ((P0 *)this)->msg_in.msg_in_field5);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field6 = qrecv(now.place_msg_in, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field6", ((P0 *)this)->msg_in.msg_in_field6);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field7 = qrecv(now.place_msg_in, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field7", ((P0 *)this)->msg_in.msg_in_field7);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field8 = qrecv(now.place_msg_in, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field8", ((P0 *)this)->msg_in.msg_in_field8);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field9 = qrecv(now.place_msg_in, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field9", ((P0 *)this)->msg_in.msg_in_field9);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field10 = qrecv(now.place_msg_in, XX-1, 9, 1);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field10", ((P0 *)this)->msg_in.msg_in_field10);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 84: /* STATE 254 - mondex.pml:141 - [((((((msg_in.msg_in_field1==14)&&(ConPurse.ConPurse_field1==msg_in.msg_in_field10))&&(ConPurse.ConPurse_field10==5))&&1)&&1))] (297:0:1 - 1) */
		IfNotBlocked
		reached[0][254] = 1;
		if (!((((((((P0 *)this)->msg_in.msg_in_field1==14)&&(((P0 *)this)->ConPurse.ConPurse_field1==((P0 *)this)->msg_in.msg_in_field10))&&(((P0 *)this)->ConPurse.ConPurse_field10==5))&&1)&&1)))
			continue;
		/* merge: ack_is_enabled = 1(0, 256, 297) */
		reached[0][256] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->ack_is_enabled);
		((P0 *)this)->ack_is_enabled = 1;
#ifdef VAR_RANGES
		logval("Main:ack_is_enabled", ((int)((P0 *)this)->ack_is_enabled));
#endif
		;
		/* merge: .(goto)(0, 262, 297) */
		reached[0][262] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 85: /* STATE 258 - mondex.pml:143 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][258] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P0 *)this)->ConPurse.ConPurse_field1, ((P0 *)this)->ConPurse.ConPurse_field2, ((P0 *)this)->ConPurse.ConPurse_field3, ((P0 *)this)->ConPurse.ConPurse_field4, ((P0 *)this)->ConPurse.ConPurse_field5, ((P0 *)this)->ConPurse.ConPurse_field6, ((P0 *)this)->ConPurse.ConPurse_field7, ((P0 *)this)->ConPurse.ConPurse_field8, ((P0 *)this)->ConPurse.ConPurse_field9, ((P0 *)this)->ConPurse.ConPurse_field10, ((P0 *)this)->ConPurse.ConPurse_field11, ((P0 *)this)->ConPurse.ConPurse_field12, ((P0 *)this)->ConPurse.ConPurse_field13, ((P0 *)this)->ConPurse.ConPurse_field14, ((P0 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 86: /* STATE 259 - mondex.pml:144 - [place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][259] = 1;
		if (q_full(now.place_msg_in))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_in, 0, ((P0 *)this)->msg_in.msg_in_field1, ((P0 *)this)->msg_in.msg_in_field2, ((P0 *)this)->msg_in.msg_in_field3, ((P0 *)this)->msg_in.msg_in_field4, ((P0 *)this)->msg_in.msg_in_field5, ((P0 *)this)->msg_in.msg_in_field6, ((P0 *)this)->msg_in.msg_in_field7, ((P0 *)this)->msg_in.msg_in_field8, ((P0 *)this)->msg_in.msg_in_field9, ((P0 *)this)->msg_in.msg_in_field10, 0, 0, 0, 0, 0, 10);
		_m = 2; goto P999; /* 0 */
	case 87: /* STATE 264 - mondex.pml:523 - [(ack_is_enabled)] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][264] = 1;
		if (!(((int)((P0 *)this)->ack_is_enabled)))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: ack_is_enabled */  (trpt+1)->bup.oval = ((P0 *)this)->ack_is_enabled;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)this)->ack_is_enabled = 0;
		_m = 3; goto P999; /* 0 */
	case 88: /* STATE 265 - mondex.pml:355 - [ConPurse.ConPurse_field1 = ConPurse.ConPurse_field1] (0:290:25 - 1) */
		IfNotBlocked
		reached[0][265] = 1;
		(trpt+1)->bup.ovals = grab_ints(25);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->ConPurse.ConPurse_field1;
		((P0 *)this)->ConPurse.ConPurse_field1 = ((P0 *)this)->ConPurse.ConPurse_field1;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field1", ((P0 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		/* merge: ConPurse.ConPurse_field2 = ConPurse.ConPurse_field2(290, 266, 290) */
		reached[0][266] = 1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->ConPurse.ConPurse_field2;
		((P0 *)this)->ConPurse.ConPurse_field2 = ((P0 *)this)->ConPurse.ConPurse_field2;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field2", ((P0 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		/* merge: ConPurse.ConPurse_field3 = ConPurse.ConPurse_field3(290, 267, 290) */
		reached[0][267] = 1;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->ConPurse.ConPurse_field3;
		((P0 *)this)->ConPurse.ConPurse_field3 = ((P0 *)this)->ConPurse.ConPurse_field3;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field3", ((P0 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		/* merge: ConPurse.ConPurse_field4 = ConPurse.ConPurse_field4(290, 268, 290) */
		reached[0][268] = 1;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->ConPurse.ConPurse_field4;
		((P0 *)this)->ConPurse.ConPurse_field4 = ((P0 *)this)->ConPurse.ConPurse_field4;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field4", ((P0 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		/* merge: ConPurse.ConPurse_field5 = ConPurse.ConPurse_field5(290, 269, 290) */
		reached[0][269] = 1;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->ConPurse.ConPurse_field5;
		((P0 *)this)->ConPurse.ConPurse_field5 = ((P0 *)this)->ConPurse.ConPurse_field5;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field5", ((P0 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		/* merge: ConPurse.ConPurse_field6 = ConPurse.ConPurse_field6(290, 270, 290) */
		reached[0][270] = 1;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->ConPurse.ConPurse_field6;
		((P0 *)this)->ConPurse.ConPurse_field6 = ((P0 *)this)->ConPurse.ConPurse_field6;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field6", ((P0 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		/* merge: ConPurse.ConPurse_field7 = ConPurse.ConPurse_field7(290, 271, 290) */
		reached[0][271] = 1;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->ConPurse.ConPurse_field7;
		((P0 *)this)->ConPurse.ConPurse_field7 = ((P0 *)this)->ConPurse.ConPurse_field7;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field7", ((P0 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		/* merge: ConPurse.ConPurse_field8 = ConPurse.ConPurse_field8(290, 272, 290) */
		reached[0][272] = 1;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->ConPurse.ConPurse_field8;
		((P0 *)this)->ConPurse.ConPurse_field8 = ((P0 *)this)->ConPurse.ConPurse_field8;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field8", ((P0 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		/* merge: ConPurse.ConPurse_field9 = ConPurse.ConPurse_field9(290, 273, 290) */
		reached[0][273] = 1;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->ConPurse.ConPurse_field9;
		((P0 *)this)->ConPurse.ConPurse_field9 = ((P0 *)this)->ConPurse.ConPurse_field9;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field9", ((P0 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		/* merge: ConPurse.ConPurse_field10 = 1(290, 274, 290) */
		reached[0][274] = 1;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->ConPurse.ConPurse_field10;
		((P0 *)this)->ConPurse.ConPurse_field10 = 1;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field10", ((P0 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		/* merge: ConPurse.ConPurse_field11 = ConPurse.ConPurse_field11(290, 275, 290) */
		reached[0][275] = 1;
		(trpt+1)->bup.ovals[10] = ((P0 *)this)->ConPurse.ConPurse_field11;
		((P0 *)this)->ConPurse.ConPurse_field11 = ((P0 *)this)->ConPurse.ConPurse_field11;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field11", ((P0 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		/* merge: ConPurse.ConPurse_field12 = ConPurse.ConPurse_field12(290, 276, 290) */
		reached[0][276] = 1;
		(trpt+1)->bup.ovals[11] = ((P0 *)this)->ConPurse.ConPurse_field12;
		((P0 *)this)->ConPurse.ConPurse_field12 = ((P0 *)this)->ConPurse.ConPurse_field12;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field12", ((P0 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		/* merge: ConPurse.ConPurse_field13 = ConPurse.ConPurse_field13(290, 277, 290) */
		reached[0][277] = 1;
		(trpt+1)->bup.ovals[12] = ((P0 *)this)->ConPurse.ConPurse_field13;
		((P0 *)this)->ConPurse.ConPurse_field13 = ((P0 *)this)->ConPurse.ConPurse_field13;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field13", ((P0 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		/* merge: ConPurse.ConPurse_field14 = ConPurse.ConPurse_field14(290, 278, 290) */
		reached[0][278] = 1;
		(trpt+1)->bup.ovals[13] = ((P0 *)this)->ConPurse.ConPurse_field14;
		((P0 *)this)->ConPurse.ConPurse_field14 = ((P0 *)this)->ConPurse.ConPurse_field14;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field14", ((P0 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		/* merge: ConPurse.ConPurse_field15 = ConPurse.ConPurse_field15(290, 279, 290) */
		reached[0][279] = 1;
		(trpt+1)->bup.ovals[14] = ((P0 *)this)->ConPurse.ConPurse_field15;
		((P0 *)this)->ConPurse.ConPurse_field15 = ((P0 *)this)->ConPurse.ConPurse_field15;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field15", ((P0 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		/* merge: msg_out.msg_out_field1 = 3(290, 280, 290) */
		reached[0][280] = 1;
		(trpt+1)->bup.ovals[15] = ((P0 *)this)->msg_out.msg_out_field1;
		((P0 *)this)->msg_out.msg_out_field1 = 3;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field1", ((P0 *)this)->msg_out.msg_out_field1);
#endif
		;
		/* merge: msg_out.msg_out_field2 = msg_in.msg_in_field2(290, 281, 290) */
		reached[0][281] = 1;
		(trpt+1)->bup.ovals[16] = ((P0 *)this)->msg_out.msg_out_field2;
		((P0 *)this)->msg_out.msg_out_field2 = ((P0 *)this)->msg_in.msg_in_field2;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field2", ((P0 *)this)->msg_out.msg_out_field2);
#endif
		;
		/* merge: msg_out.msg_out_field3 = msg_in.msg_in_field3(290, 282, 290) */
		reached[0][282] = 1;
		(trpt+1)->bup.ovals[17] = ((P0 *)this)->msg_out.msg_out_field3;
		((P0 *)this)->msg_out.msg_out_field3 = ((P0 *)this)->msg_in.msg_in_field3;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field3", ((P0 *)this)->msg_out.msg_out_field3);
#endif
		;
		/* merge: msg_out.msg_out_field4 = msg_in.msg_in_field4(290, 283, 290) */
		reached[0][283] = 1;
		(trpt+1)->bup.ovals[18] = ((P0 *)this)->msg_out.msg_out_field4;
		((P0 *)this)->msg_out.msg_out_field4 = ((P0 *)this)->msg_in.msg_in_field4;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field4", ((P0 *)this)->msg_out.msg_out_field4);
#endif
		;
		/* merge: msg_out.msg_out_field5 = msg_in.msg_in_field5(290, 284, 290) */
		reached[0][284] = 1;
		(trpt+1)->bup.ovals[19] = ((P0 *)this)->msg_out.msg_out_field5;
		((P0 *)this)->msg_out.msg_out_field5 = ((P0 *)this)->msg_in.msg_in_field5;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field5", ((P0 *)this)->msg_out.msg_out_field5);
#endif
		;
		/* merge: msg_out.msg_out_field6 = msg_in.msg_in_field6(290, 285, 290) */
		reached[0][285] = 1;
		(trpt+1)->bup.ovals[20] = ((P0 *)this)->msg_out.msg_out_field6;
		((P0 *)this)->msg_out.msg_out_field6 = ((P0 *)this)->msg_in.msg_in_field6;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field6", ((P0 *)this)->msg_out.msg_out_field6);
#endif
		;
		/* merge: msg_out.msg_out_field7 = msg_in.msg_in_field7(290, 286, 290) */
		reached[0][286] = 1;
		(trpt+1)->bup.ovals[21] = ((P0 *)this)->msg_out.msg_out_field7;
		((P0 *)this)->msg_out.msg_out_field7 = ((P0 *)this)->msg_in.msg_in_field7;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field7", ((P0 *)this)->msg_out.msg_out_field7);
#endif
		;
		/* merge: msg_out.msg_out_field8 = msg_in.msg_in_field8(290, 287, 290) */
		reached[0][287] = 1;
		(trpt+1)->bup.ovals[22] = ((P0 *)this)->msg_out.msg_out_field8;
		((P0 *)this)->msg_out.msg_out_field8 = ((P0 *)this)->msg_in.msg_in_field8;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field8", ((P0 *)this)->msg_out.msg_out_field8);
#endif
		;
		/* merge: msg_out.msg_out_field9 = msg_in.msg_in_field9(290, 288, 290) */
		reached[0][288] = 1;
		(trpt+1)->bup.ovals[23] = ((P0 *)this)->msg_out.msg_out_field9;
		((P0 *)this)->msg_out.msg_out_field9 = ((P0 *)this)->msg_in.msg_in_field9;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field9", ((P0 *)this)->msg_out.msg_out_field9);
#endif
		;
		/* merge: msg_out.msg_out_field10 = msg_in.msg_in_field10(290, 289, 290) */
		reached[0][289] = 1;
		(trpt+1)->bup.ovals[24] = ((P0 *)this)->msg_out.msg_out_field10;
		((P0 *)this)->msg_out.msg_out_field10 = ((P0 *)this)->msg_in.msg_in_field10;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field10", ((P0 *)this)->msg_out.msg_out_field10);
#endif
		;
		_m = 3; goto P999; /* 24 */
	case 89: /* STATE 290 - mondex.pml:382 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][290] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P0 *)this)->ConPurse.ConPurse_field1, ((P0 *)this)->ConPurse.ConPurse_field2, ((P0 *)this)->ConPurse.ConPurse_field3, ((P0 *)this)->ConPurse.ConPurse_field4, ((P0 *)this)->ConPurse.ConPurse_field5, ((P0 *)this)->ConPurse.ConPurse_field6, ((P0 *)this)->ConPurse.ConPurse_field7, ((P0 *)this)->ConPurse.ConPurse_field8, ((P0 *)this)->ConPurse.ConPurse_field9, ((P0 *)this)->ConPurse.ConPurse_field10, ((P0 *)this)->ConPurse.ConPurse_field11, ((P0 *)this)->ConPurse.ConPurse_field12, ((P0 *)this)->ConPurse.ConPurse_field13, ((P0 *)this)->ConPurse.ConPurse_field14, ((P0 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 90: /* STATE 291 - mondex.pml:383 - [place_msg_out!msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10] (475:0:1 - 1) */
		IfNotBlocked
		reached[0][291] = 1;
		if (q_full(now.place_msg_out))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_out);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_out, 0, ((P0 *)this)->msg_out.msg_out_field1, ((P0 *)this)->msg_out.msg_out_field2, ((P0 *)this)->msg_out.msg_out_field3, ((P0 *)this)->msg_out.msg_out_field4, ((P0 *)this)->msg_out.msg_out_field5, ((P0 *)this)->msg_out.msg_out_field6, ((P0 *)this)->msg_out.msg_out_field7, ((P0 *)this)->msg_out.msg_out_field8, ((P0 *)this)->msg_out.msg_out_field9, ((P0 *)this)->msg_out.msg_out_field10, 0, 0, 0, 0, 0, 10);
		/* merge: ack_is_enabled = 0(475, 292, 475) */
		reached[0][292] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->ack_is_enabled);
		((P0 *)this)->ack_is_enabled = 0;
#ifdef VAR_RANGES
		logval("Main:ack_is_enabled", ((int)((P0 *)this)->ack_is_enabled));
#endif
		;
		/* merge: .(goto)(475, 298, 475) */
		reached[0][298] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 2; goto P999; /* 3 */
	case 91: /* STATE 298 - mondex.pml:526 - [.(goto)] (0:475:0 - 2) */
		IfNotBlocked
		reached[0][298] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 1 */
	case 92: /* STATE 296 - mondex.pml:524 - [(1)] (475:0:0 - 1) */
		IfNotBlocked
		reached[0][296] = 1;
		if (!(1))
			continue;
		/* merge: .(goto)(475, 298, 475) */
		reached[0][298] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 93: /* STATE 301 - mondex.pml:148 - [((place_ConPurse?[ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15]&&place_msg_in?[msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10]))] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][301] = 1;
		if (!(((q_len(now.place_ConPurse) > 0)&&(q_len(now.place_msg_in) > 0))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 94: /* STATE 302 - mondex.pml:150 - [place_ConPurse?ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:15 - 1) */
		reached[0][302] = 1;
		if (q_len(now.place_ConPurse) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(15);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->ConPurse.ConPurse_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->ConPurse.ConPurse_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->ConPurse.ConPurse_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->ConPurse.ConPurse_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->ConPurse.ConPurse_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->ConPurse.ConPurse_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->ConPurse.ConPurse_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->ConPurse.ConPurse_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->ConPurse.ConPurse_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->ConPurse.ConPurse_field10;
		(trpt+1)->bup.ovals[10] = ((P0 *)this)->ConPurse.ConPurse_field11;
		(trpt+1)->bup.ovals[11] = ((P0 *)this)->ConPurse.ConPurse_field12;
		(trpt+1)->bup.ovals[12] = ((P0 *)this)->ConPurse.ConPurse_field13;
		(trpt+1)->bup.ovals[13] = ((P0 *)this)->ConPurse.ConPurse_field14;
		(trpt+1)->bup.ovals[14] = ((P0 *)this)->ConPurse.ConPurse_field15;
		;
		((P0 *)this)->ConPurse.ConPurse_field1 = qrecv(now.place_ConPurse, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field1", ((P0 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field2 = qrecv(now.place_ConPurse, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field2", ((P0 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field3 = qrecv(now.place_ConPurse, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field3", ((P0 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field4 = qrecv(now.place_ConPurse, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field4", ((P0 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field5 = qrecv(now.place_ConPurse, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field5", ((P0 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field6 = qrecv(now.place_ConPurse, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field6", ((P0 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field7 = qrecv(now.place_ConPurse, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field7", ((P0 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field8 = qrecv(now.place_ConPurse, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field8", ((P0 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field9 = qrecv(now.place_ConPurse, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field9", ((P0 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field10 = qrecv(now.place_ConPurse, XX-1, 9, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field10", ((P0 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field11 = qrecv(now.place_ConPurse, XX-1, 10, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field11", ((P0 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field12 = qrecv(now.place_ConPurse, XX-1, 11, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field12", ((P0 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field13 = qrecv(now.place_ConPurse, XX-1, 12, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field13", ((P0 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field14 = qrecv(now.place_ConPurse, XX-1, 13, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field14", ((P0 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field15 = qrecv(now.place_ConPurse, XX-1, 14, 1);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field15", ((P0 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 95: /* STATE 303 - mondex.pml:151 - [place_msg_in?msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:10 - 1) */
		reached[0][303] = 1;
		if (q_len(now.place_msg_in) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(10);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->msg_in.msg_in_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->msg_in.msg_in_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->msg_in.msg_in_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->msg_in.msg_in_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->msg_in.msg_in_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->msg_in.msg_in_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->msg_in.msg_in_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->msg_in.msg_in_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->msg_in.msg_in_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->msg_in.msg_in_field10;
		;
		((P0 *)this)->msg_in.msg_in_field1 = qrecv(now.place_msg_in, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field1", ((P0 *)this)->msg_in.msg_in_field1);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field2 = qrecv(now.place_msg_in, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field2", ((P0 *)this)->msg_in.msg_in_field2);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field3 = qrecv(now.place_msg_in, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field3", ((P0 *)this)->msg_in.msg_in_field3);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field4 = qrecv(now.place_msg_in, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field4", ((P0 *)this)->msg_in.msg_in_field4);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field5 = qrecv(now.place_msg_in, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field5", ((P0 *)this)->msg_in.msg_in_field5);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field6 = qrecv(now.place_msg_in, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field6", ((P0 *)this)->msg_in.msg_in_field6);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field7 = qrecv(now.place_msg_in, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field7", ((P0 *)this)->msg_in.msg_in_field7);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field8 = qrecv(now.place_msg_in, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field8", ((P0 *)this)->msg_in.msg_in_field8);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field9 = qrecv(now.place_msg_in, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field9", ((P0 *)this)->msg_in.msg_in_field9);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field10 = qrecv(now.place_msg_in, XX-1, 9, 1);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field10", ((P0 *)this)->msg_in.msg_in_field10);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 96: /* STATE 304 - mondex.pml:153 - [((((((msg_in.msg_in_field1==6)&&(ConPurse.ConPurse_field1==msg_in.msg_in_field10))&&(ConPurse.ConPurse_field10==8))&&1)&&1))] (347:0:1 - 1) */
		IfNotBlocked
		reached[0][304] = 1;
		if (!((((((((P0 *)this)->msg_in.msg_in_field1==6)&&(((P0 *)this)->ConPurse.ConPurse_field1==((P0 *)this)->msg_in.msg_in_field10))&&(((P0 *)this)->ConPurse.ConPurse_field10==8))&&1)&&1)))
			continue;
		/* merge: val_is_enabled = 1(0, 306, 347) */
		reached[0][306] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->val_is_enabled);
		((P0 *)this)->val_is_enabled = 1;
#ifdef VAR_RANGES
		logval("Main:val_is_enabled", ((int)((P0 *)this)->val_is_enabled));
#endif
		;
		/* merge: .(goto)(0, 312, 347) */
		reached[0][312] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 97: /* STATE 308 - mondex.pml:155 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][308] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P0 *)this)->ConPurse.ConPurse_field1, ((P0 *)this)->ConPurse.ConPurse_field2, ((P0 *)this)->ConPurse.ConPurse_field3, ((P0 *)this)->ConPurse.ConPurse_field4, ((P0 *)this)->ConPurse.ConPurse_field5, ((P0 *)this)->ConPurse.ConPurse_field6, ((P0 *)this)->ConPurse.ConPurse_field7, ((P0 *)this)->ConPurse.ConPurse_field8, ((P0 *)this)->ConPurse.ConPurse_field9, ((P0 *)this)->ConPurse.ConPurse_field10, ((P0 *)this)->ConPurse.ConPurse_field11, ((P0 *)this)->ConPurse.ConPurse_field12, ((P0 *)this)->ConPurse.ConPurse_field13, ((P0 *)this)->ConPurse.ConPurse_field14, ((P0 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 98: /* STATE 309 - mondex.pml:156 - [place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][309] = 1;
		if (q_full(now.place_msg_in))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_in, 0, ((P0 *)this)->msg_in.msg_in_field1, ((P0 *)this)->msg_in.msg_in_field2, ((P0 *)this)->msg_in.msg_in_field3, ((P0 *)this)->msg_in.msg_in_field4, ((P0 *)this)->msg_in.msg_in_field5, ((P0 *)this)->msg_in.msg_in_field6, ((P0 *)this)->msg_in.msg_in_field7, ((P0 *)this)->msg_in.msg_in_field8, ((P0 *)this)->msg_in.msg_in_field9, ((P0 *)this)->msg_in.msg_in_field10, 0, 0, 0, 0, 0, 10);
		_m = 2; goto P999; /* 0 */
	case 99: /* STATE 314 - mondex.pml:530 - [(val_is_enabled)] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][314] = 1;
		if (!(((int)((P0 *)this)->val_is_enabled)))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: val_is_enabled */  (trpt+1)->bup.oval = ((P0 *)this)->val_is_enabled;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)this)->val_is_enabled = 0;
		_m = 3; goto P999; /* 0 */
	case 100: /* STATE 315 - mondex.pml:387 - [ConPurse.ConPurse_field1 = ConPurse.ConPurse_field1] (0:340:25 - 1) */
		IfNotBlocked
		reached[0][315] = 1;
		(trpt+1)->bup.ovals = grab_ints(25);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->ConPurse.ConPurse_field1;
		((P0 *)this)->ConPurse.ConPurse_field1 = ((P0 *)this)->ConPurse.ConPurse_field1;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field1", ((P0 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		/* merge: ConPurse.ConPurse_field2 = (ConPurse.ConPurse_field2+msg_in.msg_in_field7)(340, 316, 340) */
		reached[0][316] = 1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->ConPurse.ConPurse_field2;
		((P0 *)this)->ConPurse.ConPurse_field2 = (((P0 *)this)->ConPurse.ConPurse_field2+((P0 *)this)->msg_in.msg_in_field7);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field2", ((P0 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		/* merge: ConPurse.ConPurse_field3 = ConPurse.ConPurse_field3(340, 317, 340) */
		reached[0][317] = 1;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->ConPurse.ConPurse_field3;
		((P0 *)this)->ConPurse.ConPurse_field3 = ((P0 *)this)->ConPurse.ConPurse_field3;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field3", ((P0 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		/* merge: ConPurse.ConPurse_field4 = ConPurse.ConPurse_field4(340, 318, 340) */
		reached[0][318] = 1;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->ConPurse.ConPurse_field4;
		((P0 *)this)->ConPurse.ConPurse_field4 = ((P0 *)this)->ConPurse.ConPurse_field4;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field4", ((P0 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		/* merge: ConPurse.ConPurse_field5 = ConPurse.ConPurse_field5(340, 319, 340) */
		reached[0][319] = 1;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->ConPurse.ConPurse_field5;
		((P0 *)this)->ConPurse.ConPurse_field5 = ((P0 *)this)->ConPurse.ConPurse_field5;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field5", ((P0 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		/* merge: ConPurse.ConPurse_field6 = ConPurse.ConPurse_field6(340, 320, 340) */
		reached[0][320] = 1;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->ConPurse.ConPurse_field6;
		((P0 *)this)->ConPurse.ConPurse_field6 = ((P0 *)this)->ConPurse.ConPurse_field6;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field6", ((P0 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		/* merge: ConPurse.ConPurse_field7 = ConPurse.ConPurse_field7(340, 321, 340) */
		reached[0][321] = 1;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->ConPurse.ConPurse_field7;
		((P0 *)this)->ConPurse.ConPurse_field7 = ((P0 *)this)->ConPurse.ConPurse_field7;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field7", ((P0 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		/* merge: ConPurse.ConPurse_field8 = ConPurse.ConPurse_field8(340, 322, 340) */
		reached[0][322] = 1;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->ConPurse.ConPurse_field8;
		((P0 *)this)->ConPurse.ConPurse_field8 = ((P0 *)this)->ConPurse.ConPurse_field8;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field8", ((P0 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		/* merge: ConPurse.ConPurse_field9 = ConPurse.ConPurse_field9(340, 323, 340) */
		reached[0][323] = 1;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->ConPurse.ConPurse_field9;
		((P0 *)this)->ConPurse.ConPurse_field9 = ((P0 *)this)->ConPurse.ConPurse_field9;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field9", ((P0 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		/* merge: ConPurse.ConPurse_field10 = 1(340, 324, 340) */
		reached[0][324] = 1;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->ConPurse.ConPurse_field10;
		((P0 *)this)->ConPurse.ConPurse_field10 = 1;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field10", ((P0 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		/* merge: ConPurse.ConPurse_field11 = ConPurse.ConPurse_field11(340, 325, 340) */
		reached[0][325] = 1;
		(trpt+1)->bup.ovals[10] = ((P0 *)this)->ConPurse.ConPurse_field11;
		((P0 *)this)->ConPurse.ConPurse_field11 = ((P0 *)this)->ConPurse.ConPurse_field11;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field11", ((P0 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		/* merge: ConPurse.ConPurse_field12 = ConPurse.ConPurse_field12(340, 326, 340) */
		reached[0][326] = 1;
		(trpt+1)->bup.ovals[11] = ((P0 *)this)->ConPurse.ConPurse_field12;
		((P0 *)this)->ConPurse.ConPurse_field12 = ((P0 *)this)->ConPurse.ConPurse_field12;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field12", ((P0 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		/* merge: ConPurse.ConPurse_field13 = ConPurse.ConPurse_field13(340, 327, 340) */
		reached[0][327] = 1;
		(trpt+1)->bup.ovals[12] = ((P0 *)this)->ConPurse.ConPurse_field13;
		((P0 *)this)->ConPurse.ConPurse_field13 = ((P0 *)this)->ConPurse.ConPurse_field13;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field13", ((P0 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		/* merge: ConPurse.ConPurse_field14 = ConPurse.ConPurse_field14(340, 328, 340) */
		reached[0][328] = 1;
		(trpt+1)->bup.ovals[13] = ((P0 *)this)->ConPurse.ConPurse_field14;
		((P0 *)this)->ConPurse.ConPurse_field14 = ((P0 *)this)->ConPurse.ConPurse_field14;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field14", ((P0 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		/* merge: ConPurse.ConPurse_field15 = ConPurse.ConPurse_field15(340, 329, 340) */
		reached[0][329] = 1;
		(trpt+1)->bup.ovals[14] = ((P0 *)this)->ConPurse.ConPurse_field15;
		((P0 *)this)->ConPurse.ConPurse_field15 = ((P0 *)this)->ConPurse.ConPurse_field15;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field15", ((P0 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		/* merge: msg_out.msg_out_field1 = 14(340, 330, 340) */
		reached[0][330] = 1;
		(trpt+1)->bup.ovals[15] = ((P0 *)this)->msg_out.msg_out_field1;
		((P0 *)this)->msg_out.msg_out_field1 = 14;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field1", ((P0 *)this)->msg_out.msg_out_field1);
#endif
		;
		/* merge: msg_out.msg_out_field2 = msg_in.msg_in_field2(340, 331, 340) */
		reached[0][331] = 1;
		(trpt+1)->bup.ovals[16] = ((P0 *)this)->msg_out.msg_out_field2;
		((P0 *)this)->msg_out.msg_out_field2 = ((P0 *)this)->msg_in.msg_in_field2;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field2", ((P0 *)this)->msg_out.msg_out_field2);
#endif
		;
		/* merge: msg_out.msg_out_field3 = msg_in.msg_in_field3(340, 332, 340) */
		reached[0][332] = 1;
		(trpt+1)->bup.ovals[17] = ((P0 *)this)->msg_out.msg_out_field3;
		((P0 *)this)->msg_out.msg_out_field3 = ((P0 *)this)->msg_in.msg_in_field3;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field3", ((P0 *)this)->msg_out.msg_out_field3);
#endif
		;
		/* merge: msg_out.msg_out_field4 = msg_in.msg_in_field4(340, 333, 340) */
		reached[0][333] = 1;
		(trpt+1)->bup.ovals[18] = ((P0 *)this)->msg_out.msg_out_field4;
		((P0 *)this)->msg_out.msg_out_field4 = ((P0 *)this)->msg_in.msg_in_field4;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field4", ((P0 *)this)->msg_out.msg_out_field4);
#endif
		;
		/* merge: msg_out.msg_out_field5 = ConPurse.ConPurse_field5(340, 334, 340) */
		reached[0][334] = 1;
		(trpt+1)->bup.ovals[19] = ((P0 *)this)->msg_out.msg_out_field5;
		((P0 *)this)->msg_out.msg_out_field5 = ((P0 *)this)->ConPurse.ConPurse_field5;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field5", ((P0 *)this)->msg_out.msg_out_field5);
#endif
		;
		/* merge: msg_out.msg_out_field6 = ConPurse.ConPurse_field6(340, 335, 340) */
		reached[0][335] = 1;
		(trpt+1)->bup.ovals[20] = ((P0 *)this)->msg_out.msg_out_field6;
		((P0 *)this)->msg_out.msg_out_field6 = ((P0 *)this)->ConPurse.ConPurse_field6;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field6", ((P0 *)this)->msg_out.msg_out_field6);
#endif
		;
		/* merge: msg_out.msg_out_field7 = ConPurse.ConPurse_field7(340, 336, 340) */
		reached[0][336] = 1;
		(trpt+1)->bup.ovals[21] = ((P0 *)this)->msg_out.msg_out_field7;
		((P0 *)this)->msg_out.msg_out_field7 = ((P0 *)this)->ConPurse.ConPurse_field7;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field7", ((P0 *)this)->msg_out.msg_out_field7);
#endif
		;
		/* merge: msg_out.msg_out_field8 = ConPurse.ConPurse_field8(340, 337, 340) */
		reached[0][337] = 1;
		(trpt+1)->bup.ovals[22] = ((P0 *)this)->msg_out.msg_out_field8;
		((P0 *)this)->msg_out.msg_out_field8 = ((P0 *)this)->ConPurse.ConPurse_field8;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field8", ((P0 *)this)->msg_out.msg_out_field8);
#endif
		;
		/* merge: msg_out.msg_out_field9 = ConPurse.ConPurse_field9(340, 338, 340) */
		reached[0][338] = 1;
		(trpt+1)->bup.ovals[23] = ((P0 *)this)->msg_out.msg_out_field9;
		((P0 *)this)->msg_out.msg_out_field9 = ((P0 *)this)->ConPurse.ConPurse_field9;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field9", ((P0 *)this)->msg_out.msg_out_field9);
#endif
		;
		/* merge: msg_out.msg_out_field10 = msg_in.msg_in_field5(340, 339, 340) */
		reached[0][339] = 1;
		(trpt+1)->bup.ovals[24] = ((P0 *)this)->msg_out.msg_out_field10;
		((P0 *)this)->msg_out.msg_out_field10 = ((P0 *)this)->msg_in.msg_in_field5;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field10", ((P0 *)this)->msg_out.msg_out_field10);
#endif
		;
		_m = 3; goto P999; /* 24 */
	case 101: /* STATE 340 - mondex.pml:414 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][340] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P0 *)this)->ConPurse.ConPurse_field1, ((P0 *)this)->ConPurse.ConPurse_field2, ((P0 *)this)->ConPurse.ConPurse_field3, ((P0 *)this)->ConPurse.ConPurse_field4, ((P0 *)this)->ConPurse.ConPurse_field5, ((P0 *)this)->ConPurse.ConPurse_field6, ((P0 *)this)->ConPurse.ConPurse_field7, ((P0 *)this)->ConPurse.ConPurse_field8, ((P0 *)this)->ConPurse.ConPurse_field9, ((P0 *)this)->ConPurse.ConPurse_field10, ((P0 *)this)->ConPurse.ConPurse_field11, ((P0 *)this)->ConPurse.ConPurse_field12, ((P0 *)this)->ConPurse.ConPurse_field13, ((P0 *)this)->ConPurse.ConPurse_field14, ((P0 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 102: /* STATE 341 - mondex.pml:415 - [place_msg_out!msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10] (475:0:1 - 1) */
		IfNotBlocked
		reached[0][341] = 1;
		if (q_full(now.place_msg_out))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_out);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_out, 0, ((P0 *)this)->msg_out.msg_out_field1, ((P0 *)this)->msg_out.msg_out_field2, ((P0 *)this)->msg_out.msg_out_field3, ((P0 *)this)->msg_out.msg_out_field4, ((P0 *)this)->msg_out.msg_out_field5, ((P0 *)this)->msg_out.msg_out_field6, ((P0 *)this)->msg_out.msg_out_field7, ((P0 *)this)->msg_out.msg_out_field8, ((P0 *)this)->msg_out.msg_out_field9, ((P0 *)this)->msg_out.msg_out_field10, 0, 0, 0, 0, 0, 10);
		/* merge: val_is_enabled = 0(475, 342, 475) */
		reached[0][342] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->val_is_enabled);
		((P0 *)this)->val_is_enabled = 0;
#ifdef VAR_RANGES
		logval("Main:val_is_enabled", ((int)((P0 *)this)->val_is_enabled));
#endif
		;
		/* merge: .(goto)(475, 348, 475) */
		reached[0][348] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 2; goto P999; /* 3 */
	case 103: /* STATE 348 - mondex.pml:533 - [.(goto)] (0:475:0 - 2) */
		IfNotBlocked
		reached[0][348] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 1 */
	case 104: /* STATE 346 - mondex.pml:531 - [(1)] (475:0:0 - 1) */
		IfNotBlocked
		reached[0][346] = 1;
		if (!(1))
			continue;
		/* merge: .(goto)(475, 348, 475) */
		reached[0][348] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 105: /* STATE 351 - mondex.pml:160 - [(((place_ConPurse?[ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15]&&place_LogBook?[LogBook.LogBook_field1,LogBook.LogBook_field2,LogBook.LogBook_field3,LogBook.LogBook_field4,LogBook.LogBook_field5,LogBook.LogBook_field6])&&place_msg_in?[msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10]))] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][351] = 1;
		if (!((((q_len(now.place_ConPurse) > 0)&&(q_len(now.place_LogBook) > 0))&&(q_len(now.place_msg_in) > 0))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 106: /* STATE 352 - mondex.pml:162 - [place_ConPurse?ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:15 - 1) */
		reached[0][352] = 1;
		if (q_len(now.place_ConPurse) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(15);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->ConPurse.ConPurse_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->ConPurse.ConPurse_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->ConPurse.ConPurse_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->ConPurse.ConPurse_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->ConPurse.ConPurse_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->ConPurse.ConPurse_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->ConPurse.ConPurse_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->ConPurse.ConPurse_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->ConPurse.ConPurse_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->ConPurse.ConPurse_field10;
		(trpt+1)->bup.ovals[10] = ((P0 *)this)->ConPurse.ConPurse_field11;
		(trpt+1)->bup.ovals[11] = ((P0 *)this)->ConPurse.ConPurse_field12;
		(trpt+1)->bup.ovals[12] = ((P0 *)this)->ConPurse.ConPurse_field13;
		(trpt+1)->bup.ovals[13] = ((P0 *)this)->ConPurse.ConPurse_field14;
		(trpt+1)->bup.ovals[14] = ((P0 *)this)->ConPurse.ConPurse_field15;
		;
		((P0 *)this)->ConPurse.ConPurse_field1 = qrecv(now.place_ConPurse, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field1", ((P0 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field2 = qrecv(now.place_ConPurse, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field2", ((P0 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field3 = qrecv(now.place_ConPurse, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field3", ((P0 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field4 = qrecv(now.place_ConPurse, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field4", ((P0 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field5 = qrecv(now.place_ConPurse, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field5", ((P0 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field6 = qrecv(now.place_ConPurse, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field6", ((P0 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field7 = qrecv(now.place_ConPurse, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field7", ((P0 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field8 = qrecv(now.place_ConPurse, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field8", ((P0 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field9 = qrecv(now.place_ConPurse, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field9", ((P0 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field10 = qrecv(now.place_ConPurse, XX-1, 9, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field10", ((P0 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field11 = qrecv(now.place_ConPurse, XX-1, 10, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field11", ((P0 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field12 = qrecv(now.place_ConPurse, XX-1, 11, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field12", ((P0 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field13 = qrecv(now.place_ConPurse, XX-1, 12, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field13", ((P0 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field14 = qrecv(now.place_ConPurse, XX-1, 13, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field14", ((P0 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field15 = qrecv(now.place_ConPurse, XX-1, 14, 1);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field15", ((P0 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 107: /* STATE 353 - mondex.pml:163 - [place_LogBook?LogBook.LogBook_field1,LogBook.LogBook_field2,LogBook.LogBook_field3,LogBook.LogBook_field4,LogBook.LogBook_field5,LogBook.LogBook_field6] (0:0:6 - 1) */
		reached[0][353] = 1;
		if (q_len(now.place_LogBook) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(6);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->LogBook.LogBook_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->LogBook.LogBook_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->LogBook.LogBook_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->LogBook.LogBook_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->LogBook.LogBook_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->LogBook.LogBook_field6;
		;
		((P0 *)this)->LogBook.LogBook_field1 = qrecv(now.place_LogBook, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:LogBook.LogBook_field1", ((P0 *)this)->LogBook.LogBook_field1);
#endif
		;
		((P0 *)this)->LogBook.LogBook_field2 = qrecv(now.place_LogBook, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:LogBook.LogBook_field2", ((P0 *)this)->LogBook.LogBook_field2);
#endif
		;
		((P0 *)this)->LogBook.LogBook_field3 = qrecv(now.place_LogBook, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:LogBook.LogBook_field3", ((P0 *)this)->LogBook.LogBook_field3);
#endif
		;
		((P0 *)this)->LogBook.LogBook_field4 = qrecv(now.place_LogBook, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:LogBook.LogBook_field4", ((P0 *)this)->LogBook.LogBook_field4);
#endif
		;
		((P0 *)this)->LogBook.LogBook_field5 = qrecv(now.place_LogBook, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:LogBook.LogBook_field5", ((P0 *)this)->LogBook.LogBook_field5);
#endif
		;
		((P0 *)this)->LogBook.LogBook_field6 = qrecv(now.place_LogBook, XX-1, 5, 1);
#ifdef VAR_RANGES
		logval("Main:LogBook.LogBook_field6", ((P0 *)this)->LogBook.LogBook_field6);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_LogBook);
		sprintf(simtmp, "%d", ((P0 *)this)->LogBook.LogBook_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->LogBook.LogBook_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->LogBook.LogBook_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->LogBook.LogBook_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->LogBook.LogBook_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->LogBook.LogBook_field6); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 108: /* STATE 354 - mondex.pml:164 - [place_msg_in?msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:10 - 1) */
		reached[0][354] = 1;
		if (q_len(now.place_msg_in) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(10);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->msg_in.msg_in_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->msg_in.msg_in_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->msg_in.msg_in_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->msg_in.msg_in_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->msg_in.msg_in_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->msg_in.msg_in_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->msg_in.msg_in_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->msg_in.msg_in_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->msg_in.msg_in_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->msg_in.msg_in_field10;
		;
		((P0 *)this)->msg_in.msg_in_field1 = qrecv(now.place_msg_in, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field1", ((P0 *)this)->msg_in.msg_in_field1);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field2 = qrecv(now.place_msg_in, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field2", ((P0 *)this)->msg_in.msg_in_field2);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field3 = qrecv(now.place_msg_in, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field3", ((P0 *)this)->msg_in.msg_in_field3);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field4 = qrecv(now.place_msg_in, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field4", ((P0 *)this)->msg_in.msg_in_field4);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field5 = qrecv(now.place_msg_in, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field5", ((P0 *)this)->msg_in.msg_in_field5);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field6 = qrecv(now.place_msg_in, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field6", ((P0 *)this)->msg_in.msg_in_field6);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field7 = qrecv(now.place_msg_in, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field7", ((P0 *)this)->msg_in.msg_in_field7);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field8 = qrecv(now.place_msg_in, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field8", ((P0 *)this)->msg_in.msg_in_field8);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field9 = qrecv(now.place_msg_in, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field9", ((P0 *)this)->msg_in.msg_in_field9);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field10 = qrecv(now.place_msg_in, XX-1, 9, 1);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field10", ((P0 *)this)->msg_in.msg_in_field10);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 109: /* STATE 355 - mondex.pml:166 - [((((((((msg_in.msg_in_field1==0)||(msg_in.msg_in_field1==7))||(msg_in.msg_in_field1==12))&&(ConPurse.ConPurse_field1==msg_in.msg_in_field10))&&((ConPurse.ConPurse_field10==8)||(ConPurse.ConPurse_field10==5)))&&1)&&1))] (389:0:1 - 1) */
		IfNotBlocked
		reached[0][355] = 1;
		if (!((((((((((P0 *)this)->msg_in.msg_in_field1==0)||(((P0 *)this)->msg_in.msg_in_field1==7))||(((P0 *)this)->msg_in.msg_in_field1==12))&&(((P0 *)this)->ConPurse.ConPurse_field1==((P0 *)this)->msg_in.msg_in_field10))&&((((P0 *)this)->ConPurse.ConPurse_field10==8)||(((P0 *)this)->ConPurse.ConPurse_field10==5)))&&1)&&1)))
			continue;
		/* merge: Abort_is_enabled = 1(0, 357, 389) */
		reached[0][357] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->Abort_is_enabled);
		((P0 *)this)->Abort_is_enabled = 1;
#ifdef VAR_RANGES
		logval("Main:Abort_is_enabled", ((int)((P0 *)this)->Abort_is_enabled));
#endif
		;
		/* merge: .(goto)(0, 364, 389) */
		reached[0][364] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 110: /* STATE 359 - mondex.pml:168 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][359] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P0 *)this)->ConPurse.ConPurse_field1, ((P0 *)this)->ConPurse.ConPurse_field2, ((P0 *)this)->ConPurse.ConPurse_field3, ((P0 *)this)->ConPurse.ConPurse_field4, ((P0 *)this)->ConPurse.ConPurse_field5, ((P0 *)this)->ConPurse.ConPurse_field6, ((P0 *)this)->ConPurse.ConPurse_field7, ((P0 *)this)->ConPurse.ConPurse_field8, ((P0 *)this)->ConPurse.ConPurse_field9, ((P0 *)this)->ConPurse.ConPurse_field10, ((P0 *)this)->ConPurse.ConPurse_field11, ((P0 *)this)->ConPurse.ConPurse_field12, ((P0 *)this)->ConPurse.ConPurse_field13, ((P0 *)this)->ConPurse.ConPurse_field14, ((P0 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 111: /* STATE 360 - mondex.pml:169 - [place_LogBook!LogBook.LogBook_field1,LogBook.LogBook_field2,LogBook.LogBook_field3,LogBook.LogBook_field4,LogBook.LogBook_field5,LogBook.LogBook_field6] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][360] = 1;
		if (q_full(now.place_LogBook))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_LogBook);
		sprintf(simtmp, "%d", ((P0 *)this)->LogBook.LogBook_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->LogBook.LogBook_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->LogBook.LogBook_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->LogBook.LogBook_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->LogBook.LogBook_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->LogBook.LogBook_field6); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_LogBook, 0, ((P0 *)this)->LogBook.LogBook_field1, ((P0 *)this)->LogBook.LogBook_field2, ((P0 *)this)->LogBook.LogBook_field3, ((P0 *)this)->LogBook.LogBook_field4, ((P0 *)this)->LogBook.LogBook_field5, ((P0 *)this)->LogBook.LogBook_field6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6);
		_m = 2; goto P999; /* 0 */
	case 112: /* STATE 361 - mondex.pml:170 - [place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][361] = 1;
		if (q_full(now.place_msg_in))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_in, 0, ((P0 *)this)->msg_in.msg_in_field1, ((P0 *)this)->msg_in.msg_in_field2, ((P0 *)this)->msg_in.msg_in_field3, ((P0 *)this)->msg_in.msg_in_field4, ((P0 *)this)->msg_in.msg_in_field5, ((P0 *)this)->msg_in.msg_in_field6, ((P0 *)this)->msg_in.msg_in_field7, ((P0 *)this)->msg_in.msg_in_field8, ((P0 *)this)->msg_in.msg_in_field9, ((P0 *)this)->msg_in.msg_in_field10, 0, 0, 0, 0, 0, 10);
		_m = 2; goto P999; /* 0 */
	case 113: /* STATE 366 - mondex.pml:537 - [(Abort_is_enabled)] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][366] = 1;
		if (!(((int)((P0 *)this)->Abort_is_enabled)))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: Abort_is_enabled */  (trpt+1)->bup.oval = ((P0 *)this)->Abort_is_enabled;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)this)->Abort_is_enabled = 0;
		_m = 3; goto P999; /* 0 */
	case 114: /* STATE 367 - mondex.pml:419 - [ConPurse.ConPurse_field1 = ConPurse.ConPurse_field1] (0:382:15 - 1) */
		IfNotBlocked
		reached[0][367] = 1;
		(trpt+1)->bup.ovals = grab_ints(15);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->ConPurse.ConPurse_field1;
		((P0 *)this)->ConPurse.ConPurse_field1 = ((P0 *)this)->ConPurse.ConPurse_field1;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field1", ((P0 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		/* merge: ConPurse.ConPurse_field2 = ConPurse.ConPurse_field2(382, 368, 382) */
		reached[0][368] = 1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->ConPurse.ConPurse_field2;
		((P0 *)this)->ConPurse.ConPurse_field2 = ((P0 *)this)->ConPurse.ConPurse_field2;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field2", ((P0 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		/* merge: ConPurse.ConPurse_field3 = 10(382, 369, 382) */
		reached[0][369] = 1;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->ConPurse.ConPurse_field3;
		((P0 *)this)->ConPurse.ConPurse_field3 = 10;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field3", ((P0 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		/* merge: ConPurse.ConPurse_field4 = ConPurse.ConPurse_field4(382, 370, 382) */
		reached[0][370] = 1;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->ConPurse.ConPurse_field4;
		((P0 *)this)->ConPurse.ConPurse_field4 = ((P0 *)this)->ConPurse.ConPurse_field4;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field4", ((P0 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		/* merge: ConPurse.ConPurse_field5 = (ConPurse.ConPurse_field5+1)(382, 371, 382) */
		reached[0][371] = 1;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->ConPurse.ConPurse_field5;
		((P0 *)this)->ConPurse.ConPurse_field5 = (((P0 *)this)->ConPurse.ConPurse_field5+1);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field5", ((P0 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		/* merge: ConPurse.ConPurse_field6 = ConPurse.ConPurse_field6(382, 372, 382) */
		reached[0][372] = 1;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->ConPurse.ConPurse_field6;
		((P0 *)this)->ConPurse.ConPurse_field6 = ((P0 *)this)->ConPurse.ConPurse_field6;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field6", ((P0 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		/* merge: ConPurse.ConPurse_field7 = ConPurse.ConPurse_field7(382, 373, 382) */
		reached[0][373] = 1;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->ConPurse.ConPurse_field7;
		((P0 *)this)->ConPurse.ConPurse_field7 = ((P0 *)this)->ConPurse.ConPurse_field7;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field7", ((P0 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		/* merge: ConPurse.ConPurse_field8 = ConPurse.ConPurse_field8(382, 374, 382) */
		reached[0][374] = 1;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->ConPurse.ConPurse_field8;
		((P0 *)this)->ConPurse.ConPurse_field8 = ((P0 *)this)->ConPurse.ConPurse_field8;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field8", ((P0 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		/* merge: ConPurse.ConPurse_field9 = ConPurse.ConPurse_field9(382, 375, 382) */
		reached[0][375] = 1;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->ConPurse.ConPurse_field9;
		((P0 *)this)->ConPurse.ConPurse_field9 = ((P0 *)this)->ConPurse.ConPurse_field9;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field9", ((P0 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		/* merge: ConPurse.ConPurse_field10 = 1(382, 376, 382) */
		reached[0][376] = 1;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->ConPurse.ConPurse_field10;
		((P0 *)this)->ConPurse.ConPurse_field10 = 1;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field10", ((P0 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		/* merge: ConPurse.ConPurse_field11 = ConPurse.ConPurse_field5(382, 377, 382) */
		reached[0][377] = 1;
		(trpt+1)->bup.ovals[10] = ((P0 *)this)->ConPurse.ConPurse_field11;
		((P0 *)this)->ConPurse.ConPurse_field11 = ((P0 *)this)->ConPurse.ConPurse_field5;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field11", ((P0 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		/* merge: ConPurse.ConPurse_field12 = ConPurse.ConPurse_field6(382, 378, 382) */
		reached[0][378] = 1;
		(trpt+1)->bup.ovals[11] = ((P0 *)this)->ConPurse.ConPurse_field12;
		((P0 *)this)->ConPurse.ConPurse_field12 = ((P0 *)this)->ConPurse.ConPurse_field6;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field12", ((P0 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		/* merge: ConPurse.ConPurse_field13 = ConPurse.ConPurse_field7(382, 379, 382) */
		reached[0][379] = 1;
		(trpt+1)->bup.ovals[12] = ((P0 *)this)->ConPurse.ConPurse_field13;
		((P0 *)this)->ConPurse.ConPurse_field13 = ((P0 *)this)->ConPurse.ConPurse_field7;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field13", ((P0 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		/* merge: ConPurse.ConPurse_field14 = ConPurse.ConPurse_field8(382, 380, 382) */
		reached[0][380] = 1;
		(trpt+1)->bup.ovals[13] = ((P0 *)this)->ConPurse.ConPurse_field14;
		((P0 *)this)->ConPurse.ConPurse_field14 = ((P0 *)this)->ConPurse.ConPurse_field8;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field14", ((P0 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		/* merge: ConPurse.ConPurse_field15 = ConPurse.ConPurse_field9(382, 381, 382) */
		reached[0][381] = 1;
		(trpt+1)->bup.ovals[14] = ((P0 *)this)->ConPurse.ConPurse_field15;
		((P0 *)this)->ConPurse.ConPurse_field15 = ((P0 *)this)->ConPurse.ConPurse_field9;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field15", ((P0 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		_m = 3; goto P999; /* 14 */
	case 115: /* STATE 382 - mondex.pml:436 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][382] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P0 *)this)->ConPurse.ConPurse_field1, ((P0 *)this)->ConPurse.ConPurse_field2, ((P0 *)this)->ConPurse.ConPurse_field3, ((P0 *)this)->ConPurse.ConPurse_field4, ((P0 *)this)->ConPurse.ConPurse_field5, ((P0 *)this)->ConPurse.ConPurse_field6, ((P0 *)this)->ConPurse.ConPurse_field7, ((P0 *)this)->ConPurse.ConPurse_field8, ((P0 *)this)->ConPurse.ConPurse_field9, ((P0 *)this)->ConPurse.ConPurse_field10, ((P0 *)this)->ConPurse.ConPurse_field11, ((P0 *)this)->ConPurse.ConPurse_field12, ((P0 *)this)->ConPurse.ConPurse_field13, ((P0 *)this)->ConPurse.ConPurse_field14, ((P0 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 116: /* STATE 383 - mondex.pml:437 - [place_LogBook!LogBook.LogBook_field1,LogBook.LogBook_field2,LogBook.LogBook_field3,LogBook.LogBook_field4,LogBook.LogBook_field5,LogBook.LogBook_field6] (475:0:1 - 1) */
		IfNotBlocked
		reached[0][383] = 1;
		if (q_full(now.place_LogBook))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_LogBook);
		sprintf(simtmp, "%d", ((P0 *)this)->LogBook.LogBook_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->LogBook.LogBook_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->LogBook.LogBook_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->LogBook.LogBook_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->LogBook.LogBook_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->LogBook.LogBook_field6); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_LogBook, 0, ((P0 *)this)->LogBook.LogBook_field1, ((P0 *)this)->LogBook.LogBook_field2, ((P0 *)this)->LogBook.LogBook_field3, ((P0 *)this)->LogBook.LogBook_field4, ((P0 *)this)->LogBook.LogBook_field5, ((P0 *)this)->LogBook.LogBook_field6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6);
		/* merge: Abort_is_enabled = 0(475, 384, 475) */
		reached[0][384] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->Abort_is_enabled);
		((P0 *)this)->Abort_is_enabled = 0;
#ifdef VAR_RANGES
		logval("Main:Abort_is_enabled", ((int)((P0 *)this)->Abort_is_enabled));
#endif
		;
		/* merge: .(goto)(475, 390, 475) */
		reached[0][390] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 2; goto P999; /* 3 */
	case 117: /* STATE 390 - mondex.pml:540 - [.(goto)] (0:475:0 - 2) */
		IfNotBlocked
		reached[0][390] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 1 */
	case 118: /* STATE 388 - mondex.pml:538 - [(1)] (475:0:0 - 1) */
		IfNotBlocked
		reached[0][388] = 1;
		if (!(1))
			continue;
		/* merge: .(goto)(475, 390, 475) */
		reached[0][390] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 119: /* STATE 393 - mondex.pml:174 - [((place_ConPurse?[ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15]&&place_msg_in?[msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10]))] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][393] = 1;
		if (!(((q_len(now.place_ConPurse) > 0)&&(q_len(now.place_msg_in) > 0))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 120: /* STATE 394 - mondex.pml:176 - [place_ConPurse?ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:15 - 1) */
		reached[0][394] = 1;
		if (q_len(now.place_ConPurse) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(15);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->ConPurse.ConPurse_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->ConPurse.ConPurse_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->ConPurse.ConPurse_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->ConPurse.ConPurse_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->ConPurse.ConPurse_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->ConPurse.ConPurse_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->ConPurse.ConPurse_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->ConPurse.ConPurse_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->ConPurse.ConPurse_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->ConPurse.ConPurse_field10;
		(trpt+1)->bup.ovals[10] = ((P0 *)this)->ConPurse.ConPurse_field11;
		(trpt+1)->bup.ovals[11] = ((P0 *)this)->ConPurse.ConPurse_field12;
		(trpt+1)->bup.ovals[12] = ((P0 *)this)->ConPurse.ConPurse_field13;
		(trpt+1)->bup.ovals[13] = ((P0 *)this)->ConPurse.ConPurse_field14;
		(trpt+1)->bup.ovals[14] = ((P0 *)this)->ConPurse.ConPurse_field15;
		;
		((P0 *)this)->ConPurse.ConPurse_field1 = qrecv(now.place_ConPurse, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field1", ((P0 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field2 = qrecv(now.place_ConPurse, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field2", ((P0 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field3 = qrecv(now.place_ConPurse, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field3", ((P0 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field4 = qrecv(now.place_ConPurse, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field4", ((P0 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field5 = qrecv(now.place_ConPurse, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field5", ((P0 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field6 = qrecv(now.place_ConPurse, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field6", ((P0 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field7 = qrecv(now.place_ConPurse, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field7", ((P0 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field8 = qrecv(now.place_ConPurse, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field8", ((P0 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field9 = qrecv(now.place_ConPurse, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field9", ((P0 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field10 = qrecv(now.place_ConPurse, XX-1, 9, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field10", ((P0 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field11 = qrecv(now.place_ConPurse, XX-1, 10, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field11", ((P0 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field12 = qrecv(now.place_ConPurse, XX-1, 11, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field12", ((P0 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field13 = qrecv(now.place_ConPurse, XX-1, 12, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field13", ((P0 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field14 = qrecv(now.place_ConPurse, XX-1, 13, 0);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field14", ((P0 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		((P0 *)this)->ConPurse.ConPurse_field15 = qrecv(now.place_ConPurse, XX-1, 14, 1);
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field15", ((P0 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 121: /* STATE 395 - mondex.pml:177 - [place_msg_in?msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:10 - 1) */
		reached[0][395] = 1;
		if (q_len(now.place_msg_in) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(10);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->msg_in.msg_in_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->msg_in.msg_in_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->msg_in.msg_in_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->msg_in.msg_in_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->msg_in.msg_in_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->msg_in.msg_in_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->msg_in.msg_in_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->msg_in.msg_in_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->msg_in.msg_in_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->msg_in.msg_in_field10;
		;
		((P0 *)this)->msg_in.msg_in_field1 = qrecv(now.place_msg_in, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field1", ((P0 *)this)->msg_in.msg_in_field1);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field2 = qrecv(now.place_msg_in, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field2", ((P0 *)this)->msg_in.msg_in_field2);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field3 = qrecv(now.place_msg_in, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field3", ((P0 *)this)->msg_in.msg_in_field3);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field4 = qrecv(now.place_msg_in, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field4", ((P0 *)this)->msg_in.msg_in_field4);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field5 = qrecv(now.place_msg_in, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field5", ((P0 *)this)->msg_in.msg_in_field5);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field6 = qrecv(now.place_msg_in, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field6", ((P0 *)this)->msg_in.msg_in_field6);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field7 = qrecv(now.place_msg_in, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field7", ((P0 *)this)->msg_in.msg_in_field7);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field8 = qrecv(now.place_msg_in, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field8", ((P0 *)this)->msg_in.msg_in_field8);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field9 = qrecv(now.place_msg_in, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field9", ((P0 *)this)->msg_in.msg_in_field9);
#endif
		;
		((P0 *)this)->msg_in.msg_in_field10 = qrecv(now.place_msg_in, XX-1, 9, 1);
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field10", ((P0 *)this)->msg_in.msg_in_field10);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 122: /* STATE 396 - mondex.pml:179 - [(((((((msg_in.msg_in_field1==9)&&(ConPurse.ConPurse_field1==msg_in.msg_in_field10))&&(ConPurse.ConPurse_field10==1))&&(ConPurse.ConPurse_field3==13))&&1)&&1))] (439:0:1 - 1) */
		IfNotBlocked
		reached[0][396] = 1;
		if (!(((((((((P0 *)this)->msg_in.msg_in_field1==9)&&(((P0 *)this)->ConPurse.ConPurse_field1==((P0 *)this)->msg_in.msg_in_field10))&&(((P0 *)this)->ConPurse.ConPurse_field10==1))&&(((P0 *)this)->ConPurse.ConPurse_field3==13))&&1)&&1)))
			continue;
		/* merge: readExceptionLogForged_is_enabled = 1(0, 398, 439) */
		reached[0][398] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->readExceptionLogForged_is_enabled);
		((P0 *)this)->readExceptionLogForged_is_enabled = 1;
#ifdef VAR_RANGES
		logval("Main:readExceptionLogForged_is_enabled", ((int)((P0 *)this)->readExceptionLogForged_is_enabled));
#endif
		;
		/* merge: .(goto)(0, 404, 439) */
		reached[0][404] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 123: /* STATE 400 - mondex.pml:181 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][400] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P0 *)this)->ConPurse.ConPurse_field1, ((P0 *)this)->ConPurse.ConPurse_field2, ((P0 *)this)->ConPurse.ConPurse_field3, ((P0 *)this)->ConPurse.ConPurse_field4, ((P0 *)this)->ConPurse.ConPurse_field5, ((P0 *)this)->ConPurse.ConPurse_field6, ((P0 *)this)->ConPurse.ConPurse_field7, ((P0 *)this)->ConPurse.ConPurse_field8, ((P0 *)this)->ConPurse.ConPurse_field9, ((P0 *)this)->ConPurse.ConPurse_field10, ((P0 *)this)->ConPurse.ConPurse_field11, ((P0 *)this)->ConPurse.ConPurse_field12, ((P0 *)this)->ConPurse.ConPurse_field13, ((P0 *)this)->ConPurse.ConPurse_field14, ((P0 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 124: /* STATE 401 - mondex.pml:182 - [place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][401] = 1;
		if (q_full(now.place_msg_in))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_in, 0, ((P0 *)this)->msg_in.msg_in_field1, ((P0 *)this)->msg_in.msg_in_field2, ((P0 *)this)->msg_in.msg_in_field3, ((P0 *)this)->msg_in.msg_in_field4, ((P0 *)this)->msg_in.msg_in_field5, ((P0 *)this)->msg_in.msg_in_field6, ((P0 *)this)->msg_in.msg_in_field7, ((P0 *)this)->msg_in.msg_in_field8, ((P0 *)this)->msg_in.msg_in_field9, ((P0 *)this)->msg_in.msg_in_field10, 0, 0, 0, 0, 0, 10);
		_m = 2; goto P999; /* 0 */
	case 125: /* STATE 406 - mondex.pml:544 - [(readExceptionLogForged_is_enabled)] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][406] = 1;
		if (!(((int)((P0 *)this)->readExceptionLogForged_is_enabled)))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: readExceptionLogForged_is_enabled */  (trpt+1)->bup.oval = ((P0 *)this)->readExceptionLogForged_is_enabled;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)this)->readExceptionLogForged_is_enabled = 0;
		_m = 3; goto P999; /* 0 */
	case 126: /* STATE 407 - mondex.pml:441 - [ConPurse.ConPurse_field1 = ConPurse.ConPurse_field1] (0:432:25 - 1) */
		IfNotBlocked
		reached[0][407] = 1;
		(trpt+1)->bup.ovals = grab_ints(25);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->ConPurse.ConPurse_field1;
		((P0 *)this)->ConPurse.ConPurse_field1 = ((P0 *)this)->ConPurse.ConPurse_field1;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field1", ((P0 *)this)->ConPurse.ConPurse_field1);
#endif
		;
		/* merge: ConPurse.ConPurse_field2 = ConPurse.ConPurse_field2(432, 408, 432) */
		reached[0][408] = 1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->ConPurse.ConPurse_field2;
		((P0 *)this)->ConPurse.ConPurse_field2 = ((P0 *)this)->ConPurse.ConPurse_field2;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field2", ((P0 *)this)->ConPurse.ConPurse_field2);
#endif
		;
		/* merge: ConPurse.ConPurse_field3 = ConPurse.ConPurse_field3(432, 409, 432) */
		reached[0][409] = 1;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->ConPurse.ConPurse_field3;
		((P0 *)this)->ConPurse.ConPurse_field3 = ((P0 *)this)->ConPurse.ConPurse_field3;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field3", ((P0 *)this)->ConPurse.ConPurse_field3);
#endif
		;
		/* merge: ConPurse.ConPurse_field4 = ConPurse.ConPurse_field4(432, 410, 432) */
		reached[0][410] = 1;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->ConPurse.ConPurse_field4;
		((P0 *)this)->ConPurse.ConPurse_field4 = ((P0 *)this)->ConPurse.ConPurse_field4;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field4", ((P0 *)this)->ConPurse.ConPurse_field4);
#endif
		;
		/* merge: ConPurse.ConPurse_field5 = ConPurse.ConPurse_field5(432, 411, 432) */
		reached[0][411] = 1;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->ConPurse.ConPurse_field5;
		((P0 *)this)->ConPurse.ConPurse_field5 = ((P0 *)this)->ConPurse.ConPurse_field5;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field5", ((P0 *)this)->ConPurse.ConPurse_field5);
#endif
		;
		/* merge: ConPurse.ConPurse_field6 = ConPurse.ConPurse_field6(432, 412, 432) */
		reached[0][412] = 1;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->ConPurse.ConPurse_field6;
		((P0 *)this)->ConPurse.ConPurse_field6 = ((P0 *)this)->ConPurse.ConPurse_field6;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field6", ((P0 *)this)->ConPurse.ConPurse_field6);
#endif
		;
		/* merge: ConPurse.ConPurse_field7 = ConPurse.ConPurse_field7(432, 413, 432) */
		reached[0][413] = 1;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->ConPurse.ConPurse_field7;
		((P0 *)this)->ConPurse.ConPurse_field7 = ((P0 *)this)->ConPurse.ConPurse_field7;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field7", ((P0 *)this)->ConPurse.ConPurse_field7);
#endif
		;
		/* merge: ConPurse.ConPurse_field8 = ConPurse.ConPurse_field8(432, 414, 432) */
		reached[0][414] = 1;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->ConPurse.ConPurse_field8;
		((P0 *)this)->ConPurse.ConPurse_field8 = ((P0 *)this)->ConPurse.ConPurse_field8;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field8", ((P0 *)this)->ConPurse.ConPurse_field8);
#endif
		;
		/* merge: ConPurse.ConPurse_field9 = ConPurse.ConPurse_field9(432, 415, 432) */
		reached[0][415] = 1;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->ConPurse.ConPurse_field9;
		((P0 *)this)->ConPurse.ConPurse_field9 = ((P0 *)this)->ConPurse.ConPurse_field9;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field9", ((P0 *)this)->ConPurse.ConPurse_field9);
#endif
		;
		/* merge: ConPurse.ConPurse_field10 = ConPurse.ConPurse_field10(432, 416, 432) */
		reached[0][416] = 1;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->ConPurse.ConPurse_field10;
		((P0 *)this)->ConPurse.ConPurse_field10 = ((P0 *)this)->ConPurse.ConPurse_field10;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field10", ((P0 *)this)->ConPurse.ConPurse_field10);
#endif
		;
		/* merge: ConPurse.ConPurse_field11 = ConPurse.ConPurse_field11(432, 417, 432) */
		reached[0][417] = 1;
		(trpt+1)->bup.ovals[10] = ((P0 *)this)->ConPurse.ConPurse_field11;
		((P0 *)this)->ConPurse.ConPurse_field11 = ((P0 *)this)->ConPurse.ConPurse_field11;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field11", ((P0 *)this)->ConPurse.ConPurse_field11);
#endif
		;
		/* merge: ConPurse.ConPurse_field12 = ConPurse.ConPurse_field12(432, 418, 432) */
		reached[0][418] = 1;
		(trpt+1)->bup.ovals[11] = ((P0 *)this)->ConPurse.ConPurse_field12;
		((P0 *)this)->ConPurse.ConPurse_field12 = ((P0 *)this)->ConPurse.ConPurse_field12;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field12", ((P0 *)this)->ConPurse.ConPurse_field12);
#endif
		;
		/* merge: ConPurse.ConPurse_field13 = ConPurse.ConPurse_field13(432, 419, 432) */
		reached[0][419] = 1;
		(trpt+1)->bup.ovals[12] = ((P0 *)this)->ConPurse.ConPurse_field13;
		((P0 *)this)->ConPurse.ConPurse_field13 = ((P0 *)this)->ConPurse.ConPurse_field13;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field13", ((P0 *)this)->ConPurse.ConPurse_field13);
#endif
		;
		/* merge: ConPurse.ConPurse_field14 = ConPurse.ConPurse_field14(432, 420, 432) */
		reached[0][420] = 1;
		(trpt+1)->bup.ovals[13] = ((P0 *)this)->ConPurse.ConPurse_field14;
		((P0 *)this)->ConPurse.ConPurse_field14 = ((P0 *)this)->ConPurse.ConPurse_field14;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field14", ((P0 *)this)->ConPurse.ConPurse_field14);
#endif
		;
		/* merge: ConPurse.ConPurse_field15 = ConPurse.ConPurse_field15(432, 421, 432) */
		reached[0][421] = 1;
		(trpt+1)->bup.ovals[14] = ((P0 *)this)->ConPurse.ConPurse_field15;
		((P0 *)this)->ConPurse.ConPurse_field15 = ((P0 *)this)->ConPurse.ConPurse_field15;
#ifdef VAR_RANGES
		logval("Main:ConPurse.ConPurse_field15", ((P0 *)this)->ConPurse.ConPurse_field15);
#endif
		;
		/* merge: msg_out.msg_out_field1 = 3(432, 422, 432) */
		reached[0][422] = 1;
		(trpt+1)->bup.ovals[15] = ((P0 *)this)->msg_out.msg_out_field1;
		((P0 *)this)->msg_out.msg_out_field1 = 3;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field1", ((P0 *)this)->msg_out.msg_out_field1);
#endif
		;
		/* merge: msg_out.msg_out_field2 = msg_in.msg_in_field2(432, 423, 432) */
		reached[0][423] = 1;
		(trpt+1)->bup.ovals[16] = ((P0 *)this)->msg_out.msg_out_field2;
		((P0 *)this)->msg_out.msg_out_field2 = ((P0 *)this)->msg_in.msg_in_field2;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field2", ((P0 *)this)->msg_out.msg_out_field2);
#endif
		;
		/* merge: msg_out.msg_out_field3 = msg_in.msg_in_field3(432, 424, 432) */
		reached[0][424] = 1;
		(trpt+1)->bup.ovals[17] = ((P0 *)this)->msg_out.msg_out_field3;
		((P0 *)this)->msg_out.msg_out_field3 = ((P0 *)this)->msg_in.msg_in_field3;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field3", ((P0 *)this)->msg_out.msg_out_field3);
#endif
		;
		/* merge: msg_out.msg_out_field4 = msg_in.msg_in_field4(432, 425, 432) */
		reached[0][425] = 1;
		(trpt+1)->bup.ovals[18] = ((P0 *)this)->msg_out.msg_out_field4;
		((P0 *)this)->msg_out.msg_out_field4 = ((P0 *)this)->msg_in.msg_in_field4;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field4", ((P0 *)this)->msg_out.msg_out_field4);
#endif
		;
		/* merge: msg_out.msg_out_field5 = msg_in.msg_in_field5(432, 426, 432) */
		reached[0][426] = 1;
		(trpt+1)->bup.ovals[19] = ((P0 *)this)->msg_out.msg_out_field5;
		((P0 *)this)->msg_out.msg_out_field5 = ((P0 *)this)->msg_in.msg_in_field5;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field5", ((P0 *)this)->msg_out.msg_out_field5);
#endif
		;
		/* merge: msg_out.msg_out_field6 = msg_in.msg_in_field6(432, 427, 432) */
		reached[0][427] = 1;
		(trpt+1)->bup.ovals[20] = ((P0 *)this)->msg_out.msg_out_field6;
		((P0 *)this)->msg_out.msg_out_field6 = ((P0 *)this)->msg_in.msg_in_field6;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field6", ((P0 *)this)->msg_out.msg_out_field6);
#endif
		;
		/* merge: msg_out.msg_out_field7 = msg_in.msg_in_field7(432, 428, 432) */
		reached[0][428] = 1;
		(trpt+1)->bup.ovals[21] = ((P0 *)this)->msg_out.msg_out_field7;
		((P0 *)this)->msg_out.msg_out_field7 = ((P0 *)this)->msg_in.msg_in_field7;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field7", ((P0 *)this)->msg_out.msg_out_field7);
#endif
		;
		/* merge: msg_out.msg_out_field8 = msg_in.msg_in_field8(432, 429, 432) */
		reached[0][429] = 1;
		(trpt+1)->bup.ovals[22] = ((P0 *)this)->msg_out.msg_out_field8;
		((P0 *)this)->msg_out.msg_out_field8 = ((P0 *)this)->msg_in.msg_in_field8;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field8", ((P0 *)this)->msg_out.msg_out_field8);
#endif
		;
		/* merge: msg_out.msg_out_field9 = msg_in.msg_in_field9(432, 430, 432) */
		reached[0][430] = 1;
		(trpt+1)->bup.ovals[23] = ((P0 *)this)->msg_out.msg_out_field9;
		((P0 *)this)->msg_out.msg_out_field9 = ((P0 *)this)->msg_in.msg_in_field9;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field9", ((P0 *)this)->msg_out.msg_out_field9);
#endif
		;
		/* merge: msg_out.msg_out_field10 = msg_in.msg_in_field10(432, 431, 432) */
		reached[0][431] = 1;
		(trpt+1)->bup.ovals[24] = ((P0 *)this)->msg_out.msg_out_field10;
		((P0 *)this)->msg_out.msg_out_field10 = ((P0 *)this)->msg_in.msg_in_field10;
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field10", ((P0 *)this)->msg_out.msg_out_field10);
#endif
		;
		_m = 3; goto P999; /* 24 */
	case 127: /* STATE 432 - mondex.pml:467 - [place_ConPurse!ConPurse.ConPurse_field1,ConPurse.ConPurse_field2,ConPurse.ConPurse_field3,ConPurse.ConPurse_field4,ConPurse.ConPurse_field5,ConPurse.ConPurse_field6,ConPurse.ConPurse_field7,ConPurse.ConPurse_field8,ConPurse.ConPurse_field9,ConPurse.ConPurse_field10,ConPurse.ConPurse_field11,ConPurse.ConPurse_field12,ConPurse.ConPurse_field13,ConPurse.ConPurse_field14,ConPurse.ConPurse_field15] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][432] = 1;
		if (q_full(now.place_ConPurse))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_ConPurse);
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field10); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field11); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field12); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field13); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field14); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->ConPurse.ConPurse_field15); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_ConPurse, 0, ((P0 *)this)->ConPurse.ConPurse_field1, ((P0 *)this)->ConPurse.ConPurse_field2, ((P0 *)this)->ConPurse.ConPurse_field3, ((P0 *)this)->ConPurse.ConPurse_field4, ((P0 *)this)->ConPurse.ConPurse_field5, ((P0 *)this)->ConPurse.ConPurse_field6, ((P0 *)this)->ConPurse.ConPurse_field7, ((P0 *)this)->ConPurse.ConPurse_field8, ((P0 *)this)->ConPurse.ConPurse_field9, ((P0 *)this)->ConPurse.ConPurse_field10, ((P0 *)this)->ConPurse.ConPurse_field11, ((P0 *)this)->ConPurse.ConPurse_field12, ((P0 *)this)->ConPurse.ConPurse_field13, ((P0 *)this)->ConPurse.ConPurse_field14, ((P0 *)this)->ConPurse.ConPurse_field15, 15);
		_m = 2; goto P999; /* 0 */
	case 128: /* STATE 433 - mondex.pml:468 - [place_msg_out!msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10] (475:0:1 - 1) */
		IfNotBlocked
		reached[0][433] = 1;
		if (q_full(now.place_msg_out))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_out);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_out, 0, ((P0 *)this)->msg_out.msg_out_field1, ((P0 *)this)->msg_out.msg_out_field2, ((P0 *)this)->msg_out.msg_out_field3, ((P0 *)this)->msg_out.msg_out_field4, ((P0 *)this)->msg_out.msg_out_field5, ((P0 *)this)->msg_out.msg_out_field6, ((P0 *)this)->msg_out.msg_out_field7, ((P0 *)this)->msg_out.msg_out_field8, ((P0 *)this)->msg_out.msg_out_field9, ((P0 *)this)->msg_out.msg_out_field10, 0, 0, 0, 0, 0, 10);
		/* merge: readExceptionLogForged_is_enabled = 0(475, 434, 475) */
		reached[0][434] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->readExceptionLogForged_is_enabled);
		((P0 *)this)->readExceptionLogForged_is_enabled = 0;
#ifdef VAR_RANGES
		logval("Main:readExceptionLogForged_is_enabled", ((int)((P0 *)this)->readExceptionLogForged_is_enabled));
#endif
		;
		/* merge: .(goto)(475, 440, 475) */
		reached[0][440] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 2; goto P999; /* 3 */
	case 129: /* STATE 440 - mondex.pml:547 - [.(goto)] (0:475:0 - 2) */
		IfNotBlocked
		reached[0][440] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 1 */
	case 130: /* STATE 438 - mondex.pml:545 - [(1)] (475:0:0 - 1) */
		IfNotBlocked
		reached[0][438] = 1;
		if (!(1))
			continue;
		/* merge: .(goto)(475, 440, 475) */
		reached[0][440] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 131: /* STATE 443 - mondex.pml:186 - [(place_msg_out?[msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10])] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][443] = 1;
		if (!((q_len(now.place_msg_out) > 0)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 132: /* STATE 444 - mondex.pml:188 - [place_msg_out?msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10] (0:0:10 - 1) */
		reached[0][444] = 1;
		if (q_len(now.place_msg_out) == 0) continue;

		XX=1;
		(trpt+1)->bup.ovals = grab_ints(10);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->msg_out.msg_out_field1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->msg_out.msg_out_field2;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->msg_out.msg_out_field3;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->msg_out.msg_out_field4;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->msg_out.msg_out_field5;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->msg_out.msg_out_field6;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->msg_out.msg_out_field7;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->msg_out.msg_out_field8;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->msg_out.msg_out_field9;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->msg_out.msg_out_field10;
		;
		((P0 *)this)->msg_out.msg_out_field1 = qrecv(now.place_msg_out, XX-1, 0, 0);
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field1", ((P0 *)this)->msg_out.msg_out_field1);
#endif
		;
		((P0 *)this)->msg_out.msg_out_field2 = qrecv(now.place_msg_out, XX-1, 1, 0);
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field2", ((P0 *)this)->msg_out.msg_out_field2);
#endif
		;
		((P0 *)this)->msg_out.msg_out_field3 = qrecv(now.place_msg_out, XX-1, 2, 0);
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field3", ((P0 *)this)->msg_out.msg_out_field3);
#endif
		;
		((P0 *)this)->msg_out.msg_out_field4 = qrecv(now.place_msg_out, XX-1, 3, 0);
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field4", ((P0 *)this)->msg_out.msg_out_field4);
#endif
		;
		((P0 *)this)->msg_out.msg_out_field5 = qrecv(now.place_msg_out, XX-1, 4, 0);
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field5", ((P0 *)this)->msg_out.msg_out_field5);
#endif
		;
		((P0 *)this)->msg_out.msg_out_field6 = qrecv(now.place_msg_out, XX-1, 5, 0);
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field6", ((P0 *)this)->msg_out.msg_out_field6);
#endif
		;
		((P0 *)this)->msg_out.msg_out_field7 = qrecv(now.place_msg_out, XX-1, 6, 0);
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field7", ((P0 *)this)->msg_out.msg_out_field7);
#endif
		;
		((P0 *)this)->msg_out.msg_out_field8 = qrecv(now.place_msg_out, XX-1, 7, 0);
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field8", ((P0 *)this)->msg_out.msg_out_field8);
#endif
		;
		((P0 *)this)->msg_out.msg_out_field9 = qrecv(now.place_msg_out, XX-1, 8, 0);
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field9", ((P0 *)this)->msg_out.msg_out_field9);
#endif
		;
		((P0 *)this)->msg_out.msg_out_field10 = qrecv(now.place_msg_out, XX-1, 9, 1);
#ifdef VAR_RANGES
		logval("Main:msg_out.msg_out_field10", ((P0 *)this)->msg_out.msg_out_field10);
#endif
		;
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.place_msg_out);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field10); strcat(simvals, simtmp);		}
#endif
		;
		_m = 4; goto P999; /* 0 */
	case 133: /* STATE 445 - mondex.pml:190 - [(((msg_out.msg_out_field1==3)&&1))] (471:0:1 - 1) */
		IfNotBlocked
		reached[0][445] = 1;
		if (!(((((P0 *)this)->msg_out.msg_out_field1==3)&&1)))
			continue;
		/* merge: ether_is_enabled = 1(0, 447, 471) */
		reached[0][447] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->ether_is_enabled);
		((P0 *)this)->ether_is_enabled = 1;
#ifdef VAR_RANGES
		logval("Main:ether_is_enabled", ((int)((P0 *)this)->ether_is_enabled));
#endif
		;
		/* merge: .(goto)(0, 452, 471) */
		reached[0][452] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 134: /* STATE 449 - mondex.pml:192 - [place_msg_out!msg_out.msg_out_field1,msg_out.msg_out_field2,msg_out.msg_out_field3,msg_out.msg_out_field4,msg_out.msg_out_field5,msg_out.msg_out_field6,msg_out.msg_out_field7,msg_out.msg_out_field8,msg_out.msg_out_field9,msg_out.msg_out_field10] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][449] = 1;
		if (q_full(now.place_msg_out))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_out);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_out.msg_out_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_out, 0, ((P0 *)this)->msg_out.msg_out_field1, ((P0 *)this)->msg_out.msg_out_field2, ((P0 *)this)->msg_out.msg_out_field3, ((P0 *)this)->msg_out.msg_out_field4, ((P0 *)this)->msg_out.msg_out_field5, ((P0 *)this)->msg_out.msg_out_field6, ((P0 *)this)->msg_out.msg_out_field7, ((P0 *)this)->msg_out.msg_out_field8, ((P0 *)this)->msg_out.msg_out_field9, ((P0 *)this)->msg_out.msg_out_field10, 0, 0, 0, 0, 0, 10);
		_m = 2; goto P999; /* 0 */
	case 135: /* STATE 454 - mondex.pml:551 - [(ether_is_enabled)] (0:0:1 - 1) */
		IfNotBlocked
		reached[0][454] = 1;
		if (!(((int)((P0 *)this)->ether_is_enabled)))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: ether_is_enabled */  (trpt+1)->bup.oval = ((P0 *)this)->ether_is_enabled;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)this)->ether_is_enabled = 0;
		_m = 3; goto P999; /* 0 */
	case 136: /* STATE 455 - mondex.pml:472 - [msg_in.msg_in_field1 = msg_out.msg_out_field1] (0:465:10 - 1) */
		IfNotBlocked
		reached[0][455] = 1;
		(trpt+1)->bup.ovals = grab_ints(10);
		(trpt+1)->bup.ovals[0] = ((P0 *)this)->msg_in.msg_in_field1;
		((P0 *)this)->msg_in.msg_in_field1 = ((P0 *)this)->msg_out.msg_out_field1;
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field1", ((P0 *)this)->msg_in.msg_in_field1);
#endif
		;
		/* merge: msg_in.msg_in_field2 = msg_out.msg_out_field2(465, 456, 465) */
		reached[0][456] = 1;
		(trpt+1)->bup.ovals[1] = ((P0 *)this)->msg_in.msg_in_field2;
		((P0 *)this)->msg_in.msg_in_field2 = ((P0 *)this)->msg_out.msg_out_field2;
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field2", ((P0 *)this)->msg_in.msg_in_field2);
#endif
		;
		/* merge: msg_in.msg_in_field3 = msg_out.msg_out_field3(465, 457, 465) */
		reached[0][457] = 1;
		(trpt+1)->bup.ovals[2] = ((P0 *)this)->msg_in.msg_in_field3;
		((P0 *)this)->msg_in.msg_in_field3 = ((P0 *)this)->msg_out.msg_out_field3;
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field3", ((P0 *)this)->msg_in.msg_in_field3);
#endif
		;
		/* merge: msg_in.msg_in_field4 = msg_out.msg_out_field4(465, 458, 465) */
		reached[0][458] = 1;
		(trpt+1)->bup.ovals[3] = ((P0 *)this)->msg_in.msg_in_field4;
		((P0 *)this)->msg_in.msg_in_field4 = ((P0 *)this)->msg_out.msg_out_field4;
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field4", ((P0 *)this)->msg_in.msg_in_field4);
#endif
		;
		/* merge: msg_in.msg_in_field5 = msg_out.msg_out_field5(465, 459, 465) */
		reached[0][459] = 1;
		(trpt+1)->bup.ovals[4] = ((P0 *)this)->msg_in.msg_in_field5;
		((P0 *)this)->msg_in.msg_in_field5 = ((P0 *)this)->msg_out.msg_out_field5;
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field5", ((P0 *)this)->msg_in.msg_in_field5);
#endif
		;
		/* merge: msg_in.msg_in_field6 = msg_out.msg_out_field6(465, 460, 465) */
		reached[0][460] = 1;
		(trpt+1)->bup.ovals[5] = ((P0 *)this)->msg_in.msg_in_field6;
		((P0 *)this)->msg_in.msg_in_field6 = ((P0 *)this)->msg_out.msg_out_field6;
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field6", ((P0 *)this)->msg_in.msg_in_field6);
#endif
		;
		/* merge: msg_in.msg_in_field7 = msg_out.msg_out_field7(465, 461, 465) */
		reached[0][461] = 1;
		(trpt+1)->bup.ovals[6] = ((P0 *)this)->msg_in.msg_in_field7;
		((P0 *)this)->msg_in.msg_in_field7 = ((P0 *)this)->msg_out.msg_out_field7;
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field7", ((P0 *)this)->msg_in.msg_in_field7);
#endif
		;
		/* merge: msg_in.msg_in_field8 = msg_out.msg_out_field8(465, 462, 465) */
		reached[0][462] = 1;
		(trpt+1)->bup.ovals[7] = ((P0 *)this)->msg_in.msg_in_field8;
		((P0 *)this)->msg_in.msg_in_field8 = ((P0 *)this)->msg_out.msg_out_field8;
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field8", ((P0 *)this)->msg_in.msg_in_field8);
#endif
		;
		/* merge: msg_in.msg_in_field9 = msg_out.msg_out_field9(465, 463, 465) */
		reached[0][463] = 1;
		(trpt+1)->bup.ovals[8] = ((P0 *)this)->msg_in.msg_in_field9;
		((P0 *)this)->msg_in.msg_in_field9 = ((P0 *)this)->msg_out.msg_out_field9;
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field9", ((P0 *)this)->msg_in.msg_in_field9);
#endif
		;
		/* merge: msg_in.msg_in_field10 = msg_out.msg_out_field10(465, 464, 465) */
		reached[0][464] = 1;
		(trpt+1)->bup.ovals[9] = ((P0 *)this)->msg_in.msg_in_field10;
		((P0 *)this)->msg_in.msg_in_field10 = ((P0 *)this)->msg_out.msg_out_field10;
#ifdef VAR_RANGES
		logval("Main:msg_in.msg_in_field10", ((P0 *)this)->msg_in.msg_in_field10);
#endif
		;
		_m = 3; goto P999; /* 9 */
	case 137: /* STATE 465 - mondex.pml:482 - [place_msg_in!msg_in.msg_in_field1,msg_in.msg_in_field2,msg_in.msg_in_field3,msg_in.msg_in_field4,msg_in.msg_in_field5,msg_in.msg_in_field6,msg_in.msg_in_field7,msg_in.msg_in_field8,msg_in.msg_in_field9,msg_in.msg_in_field10] (475:0:1 - 1) */
		IfNotBlocked
		reached[0][465] = 1;
		if (q_full(now.place_msg_in))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.place_msg_in);
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field1); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field2); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field3); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field4); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field5); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field6); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field7); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field8); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field9); strcat(simvals, simtmp);		strcat(simvals, ",");
		sprintf(simtmp, "%d", ((P0 *)this)->msg_in.msg_in_field10); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.place_msg_in, 0, ((P0 *)this)->msg_in.msg_in_field1, ((P0 *)this)->msg_in.msg_in_field2, ((P0 *)this)->msg_in.msg_in_field3, ((P0 *)this)->msg_in.msg_in_field4, ((P0 *)this)->msg_in.msg_in_field5, ((P0 *)this)->msg_in.msg_in_field6, ((P0 *)this)->msg_in.msg_in_field7, ((P0 *)this)->msg_in.msg_in_field8, ((P0 *)this)->msg_in.msg_in_field9, ((P0 *)this)->msg_in.msg_in_field10, 0, 0, 0, 0, 0, 10);
		/* merge: ether_is_enabled = 0(475, 466, 475) */
		reached[0][466] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)this)->ether_is_enabled);
		((P0 *)this)->ether_is_enabled = 0;
#ifdef VAR_RANGES
		logval("Main:ether_is_enabled", ((int)((P0 *)this)->ether_is_enabled));
#endif
		;
		/* merge: .(goto)(475, 472, 475) */
		reached[0][472] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 2; goto P999; /* 3 */
	case 138: /* STATE 472 - mondex.pml:554 - [.(goto)] (0:475:0 - 2) */
		IfNotBlocked
		reached[0][472] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 1 */
	case 139: /* STATE 470 - mondex.pml:552 - [(1)] (475:0:0 - 1) */
		IfNotBlocked
		reached[0][470] = 1;
		if (!(1))
			continue;
		/* merge: .(goto)(475, 472, 475) */
		reached[0][472] = 1;
		;
		/* merge: .(goto)(0, 476, 475) */
		reached[0][476] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 140: /* STATE 478 - mondex.pml:585 - [-end-] (0:0:0 - 1) */
		IfNotBlocked
		reached[0][478] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */
	case  _T5:	/* np_ */
		if (!((!(trpt->o_pm&4) && !(trpt->tau&128))))
			continue;
		/* else fall through */
	case  _T2:	/* true */
		_m = 3; goto P999;
#undef rand
	}

