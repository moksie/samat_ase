package SAMGUI.sam_controller;

import pipe.dataLayer.*;
import pipe.gui.CreateGui;

import javax.swing.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class MergeGui  {

  private static final Place sDummyPlace = new Place(0.0, 0.0, "select", "-Select-", 0.0, 0.0, 0, 0.0, 0.0, 0);
  private static final Transition sDummyTransition = new Transition(0.0, 0.0, "select", "-Select-", 0.0, 0.0, 0.0, false, false, 0, 0, "");
    private JTextArea textArea;
    private JButton btn1;
    private JButton btn2;
    private JButton btn3;
    private JButton btn4;
    private JFileChooser fileChooser;
    private JButton button;

    private JFrame rootPane;
    
    private DataLayer mSourceNet;
    private JComboBox<Place> mSourcePlaceSelector;
         
    private int mode;
    public static final int MODE_OPEN = 1;
    public static final int MODE_SAVE = 2;
    private JTextField textField;
    private File mSourceDirectory;
  private JComboBox mTargetPlaceSelctor;
  private DataLayer mTargetDataLayer;
  private JComboBox mSourceTransitionSelector;
  private JComboBox mTargetTransitionSelection;

  public void DisplayMergeActions()
    {
        final JFrame frame =new JFrame("Weave-Nets");
        rootPane = frame;
        frame.setVisible(true);
        frame.setSize(200,200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
   	    JPanel panel = new JPanel();
        frame.add(panel);
        GridBagConstraints c =new GridBagConstraints();
        GridBagLayout gridbag = new GridBagLayout();
        //Container contentPane = getContentPane();
    	//c.insets = new Insets(10,10,10,10);
//    	JButton btn1=new JButton("Base-Nets");
//    	
//    	
//    	btn1.addActionListener(new CustomActionListener());
    	//String[] basenets ={"base1", "base2"};
    	DataLayer dl = new DataLayer();
    	ArrayList placesArray = null;
    	
    	JTextField BNTxt= new JTextField(10);
    	JButton BNbtn = new JButton("Base-Net File");
    	c.gridx=1;
    	c.gridy=0;
    	gridbag.setConstraints(BNTxt, c);
    	panel.add(BNTxt, c);
    	panel.add(BNbtn, c);
        BNbtn.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent ae) {
            JFileChooser Chooser = new JFileChooser();
            Chooser.setMultiSelectionEnabled(true);
            Chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            int result = Chooser.showDialog(null, "Select Base Net Files");
            if (result == JFileChooser.APPROVE_OPTION) {
              File file = Chooser.getSelectedFile();
                mSourceDirectory = file.getParentFile();
              BNTxt.setText(file.getAbsolutePath());
              mSourceNet = new DataLayer(file);

              mSourcePlaceSelector.removeAllItems();
              mSourcePlaceSelector.addItem(sDummyPlace);
              for (Place place : mSourceNet.getPlaces()) {
                mSourcePlaceSelector.addItem(place);
              }

              mSourceTransitionSelector.removeAllItems();
              mSourceTransitionSelector.addItem(sDummyTransition);
              for (Transition transition : mSourceNet.getTransitions()) {
                mSourceTransitionSelector.addItem(transition);
              }
            }
          }
        });

      mTargetPlaceSelctor= new JComboBox();
      JLabel label4 = new JLabel("Target Place:");
      panel.add(label4);
      c.gridx=5;
      c.gridy=0;
      gridbag.setConstraints(mTargetPlaceSelctor, c);
      panel.add(mTargetPlaceSelctor, c);
//      mTargetPlaceSelctor.addActionListener(new CustomActionListener());
        
        JTextField ANTxt= new JTextField(10);
    	JButton ANbtn = new JButton("Aspect-Net File");
    	c.gridx=2;
    	c.gridy=0;
    	gridbag.setConstraints(ANTxt, c);
    	panel.add(ANTxt, c);
    	panel.add(ANbtn,c);
    	DataLayer dt= new DataLayer();
    	
    	ANbtn.addActionListener(new ActionListener()
    	{
        	public void actionPerformed(ActionEvent evt)
            {
        		JFileChooser Chooser = new JFileChooser();
                Chooser.setMultiSelectionEnabled(true);
                Chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                int result = Chooser.showDialog(null,"Select Aspect Net Files");
                if(result == JFileChooser.APPROVE_OPTION)
                {
                    File file = Chooser.getSelectedFile();
                    ANTxt.setText(file.getAbsolutePath());
                    mTargetDataLayer = new DataLayer(file);

                  mTargetPlaceSelctor.removeAllItems();
                  mTargetPlaceSelctor.addItem(sDummyPlace);
                  for (Place place : mTargetDataLayer.getPlaces()) {
                    mTargetPlaceSelctor.addItem(place);
                  }

                  mTargetTransitionSelection.removeAllItems();
                  mTargetTransitionSelection.addItem(sDummyTransition);
                  for (Transition transition : mTargetDataLayer.getTransitions()) {
                    mTargetTransitionSelection.addItem(transition);
                  }
                 }
            }
    	});      
     	 	
    	mSourcePlaceSelector= new JComboBox();
        JLabel label3 = new JLabel("Source Place:");
        panel.add(label3);
        c.gridx=4;
    	c.gridy=0;
    	gridbag.setConstraints(mSourcePlaceSelector, c);
    	panel.add(mSourcePlaceSelector, c);

    	mSourceTransitionSelector= new JComboBox();
        JLabel label5 = new JLabel("Source Transition:");
        panel.add(label5);
        c.gridx=1;
    	c.gridy=0;
    	gridbag.setConstraints(mSourceTransitionSelector, c);
    	panel.add(mSourceTransitionSelector, c);

    	 mTargetTransitionSelection= new JComboBox();
        JLabel label6 = new JLabel("Target Transition:");
        panel.add(label6);
        c.gridx=1;
    	c.gridy=0;
    	gridbag.setConstraints(mTargetTransitionSelection, c);
    	panel.add(mTargetTransitionSelection, c);
//    	mTargetTransitionSelection.addActionListener(new CustomActionListener());
    	
    	JButton btn1 = new JButton("Merge");
    	c.gridx=7;
    	c.gridy=8;
    	btn1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          weaveNet("merge");
        }
      });
    	btn1.setEnabled(true);
		Dimension buttonSize = new Dimension(60,60);
		btn1.setMaximumSize(buttonSize);
		panel.add(btn1, c);
		
		JButton btn2 = new JButton("Connect");
    	c.gridx=8;
    	c.gridy=8;
    	btn2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          weaveNet("connect");
        }
      });
    	btn2.setEnabled(true);
		Dimension buttonSize1 = new Dimension(60,60);
		btn2.setMaximumSize(buttonSize1);
		panel.add(btn2,c);
		
		JButton btn3 = new JButton("Cancel");
    	c.gridx=8;
    	c.gridy=8;
//    	btn3.addActionListener(new CustomActionListener());
    	btn3.setEnabled(true);
		Dimension buttonSize2 = new Dimension(60,60);
		btn3.setMaximumSize(buttonSize2);
		panel.add(btn3,c);
		
		   	
  }

    private void weaveNet(final String pType) {
        PlaceTransitionObject source = null;
        PlaceTransitionObject target = null;
        if (mSourcePlaceSelector.getSelectedIndex() > 0) {
            source = (PlaceTransitionObject) mSourcePlaceSelector.getSelectedItem();
        }
        else  if(mSourceTransitionSelector.getSelectedIndex() > 0) {
            source = (PlaceTransitionObject) mSourceTransitionSelector.getSelectedItem();
        }

        if (mTargetPlaceSelctor.getSelectedIndex() > 0) {
            target = (PlaceTransitionObject) mTargetPlaceSelctor.getSelectedItem();
        }
        else  if(mTargetTransitionSelection.getSelectedIndex() > 0) {
            target = (PlaceTransitionObject) mTargetTransitionSelection.getSelectedItem();
        }

        if (source == null || target == null) {
            JOptionPane.showMessageDialog(rootPane.getParent(), "Please specify valid source and target place transition object", "Error connecting nets", JOptionPane.ERROR_MESSAGE);
        }
        else {
            if ("connect".equals(pType)) {
                Arc arc = new NormalArc(0.0, 0.0, 0.0, 0.0, source, target, 1, null, false);
                mSourceNet.connect(arc, mTargetDataLayer);
            }
            else if ("merge".equals(pType)){
                mSourceNet.merge(source, target, mTargetDataLayer);
            }

            try {

                File tempFile = new File(mSourceDirectory, String.format("connected-%s-%s.xml", mSourceNet.pnmlName, mTargetDataLayer.pnmlName));
//                File tempFile = new File(new File(System.getProperty("java.io.tmpdir")), String.format("connected-%s-%s.xml", mSourceNet.pnmlName, mTargetDataLayer.pnmlName));
                DataLayerWriter saveModel = new DataLayerWriter(mSourceNet);
                saveModel.savePNML(tempFile);

                CreateGui.userPath = tempFile.getParent();
                CreateGui.getApp().createNewTab(tempFile, false);
            }
            catch (IOException | ParserConfigurationException | TransformerException e1) {
                e1.printStackTrace();
            }
        }
    }


}
   	
    	
        